$(document).ready(function () {
  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
      $('#sidebarCollapse').toggleClass('sidebar__btn--open');
      $('.sidebar__p--none').toggleClass('active');
      $('.main').toggleClass('sem-margem');
      $('.sidebar__logo--block').toggleClass('sidebar__logo--none');
      $('.sidebar__logo--sm').toggleClass('sidebar__logo--sm--block');
  });
});


//Switch light/dark
$("#switch").on('click', function () {
  if ($("body").hasClass("dark")) {
      $("body").removeClass("dark");
      $("nav").removeClass("dark");
      $("#switch").removeClass("switched");
  }
  else {
      $("body").addClass("dark");
      $("nav").addClass("dark");
      $("#switch").addClass("switched");
  }
}); 