#pragma checksum "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9e2e9b1594f45030c7715b3248261664146923a8"
// <auto-generated/>
#pragma warning disable 1591
namespace Laterza.Admin.Blazor.Pages.Blog
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Home;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using BlazorInputFile;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Empreendimento;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Escritorio;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Projeto;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.MdMidia;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.DragDrop;

#line default
#line hidden
#nullable disable
    public partial class PostGaleriaFormComponent : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "h3");
            __builder.AddAttribute(1, "class", "mb-3");
            __builder.AddMarkupContent(2, "<i class=\"fas fa-images\"></i> ");
            __builder.AddContent(3, 
#nullable restore
#line 1 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                                 model.Id == Guid.Empty ? "Inserir" : "Alterar"

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(4, "\r\n\r\n");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(5);
            __builder.AddAttribute(6, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                  model

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(7, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                         HandleValidSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(8, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.OpenElement(9, "div");
                __builder2.AddAttribute(10, "class", "row");
#nullable restore
#line 5 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
         if (!model.EhVideo)
        {

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(11, "div");
                __builder2.AddAttribute(12, "class", "col-md-12");
                __builder2.OpenElement(13, "div");
                __builder2.AddAttribute(14, "class", "form-group");
                __builder2.AddMarkupContent(15, "<h5 class=\"mb-3\">Banner</h5>\r\n\r\n                    ");
                __builder2.OpenComponent<Laterza.Admin.Blazor.Shared.UploadImgFile>(16);
                __builder2.AddAttribute(17, "width", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                          1350

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(18, "height", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                                        706

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(19, "ProcessarImagemCallBack", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, 
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                                                                      HandleProcessarUrlBannerCallBack

#line default
#line hidden
#nullable disable
                )));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(20, "\r\n                ");
                __builder2.OpenElement(21, "div");
                __builder2.AddAttribute(22, "class", "form-group");
#nullable restore
#line 14 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                     if (model.UrlImagem != null)
                    {

#line default
#line hidden
#nullable disable
                __builder2.OpenComponent<Laterza.Admin.Blazor.Shared.ImgComponent>(23);
                __builder2.AddAttribute(24, "LinkImagem", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 16 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                                   model.UrlImagem

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                    }

#line default
#line hidden
#nullable disable
                __builder2.CloseElement();
                __builder2.CloseElement();
#nullable restore
#line 20 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
        }
        else
        {

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(25, "div");
                __builder2.AddAttribute(26, "class", "col-md-12");
                __builder2.OpenElement(27, "div");
                __builder2.AddAttribute(28, "class", "form-group");
                __builder2.AddMarkupContent(29, "<h5 class=\"mb-3\">Vídeo YouTube</h5>\r\n                    ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(30);
                __builder2.AddAttribute(31, "class", "");
                __builder2.AddAttribute(32, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 26 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                            model.UrlVideo

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(33, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.UrlVideo = __value, model.UrlVideo))));
                __builder2.AddAttribute(34, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.UrlVideo));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.CloseElement();
#nullable restore
#line 29 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
        }

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(35, "div");
                __builder2.AddAttribute(36, "class", "col-md-12");
                __builder2.OpenElement(37, "div");
                __builder2.AddAttribute(38, "class", "form-group");
                __builder2.AddMarkupContent(39, "<h5 class=\"mb-3\">Título</h5>\r\n                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(40);
                __builder2.AddAttribute(41, "class", "");
                __builder2.AddAttribute(42, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 34 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                        model.Titulo

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(43, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.Titulo = __value, model.Titulo))));
                __builder2.AddAttribute(44, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.Titulo));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(45, "\r\n            ");
                __builder2.OpenElement(46, "div");
                __builder2.AddAttribute(47, "class", "form-group");
                __builder2.AddMarkupContent(48, "<h5 class=\"mb-3\">Descrição</h5>\r\n                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(49);
                __builder2.AddAttribute(50, "class", "");
                __builder2.AddAttribute(51, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 38 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                        model.Descricao

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(52, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.Descricao = __value, model.Descricao))));
                __builder2.AddAttribute(53, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.Descricao));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(54, "\r\n\r\n            ");
                __builder2.OpenElement(55, "div");
                __builder2.AddAttribute(56, "class", "d-flex form-group");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputCheckbox>(57);
                __builder2.AddAttribute(58, "class", "my-auto");
                __builder2.AddAttribute(59, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 42 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Blog\PostGaleriaFormComponent.razor"
                                            model.EhVideo

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(60, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.Boolean>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.Boolean>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.EhVideo = __value, model.EhVideo))));
                __builder2.AddAttribute(61, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.Boolean>>>(() => model.EhVideo));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(62, "\r\n                ");
                __builder2.AddMarkupContent(63, "<h5 class=\"ml-2\">Video</h5>");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(64, "\r\n            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(65);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(66, "\r\n            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.ValidationSummary>(67);
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(68, "\r\n        ");
                __builder2.AddMarkupContent(69, "<div class=\"col-md-12 text-right\"><button class=\"btn btn btn-lg btn-primary\" type=\"submit\"><i class=\"fas fa-file mr-1\"></i>Salvar</button></div>");
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
