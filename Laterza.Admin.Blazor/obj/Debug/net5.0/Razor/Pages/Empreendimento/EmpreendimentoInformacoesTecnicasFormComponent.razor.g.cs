#pragma checksum "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "150d092b233615ffa8afcea6f9495bcf30f2671e"
// <auto-generated/>
#pragma warning disable 1591
namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Home;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using BlazorInputFile;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Empreendimento;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Escritorio;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Projeto;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.MdMidia;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.DragDrop;

#line default
#line hidden
#nullable disable
    public partial class EmpreendimentoInformacoesTecnicasFormComponent : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "h3");
            __builder.AddMarkupContent(1, "<span class=\"oi oi-info\"></span> ");
            __builder.AddContent(2, 
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                       model.Id == Guid.Empty ? "Inserir" : "Alterar"

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(3, "  Informações Técnicas ");
            __builder.CloseElement();
            __builder.AddMarkupContent(4, "\r\n\r\n");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(5);
            __builder.AddAttribute(6, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 4 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                  model

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(7, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 4 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                         HandleValidSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(8, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.OpenElement(9, "div");
                __builder2.AddAttribute(10, "class", "row");
                __builder2.OpenElement(11, "div");
                __builder2.AddAttribute(12, "class", "col-md-12 mb-xl-3");
                __builder2.OpenComponent<Laterza.Admin.Blazor.Shared.IdiomaTabComponent>(13);
                __builder2.AddAttribute(14, "ProcessarIdiomaModel", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, 
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                                      HandleProcessarIdiomaModel

#line default
#line hidden
#nullable disable
                )));
                __builder2.AddAttribute(15, "hideTabsIdioma", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                                                                                   hideTabsIdioma

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(16, "idiomaParam", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                                                                                                                 idiomaModal

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(17, "\r\n        ");
                __builder2.OpenElement(18, "div");
                __builder2.AddAttribute(19, "class", "col-md-12");
                __builder2.OpenElement(20, "div");
                __builder2.AddAttribute(21, "class", "form-group");
                __builder2.AddMarkupContent(22, "<label>Função</label>\r\n                ");
                __builder2.AddMarkupContent(23, "<h5>Status</h5>\r\n                ");
                __Blazor.Laterza.Admin.Blazor.Pages.Empreendimento.EmpreendimentoInformacoesTecnicasFormComponent.TypeInference.CreateInputSelect_0(__builder2, 24, 25, "form-control form-control-lg", 26, 
#nullable restore
#line 13 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                                                               model.Funcao

#line default
#line hidden
#nullable disable
                , 27, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.Funcao = __value, model.Funcao)), 28, () => model.Funcao, 29, (__builder3) => {
                    __builder3.AddMarkupContent(30, "<option disabled selected value> -- Escolha um item -- </option>");
#nullable restore
#line 15 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                     foreach (var m in Enum.GetValues(typeof(EnumInformacoesTecnicas)).Cast<EnumInformacoesTecnicas>().ToList())
                    {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(31, "option");
                    __builder3.AddAttribute(32, "value", 
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                         ((int)m).ToString()

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddContent(33, 
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                                                 extensionMethods.Description(m)

#line default
#line hidden
#nullable disable
                    );
                    __builder3.CloseElement();
#nullable restore
#line 18 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                    }

#line default
#line hidden
#nullable disable
                }
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(34, "\r\n            ");
                __builder2.OpenElement(35, "div");
                __builder2.AddAttribute(36, "class", "form-group");
                __builder2.AddMarkupContent(37, "<label>Descrição</label>\r\n                ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(38);
                __builder2.AddAttribute(39, "class", "form-control form-control-lg");
                __builder2.AddAttribute(40, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 23 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoInformacoesTecnicasFormComponent.razor"
                                        model.Nome

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(41, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.Nome = __value, model.Nome))));
                __builder2.AddAttribute(42, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => model.Nome));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(43, "\r\n            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(44);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(45, "\r\n            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.ValidationSummary>(46);
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(47, "\r\n        ");
                __builder2.AddMarkupContent(48, "<div class=\"col-md-12 text-right\"><button class=\"btn btn btn-lg btn-primary\" type=\"submit\"><span class=\"oi oi-file\"></span> Salvar</button></div>");
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
    }
}
namespace __Blazor.Laterza.Admin.Blazor.Pages.Empreendimento.EmpreendimentoInformacoesTecnicasFormComponent
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateInputSelect_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, TValue __arg1, int __seq2, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg2, int __seq3, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg3, int __seq4, global::Microsoft.AspNetCore.Components.RenderFragment __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputSelect<TValue>>(seq);
        __builder.AddAttribute(__seq0, "class", __arg0);
        __builder.AddAttribute(__seq1, "Value", __arg1);
        __builder.AddAttribute(__seq2, "ValueChanged", __arg2);
        __builder.AddAttribute(__seq3, "ValueExpression", __arg3);
        __builder.AddAttribute(__seq4, "ChildContent", __arg4);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
