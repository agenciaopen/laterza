#pragma checksum "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a3efa04c3c4b63cc667be5eb94767fb4b0ffee03"
// <auto-generated/>
#pragma warning disable 1591
namespace Laterza.Admin.Blazor.Pages.BeneficioPagamento
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Home;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using BlazorInputFile;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Empreendimento;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Escritorio;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Projeto;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.MdMidia;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.DragDrop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
using BlazorTable;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
using Laterza.Admin.Data.Models.BeneficioPagamento;

#line default
#line hidden
#nullable disable
    public partial class BeneficioPagamentoTableComponent : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<hr>\r\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "row mb-3 align-items-end");
            __builder.AddMarkupContent(3, "<div class=\"col-md-10\"><h5>Benefícios no Pagamento</h5></div>\r\n\r\n    ");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "col-md-2 text-right");
            __builder.OpenElement(6, "button");
            __builder.AddAttribute(7, "type", "button");
            __builder.AddAttribute(8, "class", "btn btn-lg btn-primary mr-2");
            __builder.AddAttribute(9, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 12 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                            (() => Create())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(10, "<i class=\"fas fa-plus\"></i>");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n\r\n");
            __builder.OpenElement(12, "div");
            __builder.AddAttribute(13, "class", "row");
            __builder.OpenElement(14, "div");
            __builder.AddAttribute(15, "class", "col-md-12");
            __builder.OpenComponent<BlazorTable.Table<IBeneficioPagamentoModel>>(16);
            __builder.AddAttribute(17, "Items", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.IEnumerable<IBeneficioPagamentoModel>>(
#nullable restore
#line 20 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                            modelList

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(18, "PageSize", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 20 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                 8

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(19, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<BlazorTable.Column<IBeneficioPagamentoModel>>(20);
                __builder2.AddAttribute(21, "Title", "Benefício");
                __builder2.AddAttribute(22, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IBeneficioPagamentoModel, System.Object>>>(
#nullable restore
#line 21 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                    x => x.BeneficioPagamento

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(23, "Sortable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 21 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                                                          true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(24, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 21 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                                                                            true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(25, "Width", "40%");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(26, "\r\n\r\n            ");
                __builder2.OpenComponent<BlazorTable.Column<IBeneficioPagamentoModel>>(27);
                __builder2.AddAttribute(28, "Title", "Ação");
                __builder2.AddAttribute(29, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IBeneficioPagamentoModel, System.Object>>>(
#nullable restore
#line 23 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                               x => x.Id

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(30, "Width", "10%");
                __builder2.AddAttribute(31, "Template", (Microsoft.AspNetCore.Components.RenderFragment<IBeneficioPagamentoModel>)((context) => (__builder3) => {
#nullable restore
#line 25 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                     if (idToDelete == context.Id)
                    {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(32, "button");
                    __builder3.AddAttribute(33, "type", "button");
                    __builder3.AddAttribute(34, "class", "btn btn-success mr-1 popoverDeleteConfirm");
                    __builder3.AddAttribute(35, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 27 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                                          () => DeleteBeneficio(context)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(36, "<i class=\"fas fa-check\"></i>");
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(37, "\r\n                        ");
                    __builder3.OpenElement(38, "button");
                    __builder3.AddAttribute(39, "type", "button");
                    __builder3.AddAttribute(40, "class", "btn btn-danger");
                    __builder3.AddAttribute(41, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 28 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                               () => idToDelete = Guid.Empty

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(42, "<i class=\"fas fa-times\"></i>");
                    __builder3.CloseElement();
#nullable restore
#line 29 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(43, "button");
                    __builder3.AddAttribute(44, "type", "button");
                    __builder3.AddAttribute(45, "class", "btn btn-success mr-1");
                    __builder3.AddAttribute(46, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 32 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                     () => Edit(context)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(47, "<i class=\"fas fa-pencil-alt\"></i>");
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(48, "\r\n                        ");
                    __builder3.OpenElement(49, "button");
                    __builder3.AddAttribute(50, "type", "button");
                    __builder3.AddAttribute(51, "class", "btn btn-danger");
                    __builder3.AddAttribute(52, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 33 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                               () => DeleteBeneficioConfirm(context)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(53, "<i class=\"fas fa-trash-alt\"></i>");
                    __builder3.CloseElement();
#nullable restore
#line 34 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                    }

#line default
#line hidden
#nullable disable
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(54, "\r\n            ");
                __builder2.OpenComponent<BlazorTable.Pager>(55);
                __builder2.AddAttribute(56, "ShowPageSizes", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 37 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                  false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(57, "ShowPageNumber", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 37 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                         false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(58, "ShowTotalCount", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 37 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                false

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(59, "\r\n\r\n");
            __builder.OpenElement(60, "div");
            __builder.AddAttribute(61, "class", "modal modal-fullscreen fade");
            __builder.AddAttribute(62, "id", "modal-form-BeneficioPagamento");
            __builder.AddAttribute(63, "data-backdrop", "static");
            __builder.AddAttribute(64, "data-keyboard", "false");
            __builder.AddAttribute(65, "tabindex", "-1");
            __builder.AddAttribute(66, "aria-labelledby", "staticBackdropLabel");
            __builder.AddAttribute(67, "aria-hidden", "true");
            __builder.OpenElement(68, "div");
            __builder.AddAttribute(69, "class", "modal-dialog");
            __builder.OpenElement(70, "div");
            __builder.AddAttribute(71, "class", "modal-content");
            __builder.AddMarkupContent(72, "<div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><i class=\"fas fa-times\"></i></button></div>\r\n            ");
            __builder.OpenElement(73, "div");
            __builder.AddAttribute(74, "class", "modal-body");
            __builder.OpenComponent<Laterza.Admin.Blazor.Pages.BeneficioPagamento.BeneficioPagamentoFormComponent>(75);
            __builder.AddAttribute(76, "ProcessarEventCallback", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, 
#nullable restore
#line 52 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                         HandleProcessarCallBack

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(77, "idEmpreendimento", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Guid>(
#nullable restore
#line 52 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                                                    IdEmpreendimento

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(78, "BeneficioPagamentoDetalhes", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.List<Laterza.Admin.Data.Models.BeneficioPagamento.BeneficioPagamentoModel>>(
#nullable restore
#line 52 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\BeneficioPagamento\BeneficioPagamentoTableComponent.razor"
                                                                                                                                                                  BeneficioPagamentoDetalhes

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
