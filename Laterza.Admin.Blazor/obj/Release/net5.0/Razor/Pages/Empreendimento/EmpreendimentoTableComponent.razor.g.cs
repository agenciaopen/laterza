#pragma checksum "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2692aa762e5beeebff3c66b562aa059916088a90"
// <auto-generated/>
#pragma warning disable 1591
namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Blazor.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Home;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using BlazorInputFile;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Empreendimento;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Escritorio;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.Projeto;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Models.MdMidia;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.Admin.Data.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\_Imports.razor"
using Laterza.DragDrop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
using BlazorTable;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
           [Authorize(Roles = "Administrator")]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Home/Empreendimento")]
    public partial class EmpreendimentoTableComponent : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "section");
            __builder.AddAttribute(1, "class", "d-flex flex-column tabela");
            __builder.AddMarkupContent(2, "<h3 class=\"mb-2 align-self-center\">Empreendimentos</h3>\r\n    ");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "d-flex flex-column");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "d-flex justify-content-between col-12");
            __builder.OpenElement(7, "div");
            __builder.AddAttribute(8, "class", "form-group");
            __builder.AddMarkupContent(9, "<h5 class=\"mb-2\"><i class=\"fas fa-filter mr-1\"></i>Filtrar por estado</h5>\r\n                ");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(10);
            __builder.AddAttribute(11, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                  model

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(12, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __Blazor.Laterza.Admin.Blazor.Pages.Empreendimento.EmpreendimentoTableComponent.TypeInference.CreateInputSelect_0(__builder2, 13, 14, Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 12 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                           HandleFirstDropDownChange

#line default
#line hidden
#nullable disable
                ), 15, "", 16, 
#nullable restore
#line 12 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                            model.Estado

#line default
#line hidden
#nullable disable
                , 17, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => model.Estado = __value, model.Estado)), 18, () => model.Estado, 19, (__builder3) => {
                    __builder3.AddMarkupContent(20, "<option selected value>Todos</option>");
#nullable restore
#line 14 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                         foreach (var e in estados)
                        {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(21, "option");
                    __builder3.AddAttribute(22, "value", 
#nullable restore
#line 16 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                             e.Slug

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddContent(23, 
#nullable restore
#line 16 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                        e.Nome

#line default
#line hidden
#nullable disable
                    );
                    __builder3.CloseElement();
#nullable restore
#line 17 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                        }

#line default
#line hidden
#nullable disable
                }
                );
            }
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.AddMarkupContent(24, "\r\n            ");
            __builder.OpenElement(25, "div");
            __builder.AddAttribute(26, "class", "align-self-end mb-2");
            __builder.OpenElement(27, "button");
            __builder.AddAttribute(28, "class", "btn btn-primary");
            __builder.AddAttribute(29, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 23 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                          (() => Create())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(30, "<i class=\"fa fa-plus\"></i> Novo\r\n                ");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n\r\n        ");
            __builder.OpenElement(32, "div");
            __builder.AddAttribute(33, "class", "col-12");
            __builder.OpenComponent<BlazorTable.Table<IEmpreendimentoModel>>(34);
            __builder.AddAttribute(35, "Items", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.IEnumerable<IEmpreendimentoModel>>(
#nullable restore
#line 30 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                            modelList

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(36, "PageSize", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 30 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                 30

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(37, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(38);
                __builder2.AddAttribute(39, "Title", "Imagem");
                __builder2.AddAttribute(40, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 32 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                 x => x.UrlBanner

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(41, "Width", "6%");
                __builder2.AddAttribute(42, "Template", (Microsoft.AspNetCore.Components.RenderFragment<IEmpreendimentoModel>)((context) => (__builder3) => {
#nullable restore
#line 35 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                         if (!string.IsNullOrEmpty(context.UrlBanner))
                    {

#line default
#line hidden
#nullable disable
                    __builder3.OpenComponent<Laterza.Admin.Blazor.Shared.ImgComponent>(43);
                    __builder3.AddAttribute(44, "LinkImagem", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 37 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                    context.UrlBanner.Replace(".jpeg", "_thumb.jpeg")

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(45, "Class", "table-img-mini");
                    __builder3.CloseComponent();
#nullable restore
#line 39 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                        }

#line default
#line hidden
#nullable disable
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(46, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(47);
                __builder2.AddAttribute(48, "Title", "");
                __builder2.AddAttribute(49, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 42 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                           x => x.Ativo

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(50, "Width", "6%");
                __builder2.AddAttribute(51, "Template", (Microsoft.AspNetCore.Components.RenderFragment<IEmpreendimentoModel>)((context) => (__builder3) => {
#nullable restore
#line 44 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                         if (context.Ativo)
                    {

#line default
#line hidden
#nullable disable
                    __builder3.AddMarkupContent(52, "<p class=\"font-weight-bolder success\">Ativo</p>");
#nullable restore
#line 47 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                        }
                    else
                    {

#line default
#line hidden
#nullable disable
                    __builder3.AddMarkupContent(53, "<p class=\"font-weight-bolder\">Inativo</p>");
#nullable restore
#line 51 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                        }

#line default
#line hidden
#nullable disable
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(54, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(55);
                __builder2.AddAttribute(56, "Title", "Titulo");
                __builder2.AddAttribute(57, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 54 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                 x => x.Nome

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(58, "Sortable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 54 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                                         true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(59, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 55 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(60, "Width", "37%");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(61, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(62);
                __builder2.AddAttribute(63, "Title", "UF");
                __builder2.AddAttribute(64, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 56 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                             x => x.EstadoNome

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(65, "Sortable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 56 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                                           true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(66, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 57 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(67, "Width", "10%");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(68, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(69);
                __builder2.AddAttribute(70, "Title", "Bairro");
                __builder2.AddAttribute(71, "Sortable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 58 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                  true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(72, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 58 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                                 x => x.Bairro

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(73, "Width", "10%");
                __builder2.AddAttribute(74, "Template", (Microsoft.AspNetCore.Components.RenderFragment<IEmpreendimentoModel>)((context) => (__builder3) => {
#nullable restore
#line 61 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                         if (context.Bairro != null)
                    {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(75, "p");
                    __builder3.AddContent(76, 
#nullable restore
#line 63 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                             System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(context.Bairro)

#line default
#line hidden
#nullable disable
                    );
                    __builder3.CloseElement();
#nullable restore
#line 64 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                        }

#line default
#line hidden
#nullable disable
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(77, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(78);
                __builder2.AddAttribute(79, "Title", "Status");
                __builder2.AddAttribute(80, "Sortable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 67 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                  true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(81, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 68 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                             x => x.StatusEmpreendimento

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(82, "Width", "10%");
                __builder2.AddAttribute(83, "Template", (Microsoft.AspNetCore.Components.RenderFragment<IEmpreendimentoModel>)((context) => (__builder3) => {
                    __builder3.OpenElement(84, "p");
                    __builder3.AddContent(85, 
#nullable restore
#line 70 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                             extensionMethods.Description((EnumStatusEmpreendimento)context.StatusEmpreendimento)

#line default
#line hidden
#nullable disable
                    );
                    __builder3.CloseElement();
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(86, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(87);
                __builder2.AddAttribute(88, "Title", "Data");
                __builder2.AddAttribute(89, "Sortable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 73 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(90, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 74 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                             x => x.DataAlteracao

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(91, "Width", "10%");
                __builder2.AddAttribute(92, "Template", (Microsoft.AspNetCore.Components.RenderFragment<IEmpreendimentoModel>)((context) => (__builder3) => {
                    __builder3.OpenElement(93, "p");
                    __builder3.AddContent(94, 
#nullable restore
#line 76 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                             context.DataAlteracao.ToString("dd/MM/yyyy")

#line default
#line hidden
#nullable disable
                    );
                    __builder3.CloseElement();
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(95, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Column<IEmpreendimentoModel>>(96);
                __builder2.AddAttribute(97, "Title", "Ação");
                __builder2.AddAttribute(98, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<IEmpreendimentoModel, System.Object>>>(
#nullable restore
#line 79 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                               x => x.Id

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(99, "Width", "10%");
                __builder2.AddAttribute(100, "Template", (Microsoft.AspNetCore.Components.RenderFragment<IEmpreendimentoModel>)((context) => (__builder3) => {
#nullable restore
#line 81 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                         if (modelList.Count > 0)
                    {
                        

#line default
#line hidden
#nullable disable
#nullable restore
#line 83 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                         if (idToDelete == context.Id)
                        {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(101, "div");
                    __builder3.AddAttribute(102, "class", "d-flex");
                    __builder3.OpenElement(103, "button");
                    __builder3.AddAttribute(104, "type", "button");
                    __builder3.AddAttribute(105, "class", "btn btn-success mr-1 popoverDeleteConfirm botao--icon");
                    __builder3.AddAttribute(106, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 87 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                          () => DeleteGaleria(context)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(107, "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>");
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(108, "\r\n                                ");
                    __builder3.OpenElement(109, "button");
                    __builder3.AddAttribute(110, "type", "button");
                    __builder3.AddAttribute(111, "class", "btn btn-danger botao--icon");
                    __builder3.AddAttribute(112, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 89 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                                   () => idToDelete = Guid.Empty

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(113, "<i class=\"fas fa-times\"></i>");
                    __builder3.CloseElement();
                    __builder3.CloseElement();
#nullable restore
#line 91 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                           
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(114, "div");
                    __builder3.AddAttribute(115, "class", "d-flex");
                    __builder3.OpenElement(116, "button");
                    __builder3.AddAttribute(117, "class", "btn btn-success botao--icon");
                    __builder3.AddAttribute(118, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 96 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                                                      () => Edit(context)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(119, "<i class=\"fa fa-pencil-alt\" aria-hidden=\"true\"></i>");
                    __builder3.CloseElement();
#nullable restore
#line 98 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                 if (modelList.Count > 0)
                            {

#line default
#line hidden
#nullable disable
                    __builder3.OpenElement(120, "button");
                    __builder3.AddAttribute(121, "type", "button");
                    __builder3.AddAttribute(122, "class", "btn btn-danger botao--icon");
                    __builder3.AddAttribute(123, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 101 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                          () => DeleteGaleriaConfirm(context)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(124, "<i class=\"fa fa-trash\" aria-hidden=\"true\"></i>");
                    __builder3.CloseElement();
#nullable restore
#line 103 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                            }

#line default
#line hidden
#nullable disable
                    __builder3.CloseElement();
#nullable restore
#line 105 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                            

                            
                        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 108 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                         
                        }

#line default
#line hidden
#nullable disable
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(125, "\r\n                ");
                __builder2.OpenComponent<BlazorTable.Pager>(126);
                __builder2.AddAttribute(127, "ShowPageNumber", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 119 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                       true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(128, "ShowTotalCount", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 119 "C:\workspace\ConstrutoraLaterza\Laterza.Admin.Blazor\Pages\Empreendimento\EmpreendimentoTableComponent.razor"
                                                             true

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
namespace __Blazor.Laterza.Admin.Blazor.Pages.Empreendimento.EmpreendimentoTableComponent
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateInputSelect_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4, int __seq5, global::Microsoft.AspNetCore.Components.RenderFragment __arg5)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputSelect<TValue>>(seq);
        __builder.AddAttribute(__seq0, "oninput", __arg0);
        __builder.AddAttribute(__seq1, "class", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.AddAttribute(__seq5, "ChildContent", __arg5);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
