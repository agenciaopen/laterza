﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Empreendimento;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoInformacoesTecnicasTableComponent : ComponentBase
    {
        public List<EmpreendimentoInformacoesTecnicasModel> modelList { get; set; }
        public List<EmpreendimentoInformacoesTecnicasModel> empreendimentoInformacoesTecnicasModelDetalhes { get; set; }
        
        [Parameter]
        public Guid idEmpreendimento { get; set; }
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoInformacoesTecnicasDataService empreendimentoInformacoesTecnicasDataService { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            modelList = empreendimentoInformacoesTecnicasDataService.GetEmpreendimentoInformacoesTecnicas(Guid.Empty, idEmpreendimento).Where(x => x.IdiomaId == "pt").ToList();
        }
        void Create()
        {
            empreendimentoInformacoesTecnicasModelDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Informacoes-Tecnicas");
            StateHasChanged();
        }

        private void DeleteGaleriaConfirm(IEmpreendimentoInformacoesTecnicasModel empreendimentoInformacoesTecnicasModel)
        {
            idToDelete = empreendimentoInformacoesTecnicasModel.Id;
            //JSRuntime.InvokeAsync<object>("showPopover", "popoverDeleteConfirm");
        }
        private void DeleteGaleria(IEmpreendimentoInformacoesTecnicasModel empreendimentoInformacoesTecnicasModel)
        {
            empreendimentoInformacoesTecnicasDataService.EmpreendimentoInformacoesTecnicasDelete(empreendimentoInformacoesTecnicasModel.Id);
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            ProcessarEventCallback.InvokeAsync();

        }

        void Edit(IEmpreendimentoInformacoesTecnicasModel empreendimentoInformacoesTecnicasModel)
        {
            empreendimentoInformacoesTecnicasModelDetalhes = null;

            empreendimentoInformacoesTecnicasModelDetalhes = empreendimentoInformacoesTecnicasDataService.GetEmpreendimentoInformacoesTecnicas(empreendimentoInformacoesTecnicasModel.Id, idEmpreendimento);

            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Informacoes-Tecnicas");
        }
        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }
    }
}
