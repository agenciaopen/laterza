﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Empreendimento;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoValorDescricaoTableComponent : ComponentBase
    {
        public List<EmpreendimentoValorDescricaoModel> modelList { get; set; }

        public List<EmpreendimentoValorDescricaoModel> empreendimentoValorDescricaoDetalhes { get; set; }


        [Parameter]
        public Guid idEmpreendimento { get; set; }

        [Parameter]
        public int TipoEmpreendimento { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IEmpreendimentoValorDescricaoDataService empreendimentoValorDescricaoDataService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        public Guid idToDelete { get; set; }


        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
        
        private void GetTableList()
        {
            modelList = empreendimentoValorDescricaoDataService.GetEmpreendimentoValorDescricao(Guid.Empty, idEmpreendimento).Where(x => x.IdiomaId == "pt").ToList();
        }

        void Create()
        {
            empreendimentoValorDescricaoDetalhes = null;

            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Valor-Descricao");
            StateHasChanged();
        }

        void Edit(IEmpreendimentoValorDescricaoModel empreendimentoValorDescricaoModel)
        {
            empreendimentoValorDescricaoDetalhes = empreendimentoValorDescricaoDataService.GetEmpreendimentoValorDescricao(empreendimentoValorDescricaoModel.Id, idEmpreendimento);

            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Valor-Descricao");
        }

        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        private void DeleteGaleriaConfirm(IEmpreendimentoValorDescricaoModel empreendimentoValorDescricaoModel)
        {
            idToDelete = empreendimentoValorDescricaoModel.Id;
        }
        private void DeleteGaleria(IEmpreendimentoValorDescricaoModel empreendimentoValorDescricaoModel)
        {
            empreendimentoValorDescricaoDataService.EmpreendimentoValorDescricaoDelete(empreendimentoValorDescricaoModel.Id);
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            HandleProcessarCallBack();
        }
        
    }
}
