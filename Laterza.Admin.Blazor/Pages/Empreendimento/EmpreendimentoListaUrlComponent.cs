﻿using Microsoft.AspNetCore.Components;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoListaUrlComponent : ComponentBase
    {
        public List<EmpreendimentoModel> modelList { get; set; }

        public List<EmpreendimentoModel> modelListSemQuartos { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoDataService empreendimentoDataService { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
        private void GetTableList()
        {
            modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt").OrderByDescending(x => x.DataAlteracao).OrderBy(y => y.Estado).ToList();
        }
    }
}
