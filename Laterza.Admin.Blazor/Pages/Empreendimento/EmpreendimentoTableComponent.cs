﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Empreendimento;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Data.Interfaces.Estados;
using Laterza.Admin.Data.Enum;
using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.Estados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Laterza.Admin.Data.Models.FichaTecnica;
using Laterza.Admin.Data.Data.Interfaces.FichaTecnica;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using Laterza.Admin.Data.Data.Interfaces.BeneficioPagamento;

namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoTableComponent : ComponentBase
    {
        public List<EmpreendimentoModel> modelList { get; set; }
        public List<EstadosModel> estados { get; set; }

        private EmpreendimentoModelForm model = new EmpreendimentoModelForm();

        public Guid idToDelete { get; set; }

        [Inject]
        IEmpreendimentoDataService empreendimentoDataService { get; set; }

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        IEmpreendimentoGaleriaDataService empreendimentoGaleriaDataService { get; set; }

        [Inject]
        IEmpreendimentoInformacoesTecnicasDataService empreendimentoInformacoesTecnicasDataService { get; set; }

        [Inject]
        IEmpreendimentoValorDescricaoDataService empreendimentoValorDescricaoDataService { get; set; }
        [Inject]
        IFichaTecnicaDataService FichaTecnicaDataService { get; set; }
        [Inject]
        IBeneficioPagamentoDataService BeneficioPagamentoDataService { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }
        protected override void OnParametersSet()
        {
            GetTableList();
            estados = estadosDataService.GetEstados();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt").OrderByDescending(x => x.DataAlteracao).ToList();
        }

        void Create()
        {
            NavigationManager.NavigateTo("Home/Empreendimento/novo");
        }

        private void Edit(IEmpreendimentoModel empreendimento)
        {
            
            NavigationManager.NavigateTo("Home/Empreendimento/editar/" + empreendimento.Id);
            //JSRuntime.InvokeAsync<object>("showModal", "modal-form-banner");
        }

        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Value.ToString()))
            {
                modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt").ToList();
            } else
            {
                modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.Estado == e.Value.ToString() && x.IdiomaId == "pt").ToList();
            }
            StateHasChanged();
        }
        private void DeleteGaleriaConfirm(IEmpreendimentoModel empreendimentoModel)
        {
            idToDelete = empreendimentoModel.Id;
        }
        private void DeleteGaleria(IEmpreendimentoModel empreendimentoModel)
        {
            var modelListGaleriaImovel = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoGaleria.Imovel).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var galeriaImovel in modelListGaleriaImovel)
            {
                if (!galeriaImovel.EhVideo)
                {
                    blobService.DeleteBlobAsync(galeriaImovel.UrlImagem);
                }
                empreendimentoGaleriaDataService.EmpreendimentoGaleriaDelete(galeriaImovel.Id);
            }

            var modelListGaleriaPlanta = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoGaleria.Planta).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var galeriaPlanta in modelListGaleriaPlanta)
            {
                if (!galeriaPlanta.EhVideo)
                {
                    blobService.DeleteBlobAsync(galeriaPlanta.UrlImagem);
                }
                empreendimentoGaleriaDataService.EmpreendimentoGaleriaDelete(galeriaPlanta.Id);
            }

            var modelListGaleriaStatusDaObra = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoGaleria.StatusDaObra).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var galeriaStatusDaObra in modelListGaleriaStatusDaObra)
            {
                if (!galeriaStatusDaObra.EhVideo)
                {
                    blobService.DeleteBlobAsync(galeriaStatusDaObra.UrlImagem);
                }
                empreendimentoGaleriaDataService.EmpreendimentoGaleriaDelete(galeriaStatusDaObra.Id);
            }

            var modelListInformacoesTecnicas = empreendimentoInformacoesTecnicasDataService.GetEmpreendimentoInformacoesTecnicas(Guid.Empty, empreendimentoModel.Id).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var informacoesTecnicas in modelListInformacoesTecnicas)
            {
                empreendimentoInformacoesTecnicasDataService.EmpreendimentoInformacoesTecnicasDelete(informacoesTecnicas.Id);
            }

            var modelListValorDescricao = empreendimentoValorDescricaoDataService.GetEmpreendimentoValorDescricao(Guid.Empty, empreendimentoModel.Id).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var valorDescricao in modelListValorDescricao)
            {
                empreendimentoValorDescricaoDataService.EmpreendimentoValorDescricaoDelete(valorDescricao.Id);
            }
            if (!string.IsNullOrEmpty(empreendimentoModel.UrlBanner))
            {
                blobService.DeleteBlobAsync(empreendimentoModel.UrlBanner);
            }
            if (!string.IsNullOrEmpty(empreendimentoModel.UrlImagemCampanha))
            {
                blobService.DeleteBlobAsync(empreendimentoModel.UrlImagemCampanha);
            }
            if (!string.IsNullOrEmpty(empreendimentoModel.UrlLogo))
            {
                blobService.DeleteBlobAsync(empreendimentoModel.UrlLogo);
            }
            List<Guid> modelListEmpreendimentoRelacionado = empreendimentoDataService.GetEmpreendimentoRelacionado(empreendimentoModel.Id).ToList();
            foreach (var idEmreendimentoRelacionado in modelListEmpreendimentoRelacionado)
            {
                empreendimentoDataService.EmpreendimentoRelacionadoDelete(empreendimentoModel.Id, idEmreendimentoRelacionado);
            }

            List<FichaTecnicaModel> fichaTecnicaModels = FichaTecnicaDataService.GetFichaTecnica(new Guid(), empreendimentoModel.Id);
            foreach(var fichaTecnica in fichaTecnicaModels)
            {
                FichaTecnicaDataService.FichaTecnicaDelete(fichaTecnica.Id);
            }
            List<BeneficioPagamentoModel> beneficioPagamentoModels = BeneficioPagamentoDataService.GetBeneficioPagamento(new Guid(), empreendimentoModel.Id);
            foreach(var beneficioPagamento in beneficioPagamentoModels)
            {
                BeneficioPagamentoDataService.BeneficioPagamentoDelete(beneficioPagamento.Id);
            }
            empreendimentoDataService.EmpreendimentoDelete(idToDelete);
            GetTableList();
            StateHasChanged();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
        }
    }
}
