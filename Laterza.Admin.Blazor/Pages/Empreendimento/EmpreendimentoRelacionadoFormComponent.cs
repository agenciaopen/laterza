﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoRelacionadoFormComponent : ComponentBase
    {

        [Parameter]
        public Guid idEmpreendimento { get; set; }

        private Guid model = Guid.NewGuid();

        [Parameter]
        public List<Guid> modelListEmpreendimentoRelacionado { get; set; }


        public List<EmpreendimentoModel> modelList { get; set; }


        [Inject]
        IEmpreendimentoDataService empreendimentoDataService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && !modelListEmpreendimentoRelacionado.Contains(x.Id)).OrderBy(x => x.Estado).ThenBy(m => m.Bairro).ToList();
        }

      

        private void HandleValidSubmit()
        {
            empreendimentoDataService.EmpreendimentoRelacionadoCreate(idEmpreendimento, model);
            JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Empreendimento-Relacionado");
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            ProcessarEventCallback.InvokeAsync();
        }
    }
}
