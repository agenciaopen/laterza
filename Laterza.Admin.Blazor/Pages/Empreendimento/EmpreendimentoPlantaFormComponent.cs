﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models;
using Laterza.Admin.Blazor.Models.Empreendimento;
using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Enum;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoPlantaFormComponent : ComponentBase
    {
        private EmpreendimentoGaleriaModelForm model = new EmpreendimentoGaleriaModelForm();


        [Parameter]
        public List<EmpreendimentoGaleriaModel> empreendimentoGaleriaDetalhes { get; set; }

        [Parameter]
        public int tipoGaleria { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]

        public Guid idEmpreendimento { get; set; }


        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        IIdiomaDataService idiomaDataService { get; set; }


        [Inject]
        IEmpreendimentoGaleriaDataService empreendimentoGaleriaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Inject]
        NavigationManager uriHelper { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            alreadySave = false;

            if (empreendimentoGaleriaDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new EmpreendimentoGaleriaModelForm { };
                model.IdiomaId = idioma;
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = empreendimentoGaleriaDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<EmpreendimentoGaleriaModelForm>(modelSelected);
            }
        }
        private void HandleValidSubmit()
        {
            try
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modal = (EnumTipoGaleria)tipoGaleria;

                var modelSave = _mapper.Map<EmpreendimentoGaleriaModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    if (!modelSave.EhVideo)
                    {
                        var pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo + "_galeria", "/galeria/" + tipoGaleria, id);
                        blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);

                        modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                    }
                    modelSave.Id = id;
                    modelSave.IdEmpreendimento = idEmpreendimento;
                    modelSave.TipoGaleria = tipoGaleria;
                    empreendimentoGaleriaDataService.EmpreendimentoGaleriaCreate(modelSave);
                }
                else
                {
                    var pathUrlImagem = "";

                    if (!modelSave.EhVideo)
                    {
                        if (modelSave.UrlImagem != null && modelSave.UrlImagem.Contains("data:image/jpeg"))
                        {
                            pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo, "/galeria/" + tipoGaleria, modelSave.Id);

                            var urlBannerAntiga = empreendimentoGaleriaDetalhes.FirstOrDefault().UrlImagem;
                            if (!string.IsNullOrEmpty(urlBannerAntiga))
                            {
                                blobService.DeleteBlobAsync(urlBannerAntiga);
                            }
                            blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);
                            modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                        }
                    }
                    empreendimentoGaleriaDataService.EmpreendimentoGaleriaUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-" + modal);
                ProcessarEventCallback.InvokeAsync();
            }
            catch (Exception e)
            {
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = e.Message, tipo = "error" });
            }


        }
        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlImagem = imagem;
            }
            fileUploaded = true;
        }
    }
}
