﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Empreendimento;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Data.Interfaces.Estados;
using Laterza.Admin.Data.Enum;
using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.Estados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Laterza.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoRelacionadoTableComponent : ComponentBase
    {

        [Parameter]
        public Guid idEmpreendimento { get; set; }
        public List<EmpreendimentoModel> modelList { get; set; }
        public List<Guid> modelListEmpreendimentoRelacionado { get; set; }

        public Guid idToDelete { get; set; }

        [Inject]
        IEmpreendimentoDataService empreendimentoDataService { get; set; }

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }
        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            modelListEmpreendimentoRelacionado = empreendimentoDataService.GetEmpreendimentoRelacionado(idEmpreendimento).ToList();
            modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && modelListEmpreendimentoRelacionado.Contains(x.Id)).OrderByDescending(x => x.DataAlteracao).ToList();
        }

        void Create()
        {
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Empreendimento-Relacionado");
        }

       
        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Value.ToString()))
            {
                modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt").ToList();
            }
            else
            {
                modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.Estado == e.Value.ToString() && x.IdiomaId == "pt").ToList();
            }
            StateHasChanged();
        }
        private void DeleteGaleriaConfirm(IEmpreendimentoModel empreendimentoModel)
        {
            idToDelete = empreendimentoModel.Id;
        }
        private void DeleteGaleria(IEmpreendimentoModel empreendimentoModel)
        {
            empreendimentoDataService.EmpreendimentoRelacionadoDelete(idEmpreendimento, empreendimentoModel.Id);
            GetTableList();
            StateHasChanged();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
        }
    }
}
