﻿using Laterza.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Data.Data;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Home;

namespace Laterza.Admin.Blazor.Pages.Home.Banner
{
    public partial class HomeBannerTableComponent : ComponentBase
    {

        private IHomeBannerModel banner = new HomeBannerModelForm();
        public List<HomeBannerModel> modelDetalhes { get; set; }
        public List<HomeBannerModel> modelList { get; set; }
        public Guid idToDelete { get; set; }
        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IHomeDataService HomeBannerDataService { get; set; }

        [Inject]
        IIdiomaDataService idiomaDataService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }
        [Inject]
        NavigationManager NavigationManager { get; set; }
        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            modelList = HomeBannerDataService.GetBanner().OrderByDescending(x => x.DataAlteracao).ToList();
        }

        private void DeleteConfirm(IHomeBannerModel banner)
        {
            idToDelete = banner.Id;
        }

        private void Delete(IHomeBannerModel banner)
        {
            if (!string.IsNullOrEmpty(banner.UrlImagem))
            {
                blobService.DeleteBlobAsync(banner.UrlImagem);
            }

            HomeBannerDataService.HomeBannerDelete(banner.Id);

            GetTableList();
            StateHasChanged();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Exclusão feita com sucesso.", tipo = "ok" });
        }
        private void CreateBanner()
        {
            NavigationManager.NavigateTo("Home/banner/novo");
        }

        private void Edit(IHomeBannerModel banner)
        {
            NavigationManager.NavigateTo("Home/banner/editar/" + banner.Id);
        }
    }
}
