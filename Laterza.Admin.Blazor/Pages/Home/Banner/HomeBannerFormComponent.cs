﻿using Microsoft.AspNetCore.Components;
using Laterza.Admin.Blazor.Models;
using Laterza.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Microsoft.Extensions.Configuration;
using Laterza.Admin.Data.Data;
using Laterza.Admin.Blazor.Models.Home;
using AutoMapper;

namespace Laterza.Admin.Blazor.Pages.Home.Banner
{
    public partial class HomeBannerFormComponent : ComponentBase
    {

        private HomeBannerModelForm model = new HomeBannerModelForm();

        [Parameter]
        public List<HomeBannerModel> ModelDetalhes { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IHomeDataService HomeBannerDataService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Parameter]
        public EventCallback OnSave { get; set; }
        [Parameter]
        public string Id { get; set; }
        [Inject]
        IMapper _mapper { get; set; }

        private void HandleProcessarImagemCallBack(string imagem)
        {
            model.UrlImagem = imagem;
            fileUploaded = true;
        }

        private void setFields()
        {
            if (Id != null)
            {
                ModelDetalhes = HomeBannerDataService.GetBanner(Guid.Parse(Id));

                var modelSelected = ModelDetalhes.FirstOrDefault();
                modelSelected.UrlImagem = fileUploaded ? model.UrlImagem : modelSelected.UrlImagem;

                model = _mapper.Map<HomeBannerModelForm>(modelSelected);

            }
        }

        private async Task HandleValidSubmit()
        {
            var modelSave = _mapper.Map<HomeBannerModelForm>(model);

            try
            {
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    modelSave.Id = id;
                    modelSave.DataAlteracao = DateTime.Now;
                    modelSave.DataCriacao = DateTime.Now;

                    await HomeBannerDataService.HomeBannerCreate(modelSave);

                    NavigationManager.NavigateTo("Home/banner/editar/" + modelSave.Id);
                }
                else 
                {
                    ModelDetalhes = HomeBannerDataService.GetBanner(Guid.Parse(Id));
                    JSRuntime.InvokeAsync<object>("disableButtons", "");
                    var pathBanner = "";
                    var pathLogo = "";

                    if (modelSave.UrlImagem != null && modelSave.UrlImagem.Contains("data:image/jpeg"))
                    {
                        pathBanner = extensionMethods.CreatePathBlobFile(modelSave.Titulo + "_banner", "/banner", model.Id);
                        var urlBannerAntiga = ModelDetalhes.FirstOrDefault().UrlImagem;
                        if (!string.IsNullOrEmpty(urlBannerAntiga))
                        {
                            blobService.DeleteBlobAsync(urlBannerAntiga);
                        }
                        blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathBanner);
                        modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathBanner;
                    }

                    JSRuntime.InvokeAsync<object>("enableButtons", "");

                    modelSave.DataAlteracao = DateTime.Now;
                    HomeBannerDataService.HomeBannerUpdate(modelSave);
                    StateHasChanged();
                }

                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Banner Salvo com sucesso.", tipo = "ok" });
            }
            catch (Exception e)
            {
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = e.Message, tipo = "error" });
            }
        }

        protected override void OnParametersSet()
        {
            setFields();

            base.OnParametersSet();
        }
    }
}
