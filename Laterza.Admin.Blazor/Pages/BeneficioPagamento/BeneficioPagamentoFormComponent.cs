﻿using AutoMapper;
using AutoMapper.Configuration;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.BeneficioPagamento;
using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Data.Interfaces.BeneficioPagamento;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.BeneficioPagamento
{
    public partial class BeneficioPagamentoFormComponent
    {
        private BeneficioPagamentoModelForm model = new BeneficioPagamentoModelForm();
        public string[] beneficios { get; set; }

        [Parameter]
        public List<BeneficioPagamentoModel> BeneficioPagamentoDetalhes { get; set; }


        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]

        public Guid idEmpreendimento { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IBeneficioPagamentoDataService BeneficioPagamentoDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }


        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            alreadySave = false;

            if (BeneficioPagamentoDetalhes == null)
            {
                model = new BeneficioPagamentoModelForm { };
                model.IdiomaId = idioma;
            }
            else
            {
                var modelSelected = BeneficioPagamentoDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<BeneficioPagamentoModelForm>(modelSelected);
            }
        }
        private void HandleValidSubmit()
        {
            try
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<BeneficioPagamentoModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                   
                    modelSave.Id = id;
                    modelSave.IdEmpreendimento = idEmpreendimento;
                    BeneficioPagamentoDataService.BeneficioPagamentoCreate(modelSave);
                }
                else
                {
                    BeneficioPagamentoDataService.BeneficioPagamentoUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-BeneficioPagamento");
                ProcessarEventCallback.InvokeAsync();
            }
            catch (Exception e)
            {
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = e.Message, tipo = "error" });
            }
        }

        private async Task<IEnumerable<string>> buscarBeneficios(string palavraChave)
        {
            var result = await Task.FromResult(beneficios.Where(x => x.ToLower().Contains(palavraChave.ToLower())).ToList());

            if (result.Count == 0)
            {
                string[] strArrPalavraChave = new string[] { palavraChave };
                result = await Task.FromResult(strArrPalavraChave.ToList());
            }

            return result;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                JSRuntime.InvokeVoidAsync("initialize", null);

                StateHasChanged();
            }
            beneficios = BeneficioPagamentoDataService.GetBeneficioPagamento().Where(x => x.BeneficioPagamento != null).Select(p => p.BeneficioPagamento).Distinct().ToArray();
        }
    }
}
