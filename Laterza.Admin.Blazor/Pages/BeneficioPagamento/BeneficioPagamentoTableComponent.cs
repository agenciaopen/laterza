﻿using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Data.Data.Interfaces.BeneficioPagamento;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.BeneficioPagamento
{
    public partial class BeneficioPagamentoTableComponent
    {
        public List<BeneficioPagamentoModel> modelList { get; set; }
        public List<BeneficioPagamentoModel> BeneficioPagamentoDetalhes { get; set; }

        [Parameter]
        public Guid IdEmpreendimento { get; set; }
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IBeneficioPagamentoDataService BeneficioPagamentoDataService { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
        private void GetTableList()
        {
            modelList = BeneficioPagamentoDataService.GetBeneficioPagamento(Guid.Empty, IdEmpreendimento).Where(B => B.IdiomaId == "pt").ToList();
        }

        void Create()
        {
            BeneficioPagamentoDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-BeneficioPagamento");
            StateHasChanged();
        }

        void Edit(IBeneficioPagamentoModel BeneficioPagamento)
        {
            BeneficioPagamentoDetalhes = BeneficioPagamentoDataService.GetBeneficioPagamento(BeneficioPagamento.Id, IdEmpreendimento);

            JSRuntime.InvokeAsync<object>("showModal", "modal-form-BeneficioPagamento");
        }

        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        private void DeleteBeneficioConfirm(IBeneficioPagamentoModel BeneficioPagamento)
        {
            idToDelete = BeneficioPagamento.Id;
        }
        private void DeleteBeneficio(IBeneficioPagamentoModel BeneficioPagamento)
        {
            BeneficioPagamentoDataService.BeneficioPagamentoDelete(BeneficioPagamento.Id);
            ProcessarEventCallback.InvokeAsync();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Exclusão feita com sucesso.", tipo = "ok" });
        }
    }
}
