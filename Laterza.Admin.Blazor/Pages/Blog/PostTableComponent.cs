﻿using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Blog;
using Laterza.Admin.Data.Data.Blog;
using Laterza.Admin.Data.Data.Interfaces.Blog;
using Laterza.Admin.Data.Models.Blog;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Blog
{
    public partial class PostTableComponent
    {
        public List<PostModel> modelList { get; set; }
        private PostModelForm model = new PostModelForm();
        public Guid idToDelete { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IPostDataService PostDataService { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IPostGaleriaDataService PostGaleriaDataService { get; set; }

        void Create()
        {
            NavigationManager.NavigateTo("Home/Publicacao/novo");
        }

        private void Edit(IPostModel post)
        {
            NavigationManager.NavigateTo("Home/Publicacao/editar/" + post.Id);
            //JSRuntime.InvokeAsync<object>("showModal", "modal-form-banner");
        }

        private void DeleteConfirm(IPostModel post)
        {
            idToDelete = post.Id;
        }

        private async Task Delete(IPostModel post)
        {
            if (!string.IsNullOrEmpty(post.UrlImagemDestaque))
            {
                blobService.DeleteBlobAsync(post.UrlImagemDestaque);
            }

            var postGalerias = PostGaleriaDataService.GetPostGaleria(Guid.Empty, post.Id).ToList();
            foreach (var postGaleria in postGalerias)
            {
                if (!postGaleria.EhVideo)
                {
                    blobService.DeleteBlobAsync(postGaleria.UrlImagem);
                }
                PostGaleriaDataService.PostGaleriaDelete(postGaleria.Id);
            }

            await PostDataService.PostDelete(idToDelete);
            GetTableList();
            StateHasChanged();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
        }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            modelList = PostDataService.GetPost().OrderByDescending(x => x.DataAlteracao).ToList();
        }
    }
}
