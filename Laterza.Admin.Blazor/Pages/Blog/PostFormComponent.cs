﻿using AutoMapper;
using Blazored.TextEditor;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Blog;
using Laterza.Admin.Data.Data.Blog;
using Laterza.Admin.Data.Data.Interfaces.Blog;
using Laterza.Admin.Data.Models.Blog;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Blog
{
    public partial class PostFormComponent
    {
        [Parameter]
        public String id { get; set; }

        public string[] categorias{ get; set; }

        public List<PostModel> modelDetalhes { get; set; }

        PostModelForm model = new PostModelForm();
        public bool fileUploaded = false;
        public bool novoRegistro = true;

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IPostDataService PostDataService { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        BlazoredTextEditor QuillHtml;
        string QuillHTMLContent;


        private async Task HandleValidSubmit()
        {
            model.Conteudo = await this.QuillHtml.GetHTML();
            var modelSave = _mapper.Map<PostModel>(model);

            if (modelSave.Id == Guid.Empty)
            {
                var id = Guid.NewGuid();
                modelSave.Id = id;
                modelSave.Slug = extensionMethods.CreateSlug(modelSave.Titulo);
                modelSave.DataAlteracao = DateTime.Now;
                modelSave.DataCriacao = DateTime.Now;

                GerarUrlImagem(modelSave);

                PostDataService.PostCreate(modelSave);

                NavigationManager.NavigateTo("Home/Publicacao/editar/" + modelSave.Id);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Publicação feita com sucesso.", tipo = "ok" });
            }
            else
            {
                modelDetalhes = PostDataService.GetPost(Guid.Parse(id));
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                if (modelSave.UrlImagemDestaque != null && modelSave.UrlImagemDestaque.Contains("data:image/jpeg"))
                {
                    var urlBannerAntiga = modelDetalhes.FirstOrDefault().UrlImagemDestaque;
                    if (!string.IsNullOrEmpty(urlBannerAntiga))
                    {
                        blobService.DeleteBlobAsync(urlBannerAntiga);
                    }
                    GerarUrlImagem(modelSave);
                }

                JSRuntime.InvokeAsync<object>("enableButtons", "");

                modelSave.DataAlteracao = DateTime.Now;
                modelSave.Slug = extensionMethods.CreateSlug(modelSave.Titulo);
                PostDataService.PostUpdate(modelSave);
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                StateHasChanged();
            }
            
        }

        private void GerarUrlImagem(PostModel modelSave)
        {
            string pathBanner = extensionMethods.CreatePathBlobFile(modelSave.Titulo + "_banner", "/banner", modelSave.Id);
            blobService.UploadBlobFileAsync(modelSave.UrlImagemDestaque, pathBanner);
            modelSave.UrlImagemDestaque = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathBanner;
        }

        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlImagemDestaque = imagem;
            }
            fileUploaded = true;
        }

        protected override void OnParametersSet()
        {
            setFields();
            StateHasChanged();
            base.OnParametersSet();
        }

        private void setFields()
        {
            if(id != null)
            {
                modelDetalhes = PostDataService.GetPost(Guid.Parse(id));

                var modelSelected = modelDetalhes.FirstOrDefault();
                modelSelected.UrlImagemDestaque = fileUploaded ? model.UrlImagemDestaque : modelSelected.UrlImagemDestaque;

                model = _mapper.Map<PostModelForm>(modelSelected);
                novoRegistro = false;
                
            }
        }

        private void HandleStateChange()
        {
            StateHasChanged();
        }

        private async Task<IEnumerable<string>> buscarCategorias(string palavraChave)
        {
            var result = await Task.FromResult(categorias.Where(x => x.ToLower().Contains(palavraChave.ToLower())).ToList());

            if (result.Count == 0)
            {
                string[] strArrPalavraChave = new string[] { palavraChave };
                result = await Task.FromResult(strArrPalavraChave.ToList());
            }

            return result;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                JSRuntime.InvokeVoidAsync("initialize", null);
                
                StateHasChanged();
            }
            categorias = PostDataService.GetPost().Where(x => x.Categoria != null).Select(p => p.Categoria).Distinct().ToArray();
        }
    }
}
