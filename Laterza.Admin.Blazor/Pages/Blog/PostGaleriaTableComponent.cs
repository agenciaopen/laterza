﻿using AutoMapper;
using AutoMapper.Configuration;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Blog;
using Laterza.Admin.Data.Data.Interfaces.Blog;
using Laterza.Admin.Data.Models.Blog;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Blog
{
    public partial class PostGaleriaTableComponent : ComponentBase
    {
        public List<PostGaleriaModel> modelList { get; set; }
        public List<PostGaleriaModel> PostGaleriaDetalhes { get; set; }

        [Parameter]
        public Guid IdPost { get; set; }
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IPostGaleriaDataService PostGaleriaDataService { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
        private void GetTableList()
        {
            modelList = PostGaleriaDataService.GetPostGaleria(Guid.Empty, IdPost).ToList();
        }

        void Create()
        {
            PostGaleriaDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-postGaleria");
            StateHasChanged();
        }

        void Order()
        {
            JSRuntime.InvokeAsync<object>("showModal", "modal-galeria-order-post");
        }
        void Edit(IPostGaleriaModel postGaleria)
        {
            PostGaleriaDetalhes = PostGaleriaDataService.GetPostGaleria(postGaleria.Id, IdPost);

            JSRuntime.InvokeAsync<object>("showModal", "modal-form-postGaleria");
        }

        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        private void DeleteGaleriaConfirm(IPostGaleriaModel postGaleria)
        {
            idToDelete = postGaleria.Id;
            //JSRuntime.InvokeAsync<object>("showPopover", "popoverDeleteConfirm");
        }
        private void DeleteGaleria(IPostGaleriaModel postGaleria)
        {
            PostGaleriaDataService.PostGaleriaDelete(postGaleria.Id);
            if (!postGaleria.EhVideo)
            {
                blobService.DeleteBlobAsync(postGaleria.UrlImagem);
            }
            ProcessarEventCallback.InvokeAsync();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
        }
    }
}

