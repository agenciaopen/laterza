﻿using AutoMapper;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Blog;
using Laterza.Admin.Data.Data.Interfaces.Blog;
using Laterza.Admin.Data.Models.Blog;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Blog
{
    public partial class PostGaleriaFormComponent : ComponentBase
    {
        private PostGaleriaModelForm model = new PostGaleriaModelForm();


        [Parameter]
        public List<PostGaleriaModel> PostGaleriaDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]
        public Guid IdPost { get; set; }

        public bool fileUploaded = false;

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        IPostGaleriaDataService PostGaleriaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Inject]
        NavigationManager uriHelper { get; set; }

        protected override void OnParametersSet()
        {
            SetFields();
            base.OnParametersSet();
        }

        private void SetFields()
        {
            alreadySave = false;

            if (PostGaleriaDetalhes == null)
            {
                model = new PostGaleriaModelForm { };
            }
            else
            {
                var modelSelected = PostGaleriaDetalhes.FirstOrDefault();
                model = _mapper.Map<PostGaleriaModelForm>(modelSelected);
            }
        }
        private void HandleValidSubmit()
        {
            try
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<PostGaleriaModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    if (!modelSave.EhVideo)
                    {
                        var pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo + "_galeria", "/galeria/post", id);
                        blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);

                        modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                    }
                    modelSave.Id = id;
                    modelSave.IdPost = IdPost;
                    PostGaleriaDataService.PostGaleriaCreate(modelSave);
                }
                else
                {
                    var pathUrlImagem = "";

                    if (!modelSave.EhVideo)
                    {
                        if (modelSave.UrlImagem != null && modelSave.UrlImagem.Contains("data:image/jpeg"))
                        {
                            pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo, "/galeria/post", modelSave.Id);

                            var urlBannerAntiga = PostGaleriaDetalhes.FirstOrDefault().UrlImagem;
                            if (!string.IsNullOrEmpty(urlBannerAntiga))
                            {
                                blobService.DeleteBlobAsync(urlBannerAntiga);
                            }
                            blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);
                            modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                        }
                    }
                    PostGaleriaDataService.PostGaleriaUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-postGaleria");
                ProcessarEventCallback.InvokeAsync();
            }
            catch (Exception e)
            {
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = e.Message, tipo = "error" });
            }


        }
        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlImagem = imagem;
            }
            fileUploaded = true;
        }
    }
}
