﻿using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Data.Data.Interfaces.FichaTecnica;
using Laterza.Admin.Data.Models.FichaTecnica;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.FichaTecnica
{
    public partial class FichaTecnicaTableComponent
    {
        public List<FichaTecnicaModel> modelList { get; set; }
        public List<FichaTecnicaModel> FichaTecnicaDetalhes { get; set; }

        [Parameter]
        public Guid IdEmpreendimento { get; set; }
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IFichaTecnicaDataService FichaTecnicaDataService { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
        private void GetTableList()
        {
            modelList = FichaTecnicaDataService.GetFichaTecnica(Guid.Empty, IdEmpreendimento).Where(B => B.IdiomaId == "pt").ToList();
        }

        void Create()
        {
            FichaTecnicaDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-FichaTecnica");
            StateHasChanged();
        }

        void Edit(IFichaTecnicaModel FichaTecnica)
        {
            FichaTecnicaDetalhes = FichaTecnicaDataService.GetFichaTecnica(FichaTecnica.Id, IdEmpreendimento);

            JSRuntime.InvokeAsync<object>("showModal", "modal-form-FichaTecnica");
        }

        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        private void DeleteFichaTecnicaConfirm(IFichaTecnicaModel FichaTecnica)
        {
            idToDelete = FichaTecnica.Id;
        }
        private void DeleteFichaTecnica(IFichaTecnicaModel FichaTecnica)
        {
            FichaTecnicaDataService.FichaTecnicaDelete(FichaTecnica.Id);
            ProcessarEventCallback.InvokeAsync();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Exclusão feita com sucesso.", tipo = "ok" });
        }
    }
}
