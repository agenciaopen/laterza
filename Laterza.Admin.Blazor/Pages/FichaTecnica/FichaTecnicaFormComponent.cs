﻿using AutoMapper;
using Laterza.Admin.Blazor.Models.FichaTecnica;
using Laterza.Admin.Data.Data.Interfaces.FichaTecnica;
using Laterza.Admin.Data.Models.FichaTecnica;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.FichaTecnica
{
    public partial class FichaTecnicaFormComponent
    {
        private FichaTecnicaModelForm model = new FichaTecnicaModelForm();
        public string[] fichasTecnicas { get; set; }

        [Parameter]
        public List<FichaTecnicaModel> FichaTecnicaDetalhes { get; set; }


        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]

        public Guid idEmpreendimento { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IFichaTecnicaDataService FichaTecnicaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }


        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            alreadySave = false;

            if (FichaTecnicaDetalhes == null)
            {
                model = new FichaTecnicaModelForm { };
                model.IdiomaId = idioma;
            }
            else
            {
                var modelSelected = FichaTecnicaDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<FichaTecnicaModelForm>(modelSelected);
            }
        }
        private void HandleValidSubmit()
        {
            try
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                FichaTecnicaModel modelSave = _mapper.Map<FichaTecnicaModel>(model);
                if(modelSave.Destacar && AlcancouLimiteDeDestaques(modelSave))
                {
                    JSRuntime.InvokeAsync<object>("enableButtons", "");
                    JSRuntime.InvokeAsync<object>("sendMessage", 
                        new { mensagem = "Apenas 3 itens podem ser destacados!", tipo = "error" });
                    return;
                }
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();

                    modelSave.Id = id;
                    modelSave.IdEmpreendimento = idEmpreendimento;
                    FichaTecnicaDataService.FichaTecnicaCreate(modelSave);
                }
                else
                    FichaTecnicaDataService.FichaTecnicaUpdate(modelSave);

                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-FichaTecnica");
                ProcessarEventCallback.InvokeAsync();
            }
            catch (Exception e)
            {
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = e.Message, tipo = "error" });
            }
        }

        private bool AlcancouLimiteDeDestaques(FichaTecnicaModel model)
        {
            if (model.Destacar)
            {
                List<FichaTecnicaModel> fichaTecnicaModels = FichaTecnicaDataService.GetFichaTecnica(new Guid(), idEmpreendimento)
                                                            .Where(F => F.Destacar && F.IdiomaId == "pt")
                                                            .ToList();

                return fichaTecnicaModels.Count >= 3;
            }
            return false;
        }

        private async Task<IEnumerable<string>> BuscarFichasTecnicas(string palavraChave)
        {
            var result = await Task.FromResult(fichasTecnicas.Where(x => x.ToLower().Contains(palavraChave.ToLower())).ToList());

            if (result.Count == 0)
            {
                string[] strArrPalavraChave = new string[] { palavraChave };
                result = await Task.FromResult(strArrPalavraChave.ToList());
            }

            return result;
        }

        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                JSRuntime.InvokeVoidAsync("initialize", null);

                StateHasChanged();
            }
            fichasTecnicas = FichaTecnicaDataService.GetFichaTecnica().Where(x => x.Titulo!= null).Select(p => p.Titulo).Distinct().ToArray();
        }
    }
}
