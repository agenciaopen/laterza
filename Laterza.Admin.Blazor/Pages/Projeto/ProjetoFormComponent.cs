﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Projeto;
using Laterza.Admin.Data.Data.Projeto;
using Laterza.Admin.Data.Enum;
using Laterza.Admin.Data.Models.Projeto;
using Laterza.Admin.Data.Models.Cidades;
using Laterza.Admin.Data.Models.Estados;
using Laterza.Admin.Data.Data.Interfaces.Cidades;
using Laterza.Admin.Data.Data.Interfaces.Estados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Laterza.Admin.Blazor.Pages.Projeto
{
    public partial class ProjetoFormComponent
    {
        private ProjetoModelForm model = new ProjetoModelForm();

        [Parameter]
        public List<ProjetoModel> projetoModelDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        public List<EstadosModel> estados { get; set; }

        public List<CidadesModel> cidades { get; set; } = new List<CidadesModel>();

        public bool hideTabsIdioma = false;

        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IProjetoDataService projetoDataService { get; set; }

        [Inject]
        ICidadesDataService cidadesDataService { get; set; }

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            alreadySave = false;

            if (projetoModelDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new ProjetoModelForm { };
                model.IdiomaId = idioma;
                estados = estadosDataService.GetEstados();
                cidades = new List<CidadesModel>();
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = projetoModelDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                if (string.IsNullOrEmpty(modelSelected.Estado) && string.IsNullOrEmpty(modelSelected.Cidade))
                {
                    estados = estadosDataService.GetEstados();
                    cidades = new List<CidadesModel>();
                }
                else if (!string.IsNullOrEmpty(modelSelected.Estado) && string.IsNullOrEmpty(modelSelected.Cidade))
                {
                    estados = estadosDataService.GetEstados();
                    cidades = cidadesDataService.GetCidades("", model.Estado);
                }
                else if (!string.IsNullOrEmpty(modelSelected.Estado) && !string.IsNullOrEmpty(modelSelected.Cidade))
                {
                    estados = estadosDataService.GetEstados();
                    cidades = cidadesDataService.GetCidades("", modelSelected.Estado);
                }
                else
                {
                    estados = estadosDataService.GetEstados();
                    cidades = new List<CidadesModel>();
                }
                model = _mapper.Map<ProjetoModelForm>(modelSelected);
            }
        }

        private void HandleValidSubmit()
        {
            if (!alreadySave)
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<ProjetoModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();

                    var pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_projeto", "/imagem/", id);
                    blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);
                    modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;

                    modelSave.Id = id;
                    projetoDataService.ProjetoCreate(modelSave);
                }
                else
                {
                    var pathUrlImagem = "";

                    if (modelSave.UrlImagem != null && modelSave.UrlImagem.Contains("data:image/jpeg"))
                    {
                        pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_projeto", "/imagem", model.Id);
                        var urlImagemAntiga = projetoModelDetalhes.FirstOrDefault().UrlImagem;
                        if (!string.IsNullOrEmpty(urlImagemAntiga))
                        {
                            blobService.DeleteBlobAsync(urlImagemAntiga);
                        }
                        blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);
                        modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                    }

                    projetoDataService.ProjetoUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Projeto");
                ProcessarEventCallback.InvokeAsync();
            }
            alreadySave = false;
        }
        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            cidades = cidadesDataService.GetCidades("", e.Value.ToString());
            StateHasChanged();
        }

        private void HandleProcessarUrlImagemCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlImagem = imagem;
            }
            fileUploaded = true;
        }
        private void HandleStateChange()
        {
            StateHasChanged();
        }
    }
}
