﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models;
using Laterza.Admin.Blazor.Models.Projeto;
using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Data.Projeto;
using Laterza.Admin.Data.Enum;
using Laterza.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Projeto
{
    public partial class ProjetoGaleriaFormComponent : ComponentBase
    {
        private ProjetoGaleriaModelForm model = new ProjetoGaleriaModelForm();


        [Parameter]
        public List<ProjetoGaleriaModel> projetoGaleriaDetalhes { get; set; }

        [Parameter]
        public int tipoGaleria { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]

        public Guid idProjeto { get; set; }


        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        IIdiomaDataService idiomaDataService { get; set; }


        [Inject]
        IProjetoGaleriaDataService projetoGaleriaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Inject] 
        NavigationManager uriHelper { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            alreadySave = false;

            if (projetoGaleriaDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new ProjetoGaleriaModelForm { };
                model.IdiomaId = idioma;
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = projetoGaleriaDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<ProjetoGaleriaModelForm>(modelSelected);
            }
        }
        private void HandleValidSubmit()
        {
            try
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<ProjetoGaleriaModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    if (!modelSave.EhVideo)
                    {
                        var pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo + "_galeria_escritorio", "/galeria/", id);
                        blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);

                        modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                    }
                    modelSave.Id = id;
                    modelSave.IdProjeto = idProjeto;
                    projetoGaleriaDataService.ProjetoGaleriaCreate(modelSave);
                }
                else
                {
                    var pathUrlImagem = "";

                    if (!modelSave.EhVideo)
                    {
                        if (modelSave.UrlImagem != null && modelSave.UrlImagem.Contains("data:image/jpeg"))
                        {
                            pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo, "/galeria/", modelSave.Id);

                            var urlBannerAntiga = projetoGaleriaDetalhes.FirstOrDefault().UrlImagem;
                            if (!string.IsNullOrEmpty(urlBannerAntiga))
                            {
                                blobService.DeleteBlobAsync(urlBannerAntiga);
                            }
                            blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);
                            modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                        }
                    }
                    projetoGaleriaDataService.ProjetoGaleriaUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-ProjetoGaleria");
                ProcessarEventCallback.InvokeAsync();
            }
            catch (Exception e)
            {
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = e.Message, tipo = "error" });
            }
            
            
        }
        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlImagem = imagem;
            }
            fileUploaded = true;
        }
    }
}
