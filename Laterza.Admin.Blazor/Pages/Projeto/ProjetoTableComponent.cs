﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Paginas;
using Laterza.Admin.Blazor.Models.Projeto;
using Laterza.Admin.Data.Data.Interfaces.Pagina;
using Laterza.Admin.Data.Data.Projeto;
using Laterza.Admin.Data.Enum;
using Laterza.Admin.Data.Models.Pagina;
using Laterza.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Projeto
{
    public partial class ProjetoTableComponent
    {
        public const string pagina = "projetos-Sociais";
        public List<PaginaModel> modelDetalhes { get; set; }

        private PaginaProjetosSociaisModelForm model = new PaginaProjetosSociaisModelForm();

        public List<ProjetoModel> modelList { get; set; }
        public List<ProjetoModel> projetoModelDetalhes { get; set; }

        [Parameter]
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }


        [Inject]
        IPaginaDataService paginaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IProjetoDataService projetoDataService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Inject]
        IBlobService blobService { get; set; }


        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public bool novoRegistro = true;
        public bool alreadySave { get; set; } = false;
        public string idiomaModal { get; set; }
        protected override void OnParametersSet()
        {
            GetTableList();
            HandleProcessarIdiomaModel("pt");
            StateHasChanged();
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            if (!alreadySave)
            {
                alreadySave = false;

                if (!string.IsNullOrEmpty(idioma))
                {
                    idiomaModal = idioma;
                }

                hideTabsIdioma = false;
                modelDetalhes = paginaDataService.GetPagina(pagina);
                var modelSelected = modelDetalhes.Where(p => p.IdiomaId == idiomaModal).FirstOrDefault();
                model = _mapper.Map<PaginaProjetosSociaisModelForm>(modelSelected);
                novoRegistro = false;
            }
        }

        private async Task HandleValidSubmit()
        {
            try
            {
                var modelSave = _mapper.Map<PaginaModel>(model);
                var pathBanner = "";
                var pathLogo = "";

                if (modelSave.Banner != null && modelSave.Banner.Contains("data:image/jpeg"))
                {
                    pathBanner = extensionMethods.CreatePathBlobFilePagina("/banner", model.Slug);
                    var urlBannerAntiga = modelDetalhes.FirstOrDefault().Banner;
                    if (!string.IsNullOrEmpty(urlBannerAntiga))
                    {
                        blobService.DeleteBlobAsync(urlBannerAntiga);
                    }
                    blobService.UploadBlobFileAsync(modelSave.Banner, pathBanner);
                    modelSave.Banner = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathBanner;
                }

                paginaDataService.PaginaUpdate(modelSave);
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            }
            catch (Exception)
            {
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Ops, ocorreu algum erro ao salvar. Tente novamente.", tipo = "error" });
            }
        }

        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.Banner = imagem;
            }
            fileUploaded = true;
        }
        private void GetTableList()
        {
            modelList = projetoDataService.GetProjeto().Where(x => x.IdiomaId == "pt").ToList();
        }

        void Create()
        {
            projetoModelDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Projeto");
            JSRuntime.InvokeAsync<object>("selectpicker", null);
            StateHasChanged();
        }
        private void DeleteConfirm(IProjetoModel projetoModel)
        {
            idToDelete = projetoModel.Id;
            //JSRuntime.InvokeAsync<object>("showPopover", "popoverDeleteConfirm");
        }

        private void Delete(IProjetoModel projetoModel)
        {
            projetoDataService.ProjetoDelete(projetoModel.Id);
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Remoção feita com sucesso.", tipo = "ok" });
            HandleProcessarCallBack();
        }
        void Edit(IProjetoModel projetoModel)
        {
            projetoModelDetalhes = null;
            projetoModelDetalhes = projetoDataService.GetProjeto(projetoModel.Id);
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Projeto");
            JSRuntime.InvokeAsync<object>("selectpicker", null);
        }
        private void HandleProcessarCallBack()
        {
            GetTableList();
            ProcessarEventCallback.InvokeAsync();
        }
    }
}
