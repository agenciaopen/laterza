﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Faq;
using Laterza.Admin.Data.Data.Interfaces.Faq;
using Laterza.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Faq
{
    public partial class FaqFormComponent : ComponentBase
    {
        private FaqModelForm model = new FaqModelForm();


        [Parameter]
        public List<FaqModel> faqModelDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }


        [Inject]
        IFaqDataService faqDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            alreadySave = false;
            idiomaModal = idioma;
            if (faqModelDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new FaqModelForm { };
                model.IdiomaId = idioma;
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = faqModelDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<FaqModelForm>(modelSelected);
            }
        }

        private void HandleValidSubmit()
        {
            if (!alreadySave)
            {
                alreadySave = true;
                JSRuntime.InvokeAsync<object>("disableButtons", "");
                var modelSave = _mapper.Map<FaqModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    modelSave.Id = id;
                    faqDataService.FaqCreate(modelSave);
                    Thread.Sleep(1000);
                    JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                    JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Faq");
                    JSRuntime.InvokeAsync<object>("enableButtons", "");
                    ProcessarEventCallback.InvokeAsync();
                }
                else
                {
                    faqDataService.FaqUpdate(modelSave);
                    Thread.Sleep(1000);
                    JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                    JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Faq");
                    JSRuntime.InvokeAsync<object>("enableButtons", "");
                    ProcessarEventCallback.InvokeAsync();
                }
            }
            alreadySave = false;
        }

    }
}
