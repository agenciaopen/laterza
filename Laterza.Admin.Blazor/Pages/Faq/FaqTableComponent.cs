﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Data.Data.Interfaces.Faq;
using Laterza.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.Faq
{
    public partial class FaqTableComponent : ComponentBase
    {
        public List<FaqModel> modelListImovelSobrePagamentosFinanciamentos { get; set; }
        public List<FaqModel> modelListImovelSobreNossoModeloDeNegocio { get; set; }
        public List<FaqModel> modelListImovelSobreImpostosTaxas { get; set; }
        public List<FaqModel> modelListClienteAcompanhamentoDaObra { get; set; }
        public List<FaqModel> modelListClientePersonalização { get; set; }
        public List<FaqModel> modelListClienteTributosImpostosHabiteSe { get; set; }
        public List<FaqModel> faqModelDetalhes { get; set; }

        public Guid idToDelete { get; set; }

        [Inject]
        IFaqDataService faqDataService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        private void GetTableList()
        {
            modelListImovelSobrePagamentosFinanciamentos = faqDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == 1).OrderBy(z => z.Secao).ToList();
            modelListImovelSobreNossoModeloDeNegocio = faqDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == 2).OrderBy(z => z.Secao).ToList();
            modelListImovelSobreImpostosTaxas = faqDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == 3).OrderBy(z => z.Secao).ToList();
            modelListClienteAcompanhamentoDaObra = faqDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == 4).OrderBy(z => z.Secao).ToList();
            modelListClientePersonalização = faqDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == 5).OrderBy(z => z.Secao).ToList();
            modelListClienteTributosImpostosHabiteSe = faqDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == 6).OrderBy(z => z.Secao).ToList();
        }
        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
       
        private void DeleteGaleriaConfirm(IFaqModel faq)
        {
            idToDelete = faq.Id;
        }

        void Create()
        {
            faqModelDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Faq");
            StateHasChanged();
        }

        void Edit(IFaqModel faqModel)
        {
            faqModelDetalhes = faqDataService.GetFaq(faqModel.Id);

            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Faq");
        }
        private void DeleteGaleria(IFaqModel faq)
        {

            faqDataService.FaqDelete(idToDelete);
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            GetTableList();
            StateHasChanged();
        }
        private void HandleProcessarCallBack()
        {
            GetTableList();
        }

    }
}
