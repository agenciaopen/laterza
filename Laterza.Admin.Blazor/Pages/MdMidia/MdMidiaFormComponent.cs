﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.MdMidia;
using Laterza.Admin.Data.Data.MdMidia;
using Laterza.Admin.Data.Enum;
using Laterza.Admin.Data.Models.MdMidia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Laterza.Admin.Blazor.Pages.MdMidia
{
    public partial class MdMidiaFormComponent
    {
        private MdMidiaModelForm model = new MdMidiaModelForm();

        [Parameter]
        public List<MdMidiaModel> mdMidiaModelDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        public bool hideTabsIdioma = false;

        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IMdMidiaDataService mdMidiaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            alreadySave = false;


            if (mdMidiaModelDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new MdMidiaModelForm { };
                model.IdiomaId = idioma;
                model.DataPublicacao = DateTime.Now;
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = mdMidiaModelDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<MdMidiaModelForm>(modelSelected);
            }
        }

        private void HandleValidSubmit()
        {
            if (!alreadySave)
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<MdMidiaModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();

                    var pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo + "_mdMidia", "/imagem/", id);
                    blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);
                    modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                    modelSave.Id = id;
                    mdMidiaDataService.MdMidiaCreate(modelSave);
                }
                else
                {
                    var pathUrlImagem = "";

                    if (modelSave.UrlImagem != null && modelSave.UrlImagem.Contains("data:image/jpeg"))
                    {
                        pathUrlImagem = extensionMethods.CreatePathBlobFile(modelSave.Titulo + "_mdMidia", "/imagem", model.Id);
                        var urlImagemAntiga = mdMidiaModelDetalhes.FirstOrDefault().UrlImagem;
                        if (!string.IsNullOrEmpty(urlImagemAntiga))
                        {
                            blobService.DeleteBlobAsync(urlImagemAntiga);
                        }
                        blobService.UploadBlobFileAsync(modelSave.UrlImagem, pathUrlImagem);
                        modelSave.UrlImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlImagem;
                    }

                    mdMidiaDataService.MdMidiaUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-MdMidia");
                ProcessarEventCallback.InvokeAsync();
            }
            alreadySave = false;
        }
        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            StateHasChanged();
        }

        private void HandleProcessarUrlImagemCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlImagem = imagem;
            }
            fileUploaded = true;
        }
    }
}
