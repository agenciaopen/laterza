﻿using AutoMapper;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models;
using Laterza.Admin.Data.Data.Interfaces.Pagina;
using Laterza.Admin.Data.Models.Pagina;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Pages.SEO
{
    public partial class SEOComponent : ComponentBase
    {
        [Parameter]
        public string pagina { get; set; }
        public List<PaginaModel> modelDetalhes { get; set; }

        private SEOModelForm model = new SEOModelForm();

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IPaginaDataService paginaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public bool novoRegistro = true;
        public bool alreadySave { get; set; } = false;
        public string idiomaModal { get; set; }
        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            StateHasChanged();
            base.OnParametersSet();
        }
        private void HandleProcessarIdiomaModel(string idioma)
        {
            if (!alreadySave)
            {
                alreadySave = false;

                if (!string.IsNullOrEmpty(idioma))
                {
                    idiomaModal = idioma;
                }

                hideTabsIdioma = false;
                modelDetalhes = paginaDataService.GetPagina(pagina);
                var modelSelected = modelDetalhes.Where(p => p.IdiomaId == idiomaModal).FirstOrDefault();
                model = _mapper.Map<SEOModelForm>(modelSelected);
                novoRegistro = false;
            }
        }

        private async Task HandleValidSubmit()
        {
            try
            {
                var modelSave = _mapper.Map<PaginaModel>(model);

                paginaDataService.PaginaUpdate(modelSave);
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            }
            catch (Exception)
            {
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Ops, ocorreu algum erro ao salvar. Tente novamente.", tipo = "error" });
            }
        }
    }
}
