﻿using Laterza.Admin.Blazor.Models.PoliticaDePrivacidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazored.TextEditor;
using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using Microsoft.AspNetCore.Components;
using Laterza.Admin.Data.Data.Interfaces.PoliticaDePrivacidade;
using AutoMapper;
using Microsoft.JSInterop;
using System.Threading;

namespace Laterza.Admin.Blazor.Pages.PoliticaDePrivacidade
{
    public partial class PoliticaDePrivacidadeFormComponent
    {
        public List<PoliticaDePrivacidadeModel> modelDetalhes { get; set; }
        PoliticaDePrivacidadeModelForm model = new PoliticaDePrivacidadeModelForm();
        BlazoredTextEditor QuillHtml;
        string QuillHTMLContent;

        [Inject]
        IPoliticaDePrivacidadeDataService PoliticaDePrivacidadeDataService { get; set; }
        [Inject]
        IMapper _mapper { get; set; }
        [Inject]
        IJSRuntime JSRuntime { get; set; }


        private async Task HandleValidSubmit()
        {
            model.Conteudo = await this.QuillHtml.GetHTML();
            var modelSave = _mapper.Map<PoliticaDePrivacidadeModel>(model);

            if (modelSave.Id == Guid.Empty)
            {
                var id = Guid.NewGuid();
                modelSave.Id = id;
                modelSave.DataAlteracao = DateTime.Now;
                modelSave.DataCriacao = DateTime.Now;

                PoliticaDePrivacidadeDataService.PoliticaDePrivacidadeCreate(modelSave);

                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Política de privacidade salva com sucesso.", tipo = "ok" });
            }
            else
            {
                JSRuntime.InvokeAsync<object>("enableButtons", "");

                modelSave.DataAlteracao = DateTime.Now;
                PoliticaDePrivacidadeDataService.PoliticaDePrivacidadeUpdate(modelSave);
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                StateHasChanged();
            }
        }

        protected override void OnParametersSet()
        {
            setFields();
            StateHasChanged();
            base.OnParametersSet();
        }

        private void setFields()
        {
            modelDetalhes = PoliticaDePrivacidadeDataService.GetPoliticaDePrivacidade();

            var modelSelected = modelDetalhes.FirstOrDefault();

            if(modelSelected != null)
                model = _mapper.Map<PoliticaDePrivacidadeModelForm>(modelSelected);
        }

        private void HandleStateChange()
        {
            StateHasChanged();
        }
    }
}
