﻿using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Paginas;
using Laterza.Admin.Data.Data.Interfaces.Pagina;
using Laterza.Admin.Data.Models.Pagina;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Laterza.Admin.Blazor.Pages.Paginas
{
    public partial class TrabalheConoscoComponent
    {
        public const string pagina = "trabalhe-conosco";
        public List<PaginaModel> modelDetalhes { get; set; }

        private PaginaSalaDeImprensaModelForm model = new PaginaSalaDeImprensaModelForm();

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IPaginaDataService paginaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public bool novoRegistro = true;
        public bool alreadySave { get; set; } = false;
        public string idiomaModal { get; set; }
        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            StateHasChanged();
            base.OnParametersSet();
        }
        private void HandleProcessarIdiomaModel(string idioma)
        {
            if (!alreadySave)
            {
                alreadySave = false;

                if (!string.IsNullOrEmpty(idioma))
                {
                    idiomaModal = idioma;
                }

                hideTabsIdioma = false;
                modelDetalhes = paginaDataService.GetPagina(pagina);
                var modelSelected = modelDetalhes.Where(p => p.IdiomaId == idiomaModal).FirstOrDefault();
                model = _mapper.Map<PaginaSalaDeImprensaModelForm>(modelSelected);
                novoRegistro = false;
            }
        }

        private async Task HandleValidSubmit()
        {
            try
            {
                var modelSave = _mapper.Map<PaginaModel>(model);
                var pathBanner = "";
                var pathLogo = "";

                if (modelSave.Banner != null && modelSave.Banner.Contains("data:image/jpeg"))
                {
                    pathBanner = extensionMethods.CreatePathBlobFilePagina("/banner", model.Slug);
                    var urlBannerAntiga = modelDetalhes.FirstOrDefault().Banner;
                    if (!string.IsNullOrEmpty(urlBannerAntiga))
                    {
                        blobService.DeleteBlobAsync(urlBannerAntiga);
                    }
                    blobService.UploadBlobFileAsync(modelSave.Banner, pathBanner);
                    modelSave.Banner = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathBanner;
                }

                paginaDataService.PaginaUpdate(modelSave);
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            }
            catch (Exception)
            {
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Ops, ocorreu algum erro ao salvar. Tente novamente.", tipo = "error" });
            }
        }
        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.Banner = imagem;
            }
            fileUploaded = true;
        }
    }
}
