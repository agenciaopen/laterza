﻿using Laterza.Admin.Data.Models.Idioma;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models
{
    public class IdiomaModel : IIdiomaModel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
    }
}
