﻿using Laterza.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.Faq
{
    public class FaqModelForm : IFaqModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }

        [Required]
        [RegularExpression("1|2|3|4|5|6", ErrorMessage = "Tipo de Faq é obrigatório.")]
        public int TipoFaq { get; set; }
        public int TipoFaqArea { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public string UrlImagem { get; set; }
        public int Secao { get; set; }
    }
}
