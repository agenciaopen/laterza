﻿using Laterza.Admin.Data.Models.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Laterza.Admin.Blazor.Models.Blog
{
    public class PostGaleriaModelForm : IPostGaleriaModel
    {
        public Guid Id { get; set; }
        public Guid IdPost { get; set; }
        public string UrlImagem { get; set; }
        public string UrlVideo { get; set; }
        public string Descricao { get; set; }
        public string Titulo { get; set; }
        public bool EhVideo { get; set; }
        public int Ordem { get; set; }

        public string GetIdUrlVideo()
        {
            if (UrlVideo != null)
            {
                var uri = new Uri(UrlVideo);
                var query = HttpUtility.ParseQueryString(uri.Query);
                return query["v"];
            }
            return UrlVideo;
        }
    }
}
