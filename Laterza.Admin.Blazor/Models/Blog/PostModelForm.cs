﻿using Laterza.Admin.Data.Models.Blog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.Blog
{
    public class PostModelForm : IPostModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string UrlImagemDestaque { get; set; }
        public bool Destacar { get; set; }
        public string Categoria { get; set; }
        public string Conteudo { get; set; }
        public string Slug { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
