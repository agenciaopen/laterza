﻿using Laterza.Admin.Data.Models.Escritorio;
using System;
using System.ComponentModel.DataAnnotations;

namespace Laterza.Admin.Blazor.Models.Escritorio
{
    public class EscritorioModelForm : IEscritorioModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string HorarioFuncionamento { get; set; }
        public string UrlBanner { get; set; }
        public string EstadoNome { get; set; }
        public string CidadeNome { get; set; }
    }
}
