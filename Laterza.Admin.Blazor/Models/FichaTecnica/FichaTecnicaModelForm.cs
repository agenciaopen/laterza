﻿using Laterza.Admin.Data.Models.FichaTecnica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.FichaTecnica
{
    public class FichaTecnicaModelForm : IFichaTecnicaModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string Titulo { get; set; }
        public bool Destacar { get; set; }
    }
}
