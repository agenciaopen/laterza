﻿using Laterza.Admin.Data.Models.Projeto;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Laterza.Admin.Blazor.Models.Projeto
{
    public class ProjetoGaleriaModelForm : IProjetoGaleriaModel
    {
        public Guid Id { get; set; }
        public Guid IdProjeto { get; set; }
        public string IdiomaId { get; set; }
        public string UrlImagem { get; set; }
        public string UrlVideo { get; set; }
        public string Titulo { get; set; }
        public bool EhVideo { get; set; }
        public int Ordem { get; set; }

        public string GetIdUrlVideo()
        {
            if (UrlVideo != null)
            {
                var uri = new Uri(UrlVideo);
                var query = HttpUtility.ParseQueryString(uri.Query);
                return query["v"];
            }
            return UrlVideo;
        }

    }
}
