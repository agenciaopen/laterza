﻿using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.PoliticaDePrivacidade
{
    public class PoliticaDePrivacidadeModelForm : IPoliticaDePrivacidadeModel
    {
        public Guid Id { get; set; }
        public string Conteudo { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
