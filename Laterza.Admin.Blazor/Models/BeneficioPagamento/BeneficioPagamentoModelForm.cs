﻿using Laterza.Admin.Data.Models.BeneficioPagamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.BeneficioPagamento
{
    public class BeneficioPagamentoModelForm : IBeneficioPagamentoModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string BeneficioPagamento { get; set; }
    }
}
