﻿using Laterza.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.Home
{
    public class HomeBannerModelForm : IHomeBannerModel
    {
        public Guid Id { get; set; }
        public string Link { get; set; }
        public string UrlImagem { get; set; }
        public string Titulo { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
