﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.Empreendimento
{
    public class EmpreendimentoValorDescricaoModelForm
    {
        public Guid Id { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string IdiomaId { get; set; }
        [Required]

        public string Descricao { get; set; }
        [Required]

        public string Valor { get; set; }
        [Required]

        public string Titulo { get; set; }
        public string Slug { get; set; }

    }
}
