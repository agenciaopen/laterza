﻿using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Models.Empreendimento
{
    public class EmpreendimentoInformacoesTecnicasModelForm : IEmpreendimentoInformacoesTecnicasModel
    {
        public Guid Id { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string IdiomaId { get; set; }
        public string Nome { get; set; }
        public string Funcao { get; set; }
    }
}
