using AutoMapper;
using Azure.Storage.Blobs;
using BlazorTable;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Laterza.Admin.Blazor.Areas.Identity;
using Laterza.Admin.Blazor.Data;
using Laterza.Admin.Blazor.Models.Empreendimento;
using Laterza.Admin.Blazor.Models.Escritorio;
using Laterza.Admin.Blazor.Models.Projeto;
using Laterza.Admin.Blazor.Models.MdMidia;
using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Data.Empreendimento;
using Laterza.Admin.Data.Data.Cidades;
using Laterza.Admin.Data.Data.Estados;
using Laterza.Admin.Data.Data.Escritorio;
using Laterza.Admin.Data.Data.Projeto;
using Laterza.Admin.Data.Data.MdMidia;
using Laterza.Admin.Data.Data.Interfaces.Cidades;
using Laterza.Admin.Data.Data.Interfaces.Estados;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.Escritorio;
using Laterza.Admin.Data.Models.Projeto;
using Laterza.Admin.Data.Models.MdMidia;
using Laterza.DragDrop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Laterza.Admin.Data.Data.Interfaces.Pagina;
using Laterza.Admin.Data.Data.Pagina;
using Laterza.Admin.Data.Models.Pagina;
using Laterza.Admin.Blazor.Models.Paginas;
using Laterza.Admin.Data.Data.Interfaces.Faq;
using Laterza.Admin.Data.Data.Faq;
using Laterza.Admin.Data.Models.Faq;
using Laterza.Admin.Blazor.Models.Faq;
using Laterza.Admin.Data.Data.Interfaces.Blog;
using Laterza.Admin.Data.Data.Blog;
using Laterza.Admin.Blazor.Models.Blog;
using Laterza.Admin.Data.Models.Blog;
using Laterza.Admin.Data.Data.Interfaces.PoliticaDePrivacidade;
using Laterza.Admin.Data.Data.PoliticaDePrivacidade;
using Laterza.Admin.Blazor.Models.PoliticaDePrivacidade;
using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using Laterza.Admin.Data.Models.Home;
using Laterza.Admin.Blazor.Models.Home;
using Laterza.Admin.Data.Data.Interfaces.BeneficioPagamento;
using Laterza.Admin.Data.Data.BeneficioPagamento;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using Laterza.Admin.Blazor.Models.BeneficioPagamento;
using Laterza.Admin.Data.Data.FichaTecnica;
using Laterza.Admin.Data.Data.Interfaces.FichaTecnica;
using Laterza.Admin.Blazor.Models.FichaTecnica;
using Laterza.Admin.Data.Models.FichaTecnica;
using Laterza.Admin.Blazor.Models;

namespace Laterza.Admin.Blazor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<IdentityUser>>();
            services.AddDatabaseDeveloperPageExceptionFilter();
            services.AddSingleton<IExtensionMethods, ExtensionMethods>();
            services.AddSingleton<ISqlDataAccess, SqlDataAccess>();
            services.AddScoped<IHomeDataService, HomeDataService>();
            services.AddScoped<IEmpreendimentoDataService, EmpreendimentoDataService>();
            services.AddScoped<IEmpreendimentoGaleriaDataService, EmpreendimentoGaleriaDataService>();
            services.AddScoped<IEmpreendimentoValorDescricaoDataService, EmpreendimentoValorDescricaoDataService>();
            services.AddScoped<IEmpreendimentoInformacoesTecnicasDataService, EmpreendimentoInformacoesTecnicasDataService>();
            services.AddScoped<IEstadosDataService, EstadosDataService>();
            services.AddScoped<ICidadesDataService, CidadesDataService>();
            services.AddScoped<IEscritorioDataService, EscritorioDataService>();
            services.AddScoped<IProjetoDataService, ProjetoDataService>();
            services.AddScoped<IProjetoGaleriaDataService, ProjetoGaleriaDataService>();
            services.AddScoped<IMdMidiaDataService, MdMidiaDataService>();
            services.AddScoped<IPaginaDataService, PaginaDataService>();
            services.AddScoped<IFaqDataService, FaqDataService>();
            services.AddSingleton<IIdiomaDataService, IdiomaDataService>();

            services.AddScoped<IPostDataService, PostDataService>();
            services.AddScoped<IPostGaleriaDataService, PostGaleriaDataService>();
            services.AddScoped<IPoliticaDePrivacidadeDataService, PoliticaDePrivacidadeDataService>();
            services.AddScoped<IBeneficioPagamentoDataService, BeneficioPagamentoDataService>();
            services.AddScoped<IFichaTecnicaDataService, FichaTecnicaDataService>();
            
            services.AddBlazorTable();
            services.AddBlazorDragDrop();
            services.AddSingleton(x => new BlobServiceClient(Configuration.GetValue<string>("AzureStorageBlobConnectionStrings")));
            services.AddScoped<IBlobService, BlobService>();
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EmpreendimentoModelForm, EmpreendimentoModelUpdateCreate>();
                cfg.CreateMap<EmpreendimentoModelUpdateCreate, EmpreendimentoModelForm>();
                cfg.CreateMap<EmpreendimentoModel, EmpreendimentoModelForm>();
                cfg.CreateMap<EmpreendimentoModelForm, EmpreendimentoModel>();
                cfg.CreateMap<EmpreendimentoGaleriaModelForm, EmpreendimentoGaleriaModel>();
                cfg.CreateMap<EmpreendimentoGaleriaModel, EmpreendimentoGaleriaModelForm>();
                cfg.CreateMap<EmpreendimentoValorDescricaoModelForm, EmpreendimentoValorDescricaoModel>();
                cfg.CreateMap<EmpreendimentoValorDescricaoModel, EmpreendimentoValorDescricaoModelForm>();
                cfg.CreateMap<EmpreendimentoInformacoesTecnicasModel, EmpreendimentoInformacoesTecnicasModelForm>();
                cfg.CreateMap<EmpreendimentoInformacoesTecnicasModelForm, EmpreendimentoInformacoesTecnicasModel>();
                cfg.CreateMap<EscritorioModel, EscritorioModelForm>();
                cfg.CreateMap<EscritorioModelForm, EscritorioModel>();
                cfg.CreateMap<ProjetoModel, ProjetoModelForm>();
                cfg.CreateMap<ProjetoModelForm, ProjetoModel>();
                cfg.CreateMap<ProjetoGaleriaModel, ProjetoGaleriaModelForm>();
                cfg.CreateMap<ProjetoGaleriaModelForm, ProjetoGaleriaModel>();
                cfg.CreateMap<MdMidiaModel, MdMidiaModelForm>();
                cfg.CreateMap<MdMidiaModelForm, MdMidiaModel>();
                cfg.CreateMap<PaginaVendaSeuTerrenoModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaVendaSeuTerrenoModelForm>();
                cfg.CreateMap<PaginaSimuladorModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaSimuladorModelForm>();
                cfg.CreateMap<PaginaModelosModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaModelosModelForm>(); 
                cfg.CreateMap<PaginaProjetosSociaisModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaProjetosSociaisModelForm>();
                cfg.CreateMap<PaginaEscritorioModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaEscritorioModelForm>();
                cfg.CreateMap<PaginaHomeModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaHomeModelForm>();
                cfg.CreateMap<PaginaQuemSomosModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaQuemSomosModelForm>();
                cfg.CreateMap<PaginaProgramaDeIntegridadeModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaProgramaDeIntegridadeModelForm>();
                cfg.CreateMap<PaginaSalaDeImprensaModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaSalaDeImprensaModelForm>();
                cfg.CreateMap<PaginaTrabalheConoscoModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaTrabalheConoscoModelForm>();
                cfg.CreateMap<PaginaComoInvestirComponentModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaComoInvestirComponentModelForm>();
                cfg.CreateMap<FaqModel, FaqModelForm>();
                cfg.CreateMap<FaqModelForm, FaqModel>();
                cfg.CreateMap<PostModelForm, PostModel>();
                cfg.CreateMap<PostModel, PostModelForm>();
                cfg.CreateMap<PostGaleriaModel, PostGaleriaModelForm>();
                cfg.CreateMap<PostGaleriaModelForm, PostGaleriaModel>();
                cfg.CreateMap<PoliticaDePrivacidadeModelForm, PoliticaDePrivacidadeModel>();
                cfg.CreateMap<PoliticaDePrivacidadeModel, PoliticaDePrivacidadeModelForm>();
                cfg.CreateMap<HomeBannerModel, HomeBannerModelForm>();
                cfg.CreateMap<BeneficioPagamentoModelForm, BeneficioPagamentoModel>();
                cfg.CreateMap<BeneficioPagamentoModel, BeneficioPagamentoModelForm>();
                cfg.CreateMap<FichaTecnicaModelForm, FichaTecnicaModel>();
                cfg.CreateMap<FichaTecnicaModel, FichaTecnicaModelForm>();
                cfg.CreateMap<PaginaModel, SEOModelForm>();
                cfg.CreateMap<SEOModelForm, PaginaModel>();



            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseMigrationsEndPoint();

            /*if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }*/

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
