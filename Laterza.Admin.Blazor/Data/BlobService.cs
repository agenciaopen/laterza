﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Laterza.Admin.Data.Data;
using Microsoft.AspNetCore.Components;
using Laterza.Admin.Blazor.Models;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Specialized;
using System.Linq;
using System.Drawing;

namespace Laterza.Admin.Blazor.Data
{
    public class BlobService : IBlobService
    {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly IExtensionMethods _extensionMethods;

        public BlobService(BlobServiceClient blobServiceClient, IExtensionMethods extensionMethods)
        {
            _blobServiceClient = blobServiceClient;
            _extensionMethods = extensionMethods;
        }

        public void DeleteBlobAsync(string path)
        {
            try
            {
                BlobContainerClient container = _blobServiceClient.GetBlobContainerClient("img");

                var uri = new Uri(path);

                BlockBlobClient blockBlob = container.GetBlockBlobClient(uri.LocalPath.Remove(0, 5));
                blockBlob.Delete();

                BlockBlobClient blockBlobThumb = container.GetBlockBlobClient(uri.LocalPath.Remove(0, 5).Replace(".jpeg", "_thumb.jpeg"));
                blockBlobThumb.Delete();
            }
            catch (Exception)
            {
                return;
            }
           
        }
        public Task<IEnumerable<string>> ListBlobAsync(string name)
        {
            throw new NotImplementedException();
        }

        public void UploadBlobFileAsync(string base64, string path)
        {
            string fileName = Guid.NewGuid().ToString();
            byte[] filebytes = Convert.FromBase64String(base64.Replace("data:image/jpeg;base64,", ""));
            
            for (int i = 0; i < 2; i++)
            {
                if (i == 1)
                {
                    int width;
                    int height;
                    filebytes = Convert.FromBase64String(base64.Replace("data:image/jpeg;base64,", ""));
                    using (var msResize = new MemoryStream(filebytes.ToArray()))
                    {
                        var image = Image.FromStream(msResize);

                        width = (int)(image.Width / 2.5);
                        height = (int)(image.Height / 2.5);
                    }

                    FileStream fs = new FileStream(fileName + ".jpeg",
                                          FileMode.OpenOrCreate,
                                          FileAccess.ReadWrite,
                                          FileShare.ReadWrite);
                    fs.Write(filebytes);
                    fs.Position = 0;
                    
                    Image img = Image.FromStream(fs);
                    var imageDataURL = _extensionMethods.GetBase64ImageString(img, width, height);

                    filebytes = Convert.FromBase64String(imageDataURL.Replace("data:image/jpeg;base64,", ""));
                    fs.Dispose();
                    File.Delete(fileName + ".jpeg");

                    fs = new FileStream(fileName + ".jpeg",
                                          FileMode.OpenOrCreate,
                                          FileAccess.ReadWrite,
                                          FileShare.ReadWrite);
                    fs.Write(filebytes);
                    fs.Position = 0;

                    BlobContainerClient container = _blobServiceClient.GetBlobContainerClient("img");
                    BlockBlobClient blockBlob = container.GetBlockBlobClient(path.Remove(0, 1).Remove(path.Length - 6) + "_thumb.jpeg");

                    blockBlob.Upload(fs);
                    fs.Dispose();
                    File.Delete(fileName + ".jpeg");
                }
                else
                {
                    FileStream fs = new FileStream(fileName + ".jpeg",
                                          FileMode.OpenOrCreate,
                                          FileAccess.ReadWrite,
                                          FileShare.ReadWrite);
                    fs.Write(filebytes);
                    fs.Position = 0;

                    BlobContainerClient container = _blobServiceClient.GetBlobContainerClient("img");
                    BlockBlobClient blockBlob = container.GetBlockBlobClient(path.Remove(0, 1));

                    blockBlob.Upload(fs);
                    fs.Dispose();
                    File.Delete(fileName + ".jpeg");
                }
            }
        }

        public Task UploadContentBlobAsync(string content, string fileName)
        {
            throw new NotImplementedException();
        }
    }
}

