﻿using Laterza.Admin.Blazor.Models;
using System; 
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Data
{
    public interface IBlobService
    {
        Task<IEnumerable<string>> ListBlobAsync(string name);
        void UploadBlobFileAsync(string blobInfo, string path);
        Task UploadContentBlobAsync(string content, string fileName);
        void DeleteBlobAsync(string path);
    }
}
