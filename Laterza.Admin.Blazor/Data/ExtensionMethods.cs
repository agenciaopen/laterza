using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Blazor.Data
{
    public class ExtensionMethods : IExtensionMethods
    {
        public ExtensionMethods()
        {

        }
        public string GetBase64ImageString(Image image, int width, int height)
        {
            var newImage = new Bitmap(width, height);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, width, height);
            Bitmap bmp = new Bitmap(newImage);

            ImageConverter converter = new ImageConverter();

            var data = (byte[])converter.ConvertTo(bmp, typeof(byte[]));

            return "data:image/jpeg;base64," + Convert.ToBase64String(data);
        }
        public string RemoveCaracteresEspeciais(string str)
        {
            if (str == null || str.Trim() == String.Empty)
                return String.Empty;

            // Remove espa�os duplos da string
            while (str.IndexOf("  ") > 0)
                str = str.Replace("  ", " ");

            str = str.Replace(" - ", " ");
            str = str.Replace("�", "a").Replace("�", "a").Replace("�", "a").Replace("�", "a");
            str = str.Replace(" + ", "+");
            str = str.Replace("�", "e").Replace("�", "e").Replace("�", "i").Replace("�", "i");
            str = str.Replace("�", "o").Replace("�", "o").Replace("�", "o").Replace("�", "u").Replace("�", "u");
            str = str.Replace("�", "A").Replace("�", "A").Replace("�", "A").Replace("�", "A");
            str = str.Replace("�", "E").Replace("�", "E").Replace("�", "I").Replace("�", "I");
            str = str.Replace("�", "O").Replace("�", "O").Replace("�", "O").Replace("�", "U").Replace("�", "U");
            str = str.Replace("�", "c").Replace("�", "C");
            str = str.Replace("%", "").Replace("�", "").Replace("`", "").Replace("~", "").Replace("^", "").Replace("&", "");
            str = str.Replace("\"", "").Replace("'", "").Replace("@", "").Replace("#", "").Replace("$", "").Replace("!", "").Replace("*", "").Replace("(", "").Replace(")", "");
            str = str.Replace("\t", "").Replace("\r", "").Replace("\n", "").Replace("�", "1").Replace("�", "2").Replace("�", "3");
            str = str.Replace("\\", "").Replace("�", "o").Replace("�", "a").Replace("/", "").Replace("//", "").Replace(".", "-");

            return str.Trim();
        }
        public string CreatePathBlobFile(string str, string uri, Guid id)
        {
            var slug = RemoverAcentos(str);

            slug = RemoveCaracteresEspeciais(slug);

            slug = slug.Replace(" ", "-").ToLower();

            return uri + "/" + id.ToString() + "/" + slug + ".jpeg";
        }

        public string CreatePathBlobFilePagina(string uri, string pagina)
        {
            return uri + "/" + pagina + "/" + pagina + ".jpeg";
        }

        public string RemoverAcentos(string texto)
        {
            if (string.IsNullOrEmpty(texto))
            {
                return "";
            }
            string s = texto.Normalize(NormalizationForm.FormD);

            StringBuilder sb = new StringBuilder();

            for (int k = 0; k < s.Length; k++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[k]);
                if (uc != UnicodeCategory.NonSpacingMark)
                    {
                        sb.Append(s[k]);
                    }
                }
                return sb.ToString();
            }
        public string Description(Enum enumValue)
        {
            var descriptionAttribute = enumValue.GetType()
                .GetField(enumValue.ToString())
                .GetCustomAttributes(false)
                .SingleOrDefault(attr => attr.GetType() == typeof(DescriptionAttribute)) as DescriptionAttribute;

            // return description
            return descriptionAttribute?.Description ?? "";
        }

        public string CreateSlug(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }
            var slug = RemoverAcentos(str);

            slug = RemoveCaracteresEspeciais(slug);

            slug = slug.Replace(" ", "-").ToLower();
            return slug;
        }
    }
}
//Uri uri = new Uri(uriName);