﻿using Laterza.Admin.Data.Data.Interfaces.BeneficioPagamento;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laterza.Admin.Data.Data.BeneficioPagamento
{
    public class BeneficioPagamentoDataService : IBeneficioPagamentoDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public BeneficioPagamentoDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<BeneficioPagamentoModel> GetBeneficioPagamento()
        {
            try
            {
                var BeneficioPagamento = _dataAccess.LoadDataSync<BeneficioPagamentoModel, dynamic>("dbo.spBeneficioPagamento_Read", new { id = Guid.Empty, IdEmpreendimento = Guid.Empty }, "SQLDB");
                return BeneficioPagamento.ToList<BeneficioPagamentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public List<BeneficioPagamentoModel> GetBeneficioPagamento(Guid Id, Guid IdEmpreendimento)
        {
            try
            {
                var BeneficioPagamento = _dataAccess.LoadDataSync<BeneficioPagamentoModel, dynamic>("dbo.spBeneficioPagamento_Read", new { Id = Id, IdEmpreendimento = IdEmpreendimento }, "SQLDB");
                return BeneficioPagamento;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public string BeneficioPagamentoCreate(IBeneficioPagamentoModel BeneficioPagamento)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spBeneficioPagamento_Create", BeneficioPagamento, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public void BeneficioPagamentoUpdate(IBeneficioPagamentoModel BeneficioPagamento)
        {
            try
            {
                _dataAccess.SaveData("dbo.spBeneficioPagamento_Update", BeneficioPagamento, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void BeneficioPagamentoDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spBeneficioPagamento_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
