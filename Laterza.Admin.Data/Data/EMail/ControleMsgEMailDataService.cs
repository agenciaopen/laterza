﻿using Laterza.Admin.Data.Data.Interfaces.EMail;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.EMail;
using Open.CORE.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Laterza.Admin.Data.Data.EMail
{
    public class ControleMsgEMailDataService : IControleMsgEMailDataService
    {
        readonly ISqlDataAccess _dataAccess;
        readonly IControleMsgEMailLogDataService _log;
        readonly Open.CORE.Mensagem.EMail _msgEMail;

        public ControleMsgEMailDataService( ISqlDataAccess dataAccess,
                                            IControleMsgEMailLogDataService log,
                                            ConfigAplicacao config)
        {
            _dataAccess = dataAccess;
            _log = log;

            _msgEMail = new Open.CORE.Mensagem.EMail(config.ServidorSMTP, config.Porta, config.EndRemetente, config.Senha);
        }

        public List<ControleMsgEMailModel> GetMsg(DateTime dataMsg)
        {
            try
            {
                var _resultado = new List<ControleMsgEMailModel>();

                foreach (var item in _dataAccess.LoadDataSync<ControleMsgEMailModel, dynamic>("dbo.spControleMsgEMail_Read", new { DataMsg = dataMsg }, "SQLDB").ToList<ControleMsgEMailModel>())
                {
                    item.Logs = _log.GetLog(item.Id);

                    _resultado.Add(item);
                }

                return _resultado;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        ControleMsgEMailModel GetMsg(int idControleMsgEMail)
        {
            try
            {
                var estados = _dataAccess.LoadDataSync<ControleMsgEMailModel, dynamic>("dbo.spControleMsgEMail_ReadId", new { Id = idControleMsgEMail }, "SQLDB");
                return estados.ToList<ControleMsgEMailModel>().FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        string ApontaEnvioMsg(int idControleMsgEMail, string idcEnviado)
        {
            try
            {
                _dataAccess.SaveData("spControleMsgEMail_ApontaEnvio", new { Id = idControleMsgEMail, IdcEnviado = idcEnviado }, "SQLDB");
            }
            catch (Exception e)
            {
                return "Ops, ocorreu um erro, tente novamente: " + e.Message;
            }

            return string.Empty;
        }


        public string GravaMsg(ControleMsgEMailModel msg)
        {
            try
            {
                var _codMensagem = Guid.NewGuid();

                _dataAccess.SaveData("spControleMsgEMail_Create", new { CodMensagem = _codMensagem, 
                                                                        Destino = msg.Destino.Trim(), 
                                                                        Assunto = msg.Assunto.Trim(), 
                                                                        CorpoMsg = msg.CorpoMsg.Trim(), 
                                                                        NomeArquivo = msg.NomeArquivo, 
                                                                        ConteudoArquivo = msg.ConteudoArquivo}, "SQLDB");

                _dataAccess.SaveData("spContatoMsg_Create", new { CodMensagem = _codMensagem, IdcConcordaReceberInformacao = msg.IdcRecebeInformacao }, "SQLDB");
            }
            catch (Exception e)
            {
                return "Ops, ocorreu um erro, tente novamente: " + e.Message;
            }

            return string.Empty;
        }

        public string GravaLog(ControleMsgEMailLogModel log)
        {
            return _log.GravaLog(log);
        }

        public string Enviar(int idControleMsgEMail)
        {
            var _eMail = this.GetMsg(idControleMsgEMail);

            var _tmpArquivo = string.Empty;
            string _dirTmp = Directory.GetCurrentDirectory() + @"\tmp";

            if (!string.IsNullOrEmpty(_eMail.NomeArquivo))
            {
                if (!Directory.Exists(_dirTmp))
                {
                    Directory.CreateDirectory(_dirTmp);
                }

                _tmpArquivo = _dirTmp + @"\" + _eMail.NomeArquivo;

                if (File.Exists(_tmpArquivo))
                {
                    File.Delete(_tmpArquivo);
                }

                ArquivoBinario.ConvertArrayByteParaArquivo(_eMail.ConteudoArquivo, _tmpArquivo);
            }

            try
            {
                if (!string.IsNullOrEmpty(_eMail.NomeArquivo))
                {
                    _msgEMail.AdicionaAnexo(_tmpArquivo);
                }                    

                if (_msgEMail.Envia(_eMail.Destino, _eMail.Assunto, _eMail.CorpoMsg))
                {
                    this.GravaLog(new ControleMsgEMailLogModel() { IdControleMsgEMail = _eMail.Id, DescricaoLog = "Mensagem Enviada com sucesso!", TipoLog = "I" });

                    ApontaEnvioMsg(_eMail.Id, "S");
                }
            }
            catch (Exception ex)
            {
                var _msgErro = "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;

                this.GravaLog(new ControleMsgEMailLogModel() { IdControleMsgEMail = _eMail.Id, DescricaoLog = _msgErro, TipoLog = "E" });

                return _msgErro;
            }
            finally
            {
                File.Delete(_tmpArquivo);
                Directory.Delete(_dirTmp);
            }

            return string.Empty;
        }
    }
}
