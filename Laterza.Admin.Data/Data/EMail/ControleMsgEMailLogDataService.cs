﻿using Laterza.Admin.Data.Data.Interfaces.EMail;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.EMail;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Laterza.Admin.Data.Data.EMail
{
    public class ControleMsgEMailLogDataService : IControleMsgEMailLogDataService
    {
        readonly ISqlDataAccess _dataAccess;
        public ControleMsgEMailLogDataService(ISqlDataAccess dataAccess) => _dataAccess = dataAccess;

        public List<ControleMsgEMailLogModel> GetLog(int idControleMsgEMail)
        {
            try
            {
                return _dataAccess.LoadDataSync<ControleMsgEMailLogModel, dynamic>("dbo.spControleMsgEMailLog_Read", new { IdControleMsgEMail = idControleMsgEMail }, "SQLDB").ToList<ControleMsgEMailLogModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public string GravaLog(ControleMsgEMailLogModel log)
        {
            try
            {
                _dataAccess.LoadDataSync<string, dynamic>("dbo.spControleMsgEMailLog_Create", new { IdControleMsgEMail = log.IdControleMsgEMail, DescricaoLog = log.DescricaoLog, TipoLog = log.TipoLog}, "SQLDB");
            }
            catch (Exception e)
            {
                return "Ops, ocorreu um erro, tente novamente: " + e.Message;
            }

            return string.Empty;
        }
    }
}
