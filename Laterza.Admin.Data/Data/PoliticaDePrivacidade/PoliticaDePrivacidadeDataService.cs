﻿using Laterza.Admin.Data.Data.Interfaces.PoliticaDePrivacidade;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laterza.Admin.Data.Data.PoliticaDePrivacidade
{
    public class PoliticaDePrivacidadeDataService : IPoliticaDePrivacidadeDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public PoliticaDePrivacidadeDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<PoliticaDePrivacidadeModel> GetPoliticaDePrivacidade()
        {
            try
            {
                var PoliticaDePrivacidade = _dataAccess.LoadDataSync<PoliticaDePrivacidadeModel, dynamic>("dbo.spPoliticaDePrivacidade_Read", new { id = Guid.Empty }, "SQLDB");
                return PoliticaDePrivacidade.ToList<PoliticaDePrivacidadeModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public List<PoliticaDePrivacidadeModel> GetPoliticaDePrivacidade(Guid Id)
        {
            try
            {
                var PoliticaDePrivacidade = _dataAccess.LoadDataSync<PoliticaDePrivacidadeModel, dynamic>("dbo.spPoliticaDePrivacidade_Read", new { id = Id }, "SQLDB");
                return PoliticaDePrivacidade;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public string PoliticaDePrivacidadeCreate(IPoliticaDePrivacidadeModel PoliticaDePrivacidade)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spPoliticaDePrivacidade_Create", PoliticaDePrivacidade, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public void PoliticaDePrivacidadeUpdate(IPoliticaDePrivacidadeModel PoliticaDePrivacidade)
        {
            try
            {
                _dataAccess.SaveData("dbo.spPoliticaDePrivacidade_Update", PoliticaDePrivacidade, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void PoliticaDePrivacidadeDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spPoliticaDePrivacidade_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
