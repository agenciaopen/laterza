﻿using Laterza.Admin.Data.Data.Interfaces.Faq;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laterza.Admin.Data.Data.Faq
{
    public class FaqDataService : IFaqDataService

    { 
        private readonly ISqlDataAccess _dataAccess;

        public FaqDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<FaqModel> GetFaq()
        {
            try
            {
                var FaqModel = _dataAccess.LoadDataSync<FaqModel, dynamic>("dbo.spFaq_Read", new { id = Guid.Empty }, "SQLDB");
                return FaqModel.ToList<FaqModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<FaqModel> GetFaq(Guid id)
        {
            try
            {
                var escritorios = _dataAccess.LoadDataSync<FaqModel, dynamic>("dbo.spFaq_Read", new { Id = id }, "SQLDB");
                return escritorios.ToList<FaqModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public string FaqCreate(IFaqModel Faq)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spFaq_Create", Faq, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente: " + e.Message);
            }
        }

        public void FaqUpdate(IFaqModel Faq)
        {
            try
            {
                _dataAccess.SaveData("dbo.spFaq_Update", Faq, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void FaqDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spFaq_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
