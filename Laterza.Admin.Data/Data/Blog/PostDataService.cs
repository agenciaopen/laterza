﻿using Laterza.Admin.Data.Data.Interfaces.Blog;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Blog
{
    public class PostDataService : IPostDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public PostDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<PostModel> GetPost()
        {
            try
            {
                var post = _dataAccess.LoadDataSync<PostModel, dynamic>("dbo.spPost_Read", new { id = Guid.Empty }, "SQLDB");
                return post.ToList<PostModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public List<PostModel> GetPost(Guid Id)
        {
            try
            {
                var post = _dataAccess.LoadDataSync<PostModel, dynamic>("dbo.spPost_Read", new { id = Id }, "SQLDB");
                return post;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public string PostCreate(IPostModel post)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spPost_Create", post, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch(Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public void PostUpdate(IPostModel post)
        {
            try
            {
                _dataAccess.SaveData("dbo.spPost_Update", post, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public async Task PostDelete(Guid id)
        {
            try
            {
                await _dataAccess.SaveData("dbo.spPost_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
