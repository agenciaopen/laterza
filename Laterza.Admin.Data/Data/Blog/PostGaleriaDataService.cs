﻿using Laterza.Admin.Data.Data.Interfaces.Blog;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laterza.Admin.Data.Data.Blog
{
    public class PostGaleriaDataService : IPostGaleriaDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public PostGaleriaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<PostGaleriaModel> GetPostGaleria()
        {
            try
            {
                var PostGaleria = _dataAccess.LoadDataSync<PostGaleriaModel, dynamic>("dbo.spPostGaleria_Read", new { id = Guid.Empty }, "SQLDB");
                return PostGaleria.ToList<PostGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public List<PostGaleriaModel> GetPostGaleria(Guid Id, Guid IdPost)
        {
            try
            {
                var PostGaleria = _dataAccess.LoadDataSync<PostGaleriaModel, dynamic>("dbo.spPostGaleria_Read", new { Id = Id, IdPost = IdPost }, "SQLDB");
                return PostGaleria;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public string PostGaleriaCreate(IPostGaleriaModel PostGaleria)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spPostGaleria_Create", PostGaleria, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public void PostGaleriaUpdate(IPostGaleriaModel PostGaleria)
        {
            try
            {
                _dataAccess.SaveData("dbo.spPostGaleria_Update", PostGaleria, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void PostGaleriaOrderUpdate(IPostGaleriaModel postGaleria)
        {
            try
            {
                _dataAccess.LoadDataSync<PostGaleriaModel, dynamic>("dbo.spPostGaleriaOrdem_Update", new { Id = postGaleria.Id, Ordem = postGaleria.Ordem }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void PostGaleriaDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spPostGaleria_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
