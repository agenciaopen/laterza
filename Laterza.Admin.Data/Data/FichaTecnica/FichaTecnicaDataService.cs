﻿using Laterza.Admin.Data.Data.Interfaces.FichaTecnica;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.FichaTecnica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laterza.Admin.Data.Data.FichaTecnica
{
    public class FichaTecnicaDataService : IFichaTecnicaDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public FichaTecnicaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<FichaTecnicaModel> GetFichaTecnica()
        {
            try
            {
                var FichaTecnica = _dataAccess.LoadDataSync<FichaTecnicaModel, dynamic>("dbo.spFichaTecnica_Read", new { id = Guid.Empty, IdEmpreendimento = Guid.Empty }, "SQLDB");
                return FichaTecnica.ToList<FichaTecnicaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public List<FichaTecnicaModel> GetFichaTecnica(Guid Id, Guid IdEmpreendimento)
        {
            try
            {
                var FichaTecnica = _dataAccess.LoadDataSync<FichaTecnicaModel, dynamic>("dbo.spFichaTecnica_Read", new { Id = Id, IdEmpreendimento = IdEmpreendimento }, "SQLDB");
                return FichaTecnica;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public string FichaTecnicaCreate(IFichaTecnicaModel FichaTecnica)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spFichaTecnica_Create", FichaTecnica, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public void FichaTecnicaUpdate(IFichaTecnicaModel FichaTecnica)
        {
            try
            {
                _dataAccess.SaveData("dbo.spFichaTecnica_Update", FichaTecnica, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void FichaTecnicaDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spFichaTecnica_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
