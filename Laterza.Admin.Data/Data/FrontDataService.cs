﻿using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using Laterza.Admin.Data.Models.Blog;
using Laterza.Admin.Data.Models.Cidades;
using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.Escritorio;
using Laterza.Admin.Data.Models.Estados;
using Laterza.Admin.Data.Models.Faq;
using Laterza.Admin.Data.Models.FichaTecnica;
using Laterza.Admin.Data.Models.MdMidia;
using Laterza.Admin.Data.Models.Pagina;
using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using Laterza.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data
{
    public class FrontDataService : IFrontDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public FrontDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<EmpreendimentoModel> getEmpreendimentos { get; set; }
        public List<EstadosModel> getEstados { get; set; }
        public List<EmpreendimentoModel> getBairros { get; set; }
        public List<EmpreendimentoModel> getQuartos { get; set; }

        public string getEstado { get; set; }

        public List<CidadesModel> getEstadoList { get; set; }

        public List<EmpreendimentoModel> GetEmpreendimento()
        {
            try
            {
                if (getEmpreendimentos == null)
                {
                    getEmpreendimentos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read", new { id = Guid.Empty }, "SQLDB");
                }
                return getEmpreendimentos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoModel> GetEmpreendimento(Guid id)
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read", new { Id = id }, "SQLDB");
                return empreendimentos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoModel> GetEmpreendimento(string slug)
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read_BySlug", new { Slug = slug }, "SQLDB");
                return empreendimentos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria(Guid idEmpreendimento, int tipoGaleria)
        {
            try
            {
                var empreendimentoGalerias = _dataAccess.LoadDataSync<EmpreendimentoGaleriaModel, dynamic>("dbo.spEmpreendimentoGaleria_Read", new { Id = Guid.Empty, IdEmpreendimento = idEmpreendimento, TipoGaleria = tipoGaleria }, "SQLDB");
                return empreendimentoGalerias.ToList<EmpreendimentoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao(Guid idEmpreendimento)
        {
            try
            {
                var empreendimentoValorDescricaos = _dataAccess.LoadDataSync<EmpreendimentoValorDescricaoModel, dynamic>("dbo.spEmpreendimentoValorDescricao_Read", new { Id = Guid.Empty, IdEmpreendimento = idEmpreendimento }, "SQLDB");
                return empreendimentoValorDescricaos.ToList<EmpreendimentoValorDescricaoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EstadosModel> GetEstados()
        {
            try
            {
                if (getEstados == null)
                {
                    getEstados = _dataAccess.LoadDataSync<EstadosModel, dynamic>("dbo.spEstados_Read", new { Slug = "" }, "SQLDB");
                }
                return getEstados.ToList<EstadosModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas(Guid idEmpreendimento)
        {
            try
            {
                var empreendimentoInformacoesTecnicas = _dataAccess.LoadDataSync<EmpreendimentoInformacoesTecnicasModel, dynamic>("dbo.spEmpreendimentoInformacoesTecnicas_Read", new { id = Guid.Empty, IdEmpreendimento = idEmpreendimento }, "SQLDB");
                return empreendimentoInformacoesTecnicas.ToList<EmpreendimentoInformacoesTecnicasModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EscritorioModel> GetEscritorio()
        {
            try
            {
                var escritorios = _dataAccess.LoadDataSync<EscritorioModel, dynamic>("dbo.spEscritorio_Read", new { id = Guid.Empty }, "SQLDB");
                return escritorios.ToList<EscritorioModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<MdMidiaModel> GetMdMidia()
        {
            try
            {
                var mdMidias = _dataAccess.LoadDataSync<MdMidiaModel, dynamic>("dbo.spMdMidia_Read", new { id = Guid.Empty }, "SQLDB");
                return mdMidias.ToList<MdMidiaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<ProjetoModel> GetProjeto()
        {
            try
            {
                var projetos = _dataAccess.LoadDataSync<ProjetoModel, dynamic>("dbo.spProjeto_Read", new { id = Guid.Empty }, "SQLDB");
                return projetos.ToList<ProjetoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<ProjetoGaleriaModel> GetProjetoGaleria(Guid idProjeto)
        {
            try
            {
                var projetoGalerias = _dataAccess.LoadDataSync<ProjetoGaleriaModel, dynamic>("dbo.spProjetoGaleria_Read", new { Id = Guid.Empty, IdProjeto = idProjeto }, "SQLDB");
                return projetoGalerias.ToList<ProjetoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoModel> GetBairros()
        {
            try
            {
                if (getBairros == null)
                {
                    getBairros = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read_Distinct_Bairro", new { id = Guid.Empty }, "SQLDB");
                }
                return getBairros.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<CidadesModel> GetCidadesComEmpreendimentoByEstado(string estadoSlug)
        {
            try
            {
                if (getEstado == null)
                {
                    getEstado = estadoSlug;
                    getEstadoList = _dataAccess.LoadDataSync<CidadesModel, dynamic>("dbo.spCidadesEmpreendimentos_Read", new { SlugEstado = estadoSlug }, "SQLDB");
                }
                else if (getEstado != estadoSlug)
                {
                    getEstado = estadoSlug;
                    getEstadoList = _dataAccess.LoadDataSync<CidadesModel, dynamic>("dbo.spCidadesEmpreendimentos_Read", new { SlugEstado = estadoSlug }, "SQLDB");
                }

                return getEstadoList.ToList<CidadesModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente: " + e.Message);
            }
        }

        public List<EstadosModel> GetEstadosComEmpreendimento()
        {
            try
            {
                if (getEstados == null)
                {
                    getEstados = _dataAccess.LoadDataSync<EstadosModel, dynamic>("dbo.spEstadosEmpreendimento_Read", null, "SQLDB");
                }
                return getEstados.ToList<EstadosModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente: " + e.Message);
            }
        }

        public List<EmpreendimentoModel> GetQuartos()
        {
            try
            {
                if (getQuartos == null)
                {
                    getQuartos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read_Distinct_Quartos", new { id = Guid.Empty }, "SQLDB");
                }
                return getQuartos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EstadosModel> GetEstados(string slug)
        {
            try
            {
                var estados = _dataAccess.LoadDataSync<EstadosModel, dynamic>("dbo.spEstados_Read", new { Slug = slug }, "SQLDB");
                return estados.ToList<EstadosModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<PaginaModel> GetPagina(string slug)
        {
            try
            {
                var pagina = _dataAccess.LoadDataSync<PaginaModel, dynamic>("dbo.spPagina_Read", new { Slug = slug }, "SQLDB");
                return pagina.ToList<PaginaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<FaqModel> GetFaq()
        {
            try
            {
                var FaqModel = _dataAccess.LoadDataSync<FaqModel, dynamic>("dbo.spFaq_Read", new { id = Guid.Empty }, "SQLDB");
                return FaqModel.ToList<FaqModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<PostModel> GetPosts()
        {
            try
            {
                var PostModel = _dataAccess.LoadDataSync<PostModel, dynamic>("dbo.spPost_Read", new { id = Guid.Empty }, "SQLDB");
                return PostModel.ToList<PostModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<PostModel> GetPosts(Guid id)
        {
            try
            {
                var PostModel = _dataAccess.LoadDataSync<PostModel, dynamic>("dbo.spPost_Read", new { id = id }, "SQLDB");
                return PostModel.ToList<PostModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<PostGaleriaModel> GetPostGaleria(Guid idPost)
        {
            try
            {
                var PostGaleriaModel = _dataAccess.LoadDataSync<PostGaleriaModel, dynamic>("dbo.spPostGaleria_Read", new { Id = Guid.Empty, IdPost = idPost }, "SQLDB");
                return PostGaleriaModel.ToList<PostGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public PoliticaDePrivacidadeModel GetPoliticaDePrivacidade()
        {
            try
            {
                var PoliticaDePrivacidadeModel = _dataAccess.LoadDataSync<PoliticaDePrivacidadeModel, dynamic>("dbo.spPoliticaDePrivacidade_Read", new { Id = Guid.Empty }, "SQLDB");
                return PoliticaDePrivacidadeModel.First<PoliticaDePrivacidadeModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<BeneficioPagamentoModel> GetBeneficioPagamentos(Guid IdEmpreendimento)
        {
            try
            {
                var BeneficiosPagamentoModel = _dataAccess.LoadDataSync<BeneficioPagamentoModel, dynamic>("dbo.spBeneficioPagamento_Read", 
                                                        new { Id = Guid.Empty, IdEmpreendimento = IdEmpreendimento }, "SQLDB");
                return BeneficiosPagamentoModel.ToList<BeneficioPagamentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<FichaTecnicaModel> GetFichaTecnica(Guid IdEmpreendimento)
        {
            try
            {
                var fichasTecnicas = _dataAccess.LoadDataSync<FichaTecnicaModel, dynamic>("dbo.spFichaTecnica_Read",
                                                        new { Id = Guid.Empty, IdEmpreendimento = IdEmpreendimento }, "SQLDB");
                return fichasTecnicas.ToList<FichaTecnicaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoComFichaTecnica()
        {
            try
            {
                var empreendimentos = _dataAccess.LoadEmpreendimentosComFichaTecnica("dbo.spEmpreendimentoFichaTecnica_Read", "SQLDB");
                return empreendimentos;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}