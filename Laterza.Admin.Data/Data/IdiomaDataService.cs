﻿using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Idioma;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data
{
    public class IdiomaDataService : IIdiomaDataService
    {
        private readonly ISqlDataAccess _dataAccess;
        private List<IIdiomaModel> idiomas { get; set; }
        public IdiomaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<IIdiomaModel> Get()
        {
            try
            {
                if (idiomas == null)
                {
                    var result = _dataAccess.LoadDataSync<IdiomaModel, dynamic>("dbo.spIdioma_Read", new { }, "SQLDB");
                    idiomas = result.ToList<IIdiomaModel>();
                }
                return idiomas;
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente: " + e);
            }
        }
    }
}
