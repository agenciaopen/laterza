﻿using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Empreendimento
{
    public class EmpreendimentoInformacoesTecnicasDataService : IEmpreendimentoInformacoesTecnicasDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public EmpreendimentoInformacoesTecnicasDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public int EmpreendimentoInformacoesTecnicasCreate(IEmpreendimentoInformacoesTecnicasModel empreendimentoInformacoesTecnicas)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<int, dynamic>("dbo.spEmpreendimentoInformacoesTecnicas_Create", empreendimentoInformacoesTecnicas, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas()
        {
            try
            {
                var empreendimentoInformacoesTecnicas = _dataAccess.LoadDataSync<EmpreendimentoInformacoesTecnicasModel, dynamic>("dbo.spEmpreendimentoInformacoesTecnicas_Read", new { id = Guid.Empty }, "SQLDB");
                return empreendimentoInformacoesTecnicas.ToList<EmpreendimentoInformacoesTecnicasModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas(Guid id, Guid idEmpreendimento)
        {
            try
            {
                var empreendimentoInformacoesTecnicas = _dataAccess.LoadDataSync<EmpreendimentoInformacoesTecnicasModel, dynamic>("dbo.spEmpreendimentoInformacoesTecnicas_Read", new { Id = id, IdEmpreendimento = idEmpreendimento }, "SQLDB");
                return empreendimentoInformacoesTecnicas.ToList<EmpreendimentoInformacoesTecnicasModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoInformacoesTecnicasUpdate(IEmpreendimentoInformacoesTecnicasModel empreendimentoInformacoesTecnicas)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoInformacoesTecnicas_Update", empreendimentoInformacoesTecnicas, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoInformacoesTecnicasDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoInformacoesTecnicas_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
