﻿using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Empreendimento
{
    public class EmpreendimentoGaleriaDataService : IEmpreendimentoGaleriaDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public EmpreendimentoGaleriaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public int EmpreendimentoGaleriaCreate(IEmpreendimentoGaleriaModel empreendimentoGaleria)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<int, dynamic>("dbo.spEmpreendimentoGaleria_Create", empreendimentoGaleria, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria()
        {
            try
            {
                var empreendimentoGalerias = _dataAccess.LoadDataSync<EmpreendimentoGaleriaModel, dynamic>("dbo.spEmpreendimentoGaleria_Read", new { id = Guid.Empty }, "SQLDB");
                return empreendimentoGalerias.ToList<EmpreendimentoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria(Guid id, Guid idEmpreendimento, int tipoGaleria)
        {
            try
            {
                var empreendimentoGalerias = _dataAccess.LoadDataSync<EmpreendimentoGaleriaModel, dynamic>("dbo.spEmpreendimentoGaleria_Read", new { Id = id, IdEmpreendimento = idEmpreendimento, TipoGaleria = tipoGaleria }, "SQLDB");
                return empreendimentoGalerias.ToList<EmpreendimentoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoGaleriaUpdate(IEmpreendimentoGaleriaModel empreendimentoGaleria)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoGaleria_Update", empreendimentoGaleria, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoGaleriaOrderUpdate(IEmpreendimentoGaleriaModel empreendimentoGaleria)
        {
            try
            {
                _dataAccess.LoadDataSync<EmpreendimentoGaleriaModel, dynamic>("dbo.spEmpreendimentoGaleriaOrdem_Update", empreendimentoGaleria, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoGaleriaDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoGaleria_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
