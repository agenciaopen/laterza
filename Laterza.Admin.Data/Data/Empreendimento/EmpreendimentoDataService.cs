﻿using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Empreendimento
{
    public class EmpreendimentoDataService : IEmpreendimentoDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public EmpreendimentoDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public string EmpreendimentoCreate(IEmpreendimentoModelUpdateCreate empreendimento)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spEmpreendimento_Create", empreendimento, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }
        public List<EmpreendimentoModel> GetEmpreendimento()
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read", new { id = Guid.Empty }, "SQLDB");
                return empreendimentos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoModel> GetEmpreendimento(Guid id)
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read", new { Id = id }, "SQLDB");
                return empreendimentos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoUpdate(IEmpreendimentoModelUpdateCreate empreendimento)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimento_Update", empreendimento, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimento_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoRelacionadoDelete(Guid idEmpreendimento, Guid idEmpreendimentoRelacionado)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoRelacionado_Delete", new { IdEmpreendimento = idEmpreendimento, IdEmpreendimentoRelacionado = idEmpreendimentoRelacionado }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<Guid> GetEmpreendimentoRelacionado(Guid idEmpreendimento)
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<Guid, dynamic>("dbo.spEmpreendimentoRelacionado_Read", new { IdEmpreendimento = idEmpreendimento }, "SQLDB");
                return empreendimentos.ToList<Guid>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }

        public string EmpreendimentoRelacionadoCreate(Guid idEmpreendimento, Guid idEmpreendimentoRelacionado)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spEmpreendimentoRelacionado_Create", new { IdEmpreendimento = idEmpreendimento, IdEmpreendimentoRelacionado = idEmpreendimentoRelacionado }, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }
    }
}
