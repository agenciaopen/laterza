﻿using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Empreendimento
{
    public class EmpreendimentoValorDescricaoDataService : IEmpreendimentoValorDescricaoDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public EmpreendimentoValorDescricaoDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public int EmpreendimentoValorDescricaoCreate(IEmpreendimentoValorDescricaoModel empreendimentoValorDescricao)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<int, dynamic>("dbo.spEmpreendimentoValorDescricao_Create", empreendimentoValorDescricao, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao()
        {
            try
            {
                var empreendimentoValorDescricaos = _dataAccess.LoadDataSync<EmpreendimentoValorDescricaoModel, dynamic>("dbo.spEmpreendimentoValorDescricao_Read", new { id = Guid.Empty }, "SQLDB");
                return empreendimentoValorDescricaos.ToList<EmpreendimentoValorDescricaoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao(Guid id, Guid idEnpreendimento)
        {
            try
            {
                var empreendimentoValorDescricaos = _dataAccess.LoadDataSync<EmpreendimentoValorDescricaoModel, dynamic>("dbo.spEmpreendimentoValorDescricao_Read", new { Id = id, IdEmpreendimento = idEnpreendimento }, "SQLDB");
                return empreendimentoValorDescricaos.ToList<EmpreendimentoValorDescricaoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoValorDescricaoUpdate(IEmpreendimentoValorDescricaoModel empreendimentoValorDescricao)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoValorDescricao_Update", empreendimentoValorDescricao, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoValorDescricaoDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoValorDescricao_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
