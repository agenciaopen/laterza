﻿using Laterza.Admin.Data.Models.Escritorio;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Escritorio
{
    public interface IEscritorioDataService
    {
        List<EscritorioModel> GetEscritorio();
        List<EscritorioModel> GetEscritorio(Guid id);
        string EscritorioCreate(IEscritorioModel escritorio);
        void EscritorioUpdate(IEscritorioModel escritorio);
        void EscritorioDelete(Guid id);
    }
}