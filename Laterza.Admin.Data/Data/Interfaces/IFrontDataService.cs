﻿using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.Escritorio;
using Laterza.Admin.Data.Models.Estados;
using Laterza.Admin.Data.Models.MdMidia;
using Laterza.Admin.Data.Models.Projeto;
using Laterza.Admin.Data.Models.Pagina;
using Laterza.Admin.Data.Models.Faq;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Laterza.Admin.Data.Models.Cidades;
using Laterza.Admin.Data.Models.Blog;
using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using Laterza.Admin.Data.Models.FichaTecnica;

namespace Laterza.Admin.Data.Data
{
    public interface IFrontDataService
    {

        List<EmpreendimentoModel> GetEmpreendimento();
        List<EmpreendimentoModel> GetEmpreendimento(Guid id);
        List<EmpreendimentoModel> GetEmpreendimento(string slug);
        List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria(Guid idEmpreendimento, int tipoGaleria);
        List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao(Guid idEmpreendimento);
        List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas(Guid idEmpreendimento);
        List<EscritorioModel> GetEscritorio();
        List<MdMidiaModel> GetMdMidia();
        List<ProjetoModel> GetProjeto();
        List<ProjetoGaleriaModel> GetProjetoGaleria(Guid idProjeto);
        List<EmpreendimentoModel> GetBairros();
        List<EmpreendimentoModel> GetQuartos();
        List<EstadosModel> GetEstados();
        List<EstadosModel> GetEstados(string slug);
        List<PaginaModel> GetPagina(string slug);
        List<FaqModel> GetFaq();
        List<CidadesModel> GetCidadesComEmpreendimentoByEstado(string estadoSlug);
        List<EstadosModel> GetEstadosComEmpreendimento();
        List<PostModel> GetPosts();
        List<PostModel> GetPosts(Guid id);
        List<PostGaleriaModel> GetPostGaleria(Guid idPost);
        PoliticaDePrivacidadeModel GetPoliticaDePrivacidade();
        List<BeneficioPagamentoModel> GetBeneficioPagamentos(Guid IdEmpreendimento);
        List<FichaTecnicaModel> GetFichaTecnica(Guid IdEmpreendimento);
        List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoComFichaTecnica();
    }
}