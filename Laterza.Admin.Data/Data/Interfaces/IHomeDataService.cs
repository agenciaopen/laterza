﻿using Laterza.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data
{
    public interface IHomeDataService
    {
        List<HomeBannerModel> GetBanner();
        List<HomeBannerModel> GetBanner(Guid id);
        Task<int> HomeBannerCreate(IHomeBannerModel banner);
        Task HomeBannerUpdate(IHomeBannerModel banner);
        void HomeBannerDelete(Guid id);
    }
}