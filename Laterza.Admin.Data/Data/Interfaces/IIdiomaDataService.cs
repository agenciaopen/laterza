﻿using Laterza.Admin.Data.Models.Idioma;
using System.Collections.Generic;

namespace Laterza.Admin.Data.Data
{
    public interface IIdiomaDataService
    {
        List<IIdiomaModel> Get();
    }
}