﻿using Laterza.Admin.Data.Models.Pagina;
using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Data.Interfaces.Pagina
{
    public interface IPaginaDataService
    {
        List<PaginaModel> GetPagina(string slug);
        string PaginaCreate(IPaginaModel escritorio);
        void PaginaUpdate(IPaginaModel escritorio);
    }
}
