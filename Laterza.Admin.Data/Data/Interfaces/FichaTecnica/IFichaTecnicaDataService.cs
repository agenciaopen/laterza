﻿using Laterza.Admin.Data.Models.FichaTecnica;
using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Data.Interfaces.FichaTecnica
{
    public interface IFichaTecnicaDataService
    {
        List<FichaTecnicaModel> GetFichaTecnica();
        List<FichaTecnicaModel> GetFichaTecnica(Guid Id, Guid IdEmpreendimento);
        string FichaTecnicaCreate(IFichaTecnicaModel FichaTecnica);
        void FichaTecnicaDelete(Guid Id);
        void FichaTecnicaUpdate(IFichaTecnicaModel FichaTecnica);
    }
}
