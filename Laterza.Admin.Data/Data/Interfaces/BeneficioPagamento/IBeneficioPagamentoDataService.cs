﻿using Laterza.Admin.Data.Models.BeneficioPagamento;
using System;
using System.Collections.Generic;

namespace Laterza.Admin.Data.Data.Interfaces.BeneficioPagamento
{
    public interface IBeneficioPagamentoDataService
    {
        List<BeneficioPagamentoModel> GetBeneficioPagamento();
        List<BeneficioPagamentoModel> GetBeneficioPagamento(Guid Id, Guid IdEmpreendimento);
        string BeneficioPagamentoCreate(IBeneficioPagamentoModel BeneficioPagamento);
        void BeneficioPagamentoDelete(Guid Id);
        void BeneficioPagamentoUpdate(IBeneficioPagamentoModel BeneficioPagamento);
    }
}
