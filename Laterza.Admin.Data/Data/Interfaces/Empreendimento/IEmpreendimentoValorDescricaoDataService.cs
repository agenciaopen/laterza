﻿using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Empreendimento
{
    public interface IEmpreendimentoValorDescricaoDataService
    {
        List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao();
        List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao(Guid id, Guid idEmpreendimento);
        int EmpreendimentoValorDescricaoCreate(IEmpreendimentoValorDescricaoModel empreendimento);
        void EmpreendimentoValorDescricaoUpdate(IEmpreendimentoValorDescricaoModel empreendimento);
        void EmpreendimentoValorDescricaoDelete(Guid id);
    }
}