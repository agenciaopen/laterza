﻿using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Empreendimento
{
    public interface IEmpreendimentoDataService
    {
        List<EmpreendimentoModel> GetEmpreendimento();
        List<EmpreendimentoModel> GetEmpreendimento(Guid id);
        string EmpreendimentoCreate(IEmpreendimentoModelUpdateCreate empreendimento);
        void EmpreendimentoUpdate(IEmpreendimentoModelUpdateCreate empreendimento);
        void EmpreendimentoDelete(Guid id);
        List<Guid> GetEmpreendimentoRelacionado(Guid idEmpreendimento);
        string EmpreendimentoRelacionadoCreate(Guid idEmpreendimento, Guid idEmpreendimentoRelacionado);
        void EmpreendimentoRelacionadoDelete(Guid idEmpreendimento, Guid idEmpreendimentoRelacionado);
    }
}