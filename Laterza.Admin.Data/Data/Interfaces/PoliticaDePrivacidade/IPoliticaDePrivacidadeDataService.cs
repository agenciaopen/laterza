﻿using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Data.Interfaces.PoliticaDePrivacidade
{
    public interface IPoliticaDePrivacidadeDataService
    {
        List<PoliticaDePrivacidadeModel> GetPoliticaDePrivacidade();
        List<PoliticaDePrivacidadeModel> GetPoliticaDePrivacidade(Guid Id);
        string PoliticaDePrivacidadeCreate(IPoliticaDePrivacidadeModel PoliticaDePrivacidade);
        void PoliticaDePrivacidadeDelete(Guid Id);
        void PoliticaDePrivacidadeUpdate(IPoliticaDePrivacidadeModel PoliticaDePrivacidade);
    }
}
