﻿using Laterza.Admin.Data.Models.Blog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Data.Interfaces.Blog
{
    public interface IPostGaleriaDataService
    {
        List<PostGaleriaModel> GetPostGaleria();
        List<PostGaleriaModel> GetPostGaleria(Guid Id, Guid IdPost);
        string PostGaleriaCreate(IPostGaleriaModel postGaleria);
        void PostGaleriaDelete(Guid Id);
        void PostGaleriaUpdate(IPostGaleriaModel postGaleria);
        void PostGaleriaOrderUpdate(IPostGaleriaModel postGaleria);
    }
}
