﻿using Laterza.Admin.Data.Models.Blog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Interfaces.Blog
{
    public interface IPostDataService
    {
        List<PostModel> GetPost();
        List<PostModel> GetPost(Guid Id);
        string PostCreate(IPostModel post);
        Task PostDelete(Guid Id);
        void PostUpdate(IPostModel post);
    }
}
