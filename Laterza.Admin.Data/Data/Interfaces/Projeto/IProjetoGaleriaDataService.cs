﻿using Laterza.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Projeto
{
    public  interface IProjetoGaleriaDataService
    {
        List<ProjetoGaleriaModel> GetProjetoGaleria();
        List<ProjetoGaleriaModel> GetProjetoGaleria(Guid id, Guid idProjeto);
        int ProjetoGaleriaCreate(IProjetoGaleriaModel projetoGaleria);
        void ProjetoGaleriaUpdate(IProjetoGaleriaModel projetoGaleria);
        void ProjetoGaleriaDelete(Guid id);
    }
}