﻿using Laterza.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data.Projeto
{
    public interface IProjetoDataService
    {
        List<ProjetoModel> GetProjeto();
        List<ProjetoModel> GetProjeto(Guid id);
        string ProjetoCreate(IProjetoModel projeto);
        void ProjetoUpdate(IProjetoModel projeto);
        void ProjetoDelete(Guid id);
    }
}