﻿using Laterza.Admin.Data.Models.Cidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Data.Interfaces.Cidades
{
    public interface ICidadesDataService
    {
        List<CidadesModel> GetCidades();
        List<CidadesModel> GetCidades(string slug, string slugEstado);
    }
}
