﻿using Laterza.Admin.Data.Models.EMail;
using System.Collections.Generic;

namespace Laterza.Admin.Data.Data.Interfaces.EMail
{
    public interface IControleMsgEMailLogDataService
    {
        List<ControleMsgEMailLogModel> GetLog(int idControleMsgEMail);

        string GravaLog(ControleMsgEMailLogModel log);
    }
}
