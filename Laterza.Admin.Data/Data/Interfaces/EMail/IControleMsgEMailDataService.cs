﻿using Laterza.Admin.Data.Models.EMail;
using System;
using System.Collections.Generic;

namespace Laterza.Admin.Data.Data.Interfaces.EMail
{
    public interface IControleMsgEMailDataService
    {
        List<ControleMsgEMailModel> GetMsg(DateTime dataMsg);

        string GravaMsg(ControleMsgEMailModel msg);

        string GravaLog(ControleMsgEMailLogModel log);

        string Enviar(int idControleMsgEMail);
    }
}
