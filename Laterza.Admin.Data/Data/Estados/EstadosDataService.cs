﻿using Laterza.Admin.Data.Data.Interfaces.Estados;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Estados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laterza.Admin.Data.Data.Estados
{
    public class EstadosDataService : IEstadosDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public EstadosDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public List<EstadosModel> GetEstados()
        {
            try
            {
                var estados = _dataAccess.LoadDataSync<EstadosModel, dynamic>("dbo.spEstados_Read", new { Slug = "" }, "SQLDB");
                return estados.ToList<EstadosModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EstadosModel> GetEstados(string slug)
        {
            try
            {
                var estados = _dataAccess.LoadDataSync<EstadosModel, dynamic>("dbo.spEstados_Read", new { Slug = slug }, "SQLDB");
                return estados.ToList<EstadosModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
