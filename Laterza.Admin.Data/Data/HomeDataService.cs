﻿using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.Data
{
    public class HomeDataService : IHomeDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public HomeDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public async Task<int> HomeBannerCreate(IHomeBannerModel banner)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<int, dynamic>("dbo.spHomeBanner_Create", banner, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<HomeBannerModel> GetBanner()
        {
            try
            {
                var banners =  _dataAccess.LoadDataSync<HomeBannerModel, dynamic>("dbo.spHomeBanner_Read", new { id = Guid.Empty }, "SQLDB");
                return banners.ToList<HomeBannerModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<HomeBannerModel> GetBanner(Guid id)
        {
            try
            {
                var banners = _dataAccess.LoadDataSync<HomeBannerModel, dynamic>("dbo.spHomeBanner_Read", new { Id = id }, "SQLDB");
                return banners.ToList<HomeBannerModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public async Task HomeBannerUpdate(IHomeBannerModel banner)
        {
            try
            {
                await _dataAccess.SaveData("dbo.spHomeBanner_Update", banner, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public async void HomeBannerDelete(Guid id)
        {
            try
            {
                await _dataAccess.SaveData("dbo.spHomeBanner_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
