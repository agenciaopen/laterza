﻿using Dapper;
using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.FichaTecnica;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.DataAccess
{
    public class SqlDataAccess : ISqlDataAccess
    {
        private readonly IConfiguration _config;

        public SqlDataAccess(IConfiguration config)
        {
            _config = config;
        }
        public async Task<List<T>> LoadData<T, U>(string storedProcedure, U parameters, string connectionStringName)
        {
            string connectionString = _config.GetConnectionString(connectionStringName);

           
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var rows = await connection.QueryAsync<T>(storedProcedure, parameters,
                    commandType: CommandType.StoredProcedure);

                return rows.ToList();
            }
            
        }
        public List<EmpreendimentoFichaTecnicaModel> LoadEmpreendimentosComFichaTecnica(string storedProcedure, string connectionStringName)
        {
            string connectionString = _config.GetConnectionString(connectionStringName);

            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var empreendimentoDictionary = new Dictionary<Guid, EmpreendimentoFichaTecnicaModel>();


                var list = connection.Query<EmpreendimentoFichaTecnicaModel, FichaTecnicaModel, EmpreendimentoFichaTecnicaModel>(
                    storedProcedure,
                    (empreendimento, fichaTecnica) =>
                    {
                        EmpreendimentoFichaTecnicaModel empreendimentoEntry;

                        if (!empreendimentoDictionary.TryGetValue(empreendimento.Id, out empreendimentoEntry))
                        {
                            empreendimentoEntry = empreendimento;
                            empreendimentoEntry.FichasTecnicas = new List<FichaTecnicaModel>();
                            empreendimentoDictionary.Add(empreendimentoEntry.Id, empreendimentoEntry);
                        }

                        if(fichaTecnica != null)
                            empreendimentoEntry.FichasTecnicas.Add(fichaTecnica);
                        return empreendimentoEntry;
                    },
                    splitOn: "Id")
                .Distinct()
                .ToList();

                return list;
            }
        }

        //public IEnumerable<TParent> QueryParentChild<TParent, TChild, TParentKey>(
        //    string storedProcedure,
        //    string connectionStringName,
        //    Func<TParent, TParentKey> parentKeySelector,
        //    Func<TParent, IList<TChild>> childSelector,
        //    dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null)
        //{
        //    string connectionString = _config.GetConnectionString(connectionStringName);
        //    Dictionary<TParentKey, TParent> cache = new Dictionary<TParentKey, TParent>();

        //    using (IDbConnection connection = new SqlConnection(connectionString))
        //    {
        //        connection.Query<TParent, TChild, TParent>(
        //        storedProcedure,
        //        (parent, child) =>
        //        {
        //            if (!cache.ContainsKey(parentKeySelector(parent)))
        //            {
        //                cache.Add(parentKeySelector(parent), parent);
        //            }

        //            TParent cachedParent = cache[parentKeySelector(parent)];
        //            IList<TChild> children = childSelector(cachedParent);
        //            children.Add(child);
        //            return cachedParent;
        //        },
        //        param as object, transaction, buffered, splitOn, commandTimeout, commandType);

        //        return cache.Values;
        //    }          
        //}

        public List<T> LoadDataSync<T, U>(string storedProcedure, U parameters, string connectionStringName)
        {
            string connectionString = _config.GetConnectionString(connectionStringName);


            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var rows = connection.Query<T>(storedProcedure, parameters,
                    commandType: CommandType.StoredProcedure);

                return rows.ToList();
            }

        }

        public async Task SaveData<T>(string storedProcedure, T parameters, string connectionStringName)
        {
            string connectionString = _config.GetConnectionString(connectionStringName);

            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var rows = await connection.QueryAsync(storedProcedure, parameters, commandType: CommandType.StoredProcedure); 
            }
        }
    }
}
