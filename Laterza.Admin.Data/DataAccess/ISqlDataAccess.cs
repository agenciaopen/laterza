﻿using Laterza.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Laterza.Admin.Data.DataAccess
{
    public interface ISqlDataAccess
    {
        Task<List<T>> LoadData<T, U>(string storedProcedure, U parameters, string connectionStringName);
        List<T> LoadDataSync<T, U>(string storedProcedure, U parameters, string connectionStringName);
        Task SaveData<T>(string storedProcedure, T parameters, string connectionStringName);
        List<EmpreendimentoFichaTecnicaModel> LoadEmpreendimentosComFichaTecnica(string storedProcedure, string connectionStringName);
        //IEnumerable<TParent> QueryParentChild<TParent, TChild, TParentKey>(
        //            string storedProcedure,
        //            string connectionStringName,
        //            Func<TParent, TParentKey> parentKeySelector,
        //            Func<TParent, IList<TChild>> childSelector,
        //            dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
    }
}