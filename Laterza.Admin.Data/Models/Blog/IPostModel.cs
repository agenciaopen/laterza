﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.Blog
{
    public interface IPostModel
    {
        Guid Id { get; set; }
        string Titulo { get; set; }
        string Descricao { get; set; }
        string UrlImagemDestaque { get; set; }
        bool Destacar { get; set; }
        string Categoria { get; set; }
        string Conteudo { get; set; }
        string Slug { get; set; }
        DateTime DataCriacao { get; set; }
        DateTime DataAlteracao { get; set; }
    }
}
