﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.Blog
{
    public interface IPostGaleriaModel
    {
        Guid Id { get; set; }
        Guid IdPost { get; set; }
        string UrlImagem { get; set; }
        string UrlVideo { get; set; }
        string Descricao { get; set; }
        string Titulo { get; set; }
        bool EhVideo { get; set; }
        int Ordem { get; set; }
        string GetIdUrlVideo();
    }
}
