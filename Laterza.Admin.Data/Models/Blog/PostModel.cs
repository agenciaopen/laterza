﻿using System;

namespace Laterza.Admin.Data.Models.Blog
{
    public class PostModel : IPostModel
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string UrlImagemDestaque { get; set; }
        public bool Destacar { get; set; }
        public string Categoria { get; set; }
        public string Conteudo { get; set; }
        public string Slug { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
