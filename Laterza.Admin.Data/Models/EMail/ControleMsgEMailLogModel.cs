﻿using System;

namespace Laterza.Admin.Data.Models.EMail
{
    public class ControleMsgEMailLogModel: IControleMsgEMailLogModel
    {
        public int IdControleMsgEMail { get; set; }

        public int IdLog { get; set; }

        public string DescricaoLog { get; set; }

        public string TipoLog { get; set; }

        public DateTime DataLog { get; set; }
    }
}
