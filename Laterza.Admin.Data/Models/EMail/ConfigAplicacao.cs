﻿
namespace Laterza.Admin.Data.Models.EMail
{
    public class ConfigAplicacao
    {
        public string ServidorSMTP { get; set; }
        public int Porta { get; set; }
        public string EndRemetente { get; set; }
        public string Senha { get; set; }

        public int QteIntervalEnvioEMailMin { get; set; }

        public string DestinoContato { get; set; }

        public string DestinoTrabalheConoco { get; set; }
    }
}
