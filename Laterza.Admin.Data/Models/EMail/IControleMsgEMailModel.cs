﻿using System;
using System.Collections.Generic;

namespace Laterza.Admin.Data.Models.EMail
{
    public interface IControleMsgEMailModel
    {
        int Id { get; set; }

        DateTime DataMsg { get; set; }

        string Destino { get; set; }

        string Assunto { get; set; }

        string CorpoMsg { get; set; }

        string IdcEnviado { get; set; }

        List<ControleMsgEMailLogModel> Logs { get; set; }
    }
}
