﻿using System;

namespace Laterza.Admin.Data.Models.EMail
{
    public interface IControleMsgEMailLogModel
    {
        int IdControleMsgEMail { get; set; }

        int IdLog { get; set; }

        string DescricaoLog { get; set; }

        string TipoLog { get; set; }

        DateTime DataLog { get; set; }
    }
}
