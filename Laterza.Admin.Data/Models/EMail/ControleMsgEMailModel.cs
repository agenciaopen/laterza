﻿using System;
using System.Collections.Generic;

namespace Laterza.Admin.Data.Models.EMail
{
    public class ControleMsgEMailModel: IControleMsgEMailModel
    {
        public int Id { get; set; }

        public DateTime DataMsg { get; set; }

        public Guid CodMensagem { get; set; }

        public string Destino { get; set; }

        public string Assunto { get; set; }

        public string CorpoMsg { get; set; }

        public string IdcEnviado { get; set; }

        public string NomeArquivo { get; set; }

        public byte[] ConteudoArquivo { get; set; }

        public List<ControleMsgEMailLogModel> Logs { get; set; }

        public string IdcRecebeInformacao { get; set; }
    }
}
