﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.Cidades
{
    public class CidadesModel : ICidadesModel
    {
        public string UF { get; set; }
        public string Nome { get; set; }
        public string Slug { get; set; }
    }
}
