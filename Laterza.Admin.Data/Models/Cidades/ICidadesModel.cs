﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.Cidades
{
    public interface ICidadesModel
    {
        string UF { get; set; }
        string Nome { get; set; }
        string Slug { get; set; }
    }
}
