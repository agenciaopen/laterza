﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.Idioma
{
    public class IdiomaModel : IIdiomaModel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
    }
}
