﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.PoliticaDePrivacidade
{
    public class PoliticaDePrivacidadeModel : IPoliticaDePrivacidadeModel
    {
        public Guid Id { get; set; }
        public string Conteudo { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
