﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.PoliticaDePrivacidade
{
    public interface IPoliticaDePrivacidadeModel
    {
        Guid Id { get; set; }
        string Conteudo { get; set; }
        DateTime DataCriacao { get; set; }
        DateTime DataAlteracao { get; set; }
    }
}
