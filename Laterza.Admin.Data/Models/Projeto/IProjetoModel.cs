﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Laterza.Admin.Data.Models.Projeto
{
    public interface IProjetoModel
    {
        Guid Id { get; set; }
        string IdiomaId { get; set; }
        string Nome { get; set; }
        string Descricao { get; set; }
        string Endereco { get; set; }
        string Estado { get; set; }
        string Cidade { get; set; }
        string UrlImagem { get; set; }
        bool ProjetoSocial { get; set; }
        string EstadoNome { get; set; }
        string CidadeNome { get; set; }
    }
}