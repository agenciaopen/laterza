﻿using System;
using System.Web;

namespace Laterza.Admin.Data.Models.Projeto
{
    public interface IProjetoGaleriaModel
    {
        Guid Id { get; set; }
        Guid IdProjeto { get; set; }
        string IdiomaId { get; set; }
        string UrlImagem { get; set; }
        string UrlVideo { get; set; }
        string Titulo { get; set; }
        bool EhVideo { get; set; }
        int Ordem { get; set; }
        string GetIdUrlVideo();
    }
}