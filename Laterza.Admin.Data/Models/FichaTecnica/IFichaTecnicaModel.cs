﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.FichaTecnica
{
    public interface IFichaTecnicaModel
    {
        Guid Id { get; set; }
        string IdiomaId { get; set; }
        Guid IdEmpreendimento { get; set; }
        string Titulo { get; set; }
        bool Destacar { get; set; }
    }
}
