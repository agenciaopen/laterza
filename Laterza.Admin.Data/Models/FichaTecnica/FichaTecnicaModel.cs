﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.FichaTecnica
{
    public class FichaTecnicaModel : IFichaTecnicaModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string Titulo { get; set; }
        public bool Destacar { get; set; }
    }
}
