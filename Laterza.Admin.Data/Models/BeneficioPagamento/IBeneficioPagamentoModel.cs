﻿using System;

namespace Laterza.Admin.Data.Models.BeneficioPagamento
{
    public interface IBeneficioPagamentoModel
    {
        Guid Id { get; set; }
        string IdiomaId { get; set; }
        Guid IdEmpreendimento { get; set; }
        string BeneficioPagamento { get; set; }
    }
}
