﻿using System;

namespace Laterza.Admin.Data.Models.BeneficioPagamento
{
    public class BeneficioPagamentoModel : IBeneficioPagamentoModel
    {
        public Guid Id { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string BeneficioPagamento { get; set; }
        public string IdiomaId { get; set; }
    }
}
