﻿using System;

namespace Laterza.Admin.Data.Models.Empreendimento
{
    public interface IEmpreendimentoModelUpdateCreate
    {
        Guid Id { get; set; }
        string IdiomaId { get; set; }
        string Nome { get; set; }
        int TipoEmpreendimento { get; set; }
        int StatusEmpreendimento { get; set; }
        string Metragem { get; set; }
        string VagasGaragem { get; set; }
        string Quartos { get; set; }
        bool UnidadeSujeitaDisponibilidade { get; set; }
        string Endereco { get; set; }
        string UrlVideo { get; set; }
        string UrlBanner { get; set; }
        string UrlLogo { get; set; }
        bool Ativo { get; set; }
        bool Lote { get; set; }
        DateTime DataCriacao { get; set; }
        DateTime DataAlteracao { get; set; }
        string UserCriacao { get; set; }
        string UserAlteracao { get; set; }
        int StatusFundacao { get; set; }
        int StatusEstrutura { get; set; }
        int StatusAlvenaria { get; set; }
        int StatusFachada { get; set; }
        int StatusAcabamento { get; set; }
        int StatusPintura { get; set; }
        int StatusEntrega { get; set; }
        int StatusServicosPreliminares { get; set; }
        string Slug { get; set; }
        string Estado { get; set; }
        string Cidade { get; set; }
        string CEP { get; set; }
        bool DestacarHome { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
        string TourVirtual { get; set; }
        bool TourVirtualAtivo { get; set; }
        string Bairro { get; set; }
        bool DestacarStatus { get; set; }
        bool DestacarVitrine { get; set; }
        bool Sazonal { get; set; }
        string ArquivoPlanta { get; set; }
        string ModeloNegocio { get; set; }
        string IdRdstation { get; set; }
        string UrlImagemCampanha { get; set; }
        string Valor { get; set; }
        string SubTitulo { get; set; }
        string QtdUnidades { get; set; }
        string MetragemTerreno { get; set; }
        string MetragemCasa { get; set; }
        string Resumo { get; set; }
        string RegiaoDoBairro { get; set; }
    }
}