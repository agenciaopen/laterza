﻿using Laterza.Admin.Data.Models.FichaTecnica;
using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.Empreendimento
{
    public interface IEmpreendimentoFichaTecnicaModel : IEmpreendimentoModel
    {
        List<FichaTecnicaModel> FichasTecnicas { get; set; }
    }
}
