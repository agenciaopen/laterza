﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Laterza.Admin.Data.Models.Empreendimento
{
    public class EmpreendimentoModel : IEmpreendimentoModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Nome { get; set; }
        public int TipoEmpreendimento { get; set; }
        public int StatusEmpreendimento { get; set; }
        public string Metragem { get; set; }
        public string VagasGaragem { get; set; }
        public string Quartos { get; set; }
        public bool UnidadeSujeitaDisponibilidade { get; set; }
        public string Endereco { get; set; }
        public string UrlVideo { get; set; }
        public string UrlBanner { get; set; }
        public string UrlLogo { get; set; }
        public bool Ativo { get; set; }
        public bool Lote { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
        public string UserCriacao { get; set; }
        public string UserAlteracao { get; set; }
        public int StatusFundacao { get; set; }
        public int StatusEstrutura { get; set; }
        public int StatusAlvenaria { get; set; }
        public int StatusFachada { get; set; }
        public int StatusAcabamento { get; set; }
        public int StatusPintura { get; set; }
        public int StatusEntrega { get; set; }
        public int StatusServicosPreliminares { get; set; }
        public string Slug { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string EstadoNome { get; set; }
        public string CidadeNome { get; set; }
        public string CEP { get; set; }
        public bool DestacarHome { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TourVirtual { get; set; }
        public bool TourVirtualAtivo { get; set; }
        public string Bairro { get; set; }
        public bool DestacarStatus { get; set; }
        public bool DestacarVitrine { get; set; }
        public bool Sazonal { get; set; }
        public string ArquivoPlanta { get; set; }
        public string ModeloNegocio { get; set; }
        public string AnoEntrega { get; set; }
        public string UrlChat { get; set; }
        public string MataTittle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string IdRdstation { get; set; }
        public string UrlImagemCampanha { get; set; }
        public string Valor { get; set; }
        public string SubTitulo { get; set; }
        public string QtdUnidades { get; set; }
        public string MetragemTerreno { get; set; }
        public string MetragemCasa { get; set; }
        public string Resumo { get; set; }
        public string RegiaoDoBairro { get; set; }

        public string getIdUrlVideo()
        {
            var uri = new Uri(UrlVideo);
            var query = HttpUtility.ParseQueryString(uri.Query);
            return string.IsNullOrEmpty(query["v"]) ? "" : query["v"];
        }


    }
}
