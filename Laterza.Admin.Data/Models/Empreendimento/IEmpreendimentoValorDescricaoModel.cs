﻿using System;

namespace Laterza.Admin.Data.Models.Empreendimento
{
    public interface IEmpreendimentoValorDescricaoModel
    {
        Guid Id { get; set; }
        Guid IdEmpreendimento { get; set; }
        string IdiomaId { get; set; }
        string Descricao { get; set; }
        string Valor { get; set; }
        string Titulo { get; set; }
        string Slug { get; set; }


    }
}