﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Laterza.Admin.Data.Models.MdMidia
{
    public class MdMidiaModel : IMdMidiaModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Conteudo { get; set; }
        public DateTime DataPublicacao { get; set; }
        public string UrlImagem { get; set; }
        public string Link { get; set; }

    }
}
