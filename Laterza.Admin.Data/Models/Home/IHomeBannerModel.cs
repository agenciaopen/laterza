﻿using System;

namespace Laterza.Admin.Data.Models.Home
{
    public interface IHomeBannerModel
    {

        Guid Id { get; set; }
        string Link { get; set; }
        string UrlImagem { get; set; }
        string Titulo { get; set; }
        bool Ativo { get; set; }
        DateTime DataCriacao { get; set; }
        DateTime DataAlteracao { get; set; }

    }
}