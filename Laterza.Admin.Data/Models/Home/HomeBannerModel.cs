﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laterza.Admin.Data.Models.Home
{
    public class HomeBannerModel : IHomeBannerModel
    {
        public Guid Id { get; set; }
        public string Link { get; set; }
        public string UrlImagem { get; set; }
        public string Titulo { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
