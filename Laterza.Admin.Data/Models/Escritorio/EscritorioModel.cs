﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Linq;
using Laterza.Admin.Data.Data.Cidades;
using Laterza.Admin.Data.Data.Interfaces.Cidades;
using Laterza.Admin.Data.Models.Cidades;

namespace Laterza.Admin.Data.Models.Escritorio
{
    public class EscritorioModel : IEscritorioModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string HorarioFuncionamento { get; set; }
        public string UrlBanner{ get; set; }
        public string EstadoNome { get; set; }
        public string CidadeNome { get; set; }
    }
}
