﻿using System.ComponentModel;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumModeloNegocio
    {
        [Description("Incorporação")]
        Incorporacao = 1,

        [Description("Administração")]
        Administracao = 2
    }
}
