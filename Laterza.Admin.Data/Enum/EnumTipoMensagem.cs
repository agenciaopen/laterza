﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumTipoMensagem
    {
        [Description("Fale conosco")]
        FaleConosco = 1,

        [Description("Trabalhe conosco")]
        TrabalheConosco = 2,

        [Description("Newsletter")]
        Newsletter = 3,

        [Description("Seja um fornecedor")]
        SejaUmFornecedor = 4,

        [Description("Venda seu terreno")]
        VendaSeuTerreno = 4

    }
}
