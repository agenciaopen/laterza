﻿using System.ComponentModel;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumInformacoesTecnicas
    {
        [Description("Arquitetura")]
        Arquitetura = 1,

        [Description("Estrutura")]
        Estrutura = 2,

        [Description("Decoração")]
        Decoracao = 3,

        [Description("Fundação")]
        Fundacao = 4,

        [Description("Instalação")]
        Instalacao = 5,

        [Description("Paisagismo")]
        Paisagismo = 6,

        [Description("Ambientação")]
        Ambientacao = 7,
    }
}
