﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumFaqArea
    {
        [Description("Imóvel")]
        Imovel = 1,

        [Description("Cliente")]
        Cliente = 2
    }
}
