﻿using System.ComponentModel;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumStatusEmpreendimento
    {
        [Description("Em Construção")]
        EmConstrucao = 1,

        [Description("Entregue")]
        Entregue = 2,

        [Description("Lançamento")]
        Lancamento = 4
    }
}


