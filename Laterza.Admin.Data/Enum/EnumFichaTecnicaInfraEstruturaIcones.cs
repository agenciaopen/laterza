﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumFichaTecnicaInfraEstruturaIcones
    {
        [Description("Área")]
        Area = 1,
        [Description("Andar Corporativo")]
        Andar_corporativo = 2,
        [Description("Área de Serviço")]
        Area_de_Servico = 4,
        [Description("Área verde")]
        Area_verde = 5,
        [Description("Auditório")]
        Auditorio = 6,
        [Description("Automação")]
        Automacao = 7,
        [Description("Banheiro de Serviço")]
        Banheiro_de_Servico = 8,
        [Description("Banheiros")]
        Banheiros = 9,
        [Description("Central de Gás")]
        Central_de_Gas = 13,
        [Description("Controle de Acesso")]
        Controle_de_Acesso = 15,
        [Description("Coworking")]
        Coworking = 16,
        [Description("Cozinha")]
        Cozinha = 17,
        [Description("Elevador Social")]
        Elevador_Social = 18,
        [Description("Elevador Inteligente")]
        Elevador_Inteligente = 19,
        [Description("Elevador Serviço")]
        Elevador_Servico = 20,
        [Description("Escada")]
        Escada = 21,
        [Description("Estac Rotativo")]
        Estac_Rotativo = 23,
        [Description("Fechadura Eletronica")]
        Fechadura_Eletronica = 24,
        [Description("Foyer")]
        Foyer = 27,
        [Description("Garagem")]
        Garagem = 28,
        [Description("Guarda Volumes")]
        Guarda_Volumes = 30,
        [Description("Guarita")]
        Guarita = 31,
        [Description("Lavabo")]
        Lavabo = 37,
        [Description("Infraestrutura")]
        Infraestrutura = 35,
        [Description("Local para erador")]
        Local_para_gerador = 38,
        [Description("Local para lixo")]
        Local_para_lixo = 39,
        [Description("Local para Lojas")]
        Local_para_Lojas = 40,
        [Description("Local para Split")]
        Local_para_Split = 41,
        [Description("Painel Solar")]
        Painel_Solar = 44,
        [Description("Pet Place")]
        Pet_Place = 45,
        [Description("Pet Wash")]
        Pet_Wash = 46,
        [Description("Piso Elevado")]
        Piso_Elevado = 51,
        [Description("Quartos")]
        Quartos = 57,
        [Description("Suite")]
        Suite = 65,
        [Description("Terraco")]
        Terraco = 66,
        [Description("Terraco coberto")]
        Terraco_coberto = 67,
        [Description("Teto Verde")]
        Teto_Verde = 68,
        [Description("Tomada Carro Eletrico")]
        Tomada_Carro_eletrico = 69,
        [Description("Varanda")]
        Varanda = 70,
        [Description("Varanda Gourmet")]
        Varanda_Gourmet = 71,
        [Description("Vestiário")]
        Vestiario = 72,
        [Description("Reconhecimento facial")]
        Reconhecimento_facial = 58,
        [Description("Salas de Reunião")]
        Salas_de_Reuniao = 63,
        [Description("Hall")]
        Hall = 73,
        [Description("Lavanderia")]
        Lavanderia = 74,
        [Description("Bicicletário")]
        Bicicletario = 10,
        [Description("Elevador Panorâmico")]
        Elevador_Panoramico = 75,
        [Description("Point Carro de App")]
        Point_Carro_De_App = 76,
        [Description("Depósito")]
        Deposito = 77,
        [Description("Heliponto")]
        Heliponto = 78,
        [Description("WC")]
        WC = 79,
        [Description("Unidade de preparação para automação")]
        Unidade_De_Preparacao_Para_Automacao = 80,
        [Description("Escada de Serviço")]
        Escada_De_Servico = 81,

    }
}
