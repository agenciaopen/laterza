﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumFaq
    {
        [Description("Imovel - Sobre pagamentos e financiamentos")]
        ImovelSobrePagamentosFinanciamentos = 1,

        [Description("Imovel - Sobre o nosso modelo de negócio")]
        ImovelSobreNossoModeloDeNegocio = 2,

        [Description("Imovel - Sobre impostos e taxas")]
        ImovelSobreImpostosTaxas = 3,

        [Description("Cliente - Acompanhamento da obra")]
        ClienteAcompanhamentoDaObra = 4,

        [Description("Cliente - Personalização ")]
        ClientePersonalização = 5,

        [Description("Cliente - Tributos, impostos e Habite-se")]
        ClienteTributosImpostosHabiteSe = 6
    }
}

