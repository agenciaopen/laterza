﻿using System.ComponentModel;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumTipoEmpreendimento
    {
        [Description("Empresarial")]
        Empresarial = 1,

        [Description("Residencial Condomínio")]
        ResidencialCondominio = 2,

        [Description("Residencial Incorporação")]
        ResidencialIncorporacao = 3,

        [Description("Beach Class")]
        BeachClass = 4
    }
}
