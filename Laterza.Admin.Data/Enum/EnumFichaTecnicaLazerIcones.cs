﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumFichaTecnicaLazerIcones
    {
        [Description("Área de Lazer")]
        Area_de_Lazer = 3,
        [Description("Bicicletário")]
        Bicicletario = 10,
        [Description("Brinquedoteca")]
        Brinquedoteca = 11,
        [Description("Brinquedoteca interativa")]
        Brinquedoteca_interativa = 12,
        [Description("Churrasqueira")]
        Churrasqueira = 14,
        [Description("Espaco Gourmet")]
        Espaco_Gourmet = 22,
        [Description("Fitness")]
        Fitness = 25,
        [Description("Fitness Externo")]
        Fitness_Externo = 26,
        [Description("Gazebo")]
        Gazebo = 29,
        [Description("Hidromassagem")]
        Hidromassagem = 33,
        [Description("Horta")]
        Horta = 34,
        [Description("Jacuzzi")]
        Jacuzzi = 36,
        [Description("Mini Campo")]
        Mini_Campo = 42,
        [Description("Piscina")]
        Piscina = 47,
        [Description("Piscina Aquecida")]
        Piscina_Aquecida = 48,
        [Description("Piscina coberta e aquecida")]
        Piscina_coberta_e_aquecida = 49,
        [Description("Piscina infantil")]
        Piscina_infantil = 50,
        [Description("Pista de Cooper")]
        Pista_de_Cooper = 52,
        [Description("Playground")]
        Playground = 53,
        [Description("Pomar")]
        Pomar = 54,
        [Description("Praça")]
        Praca = 55,
        [Description("Quadra de Tenis")]
        Quadra_de_Tenis = 56,
        [Description("Sala de Jantar")]
        Sala_de_Jantar = 60,
        [Description("Sala de Jogos")]
        Sala_de_Jogos = 61,
        [Description("Salão de Festas")]
        Salao_de_Festas = 62,
        [Description("Quadra de Squash")]
        Quadra_De_Squash = 82,
        [Description("Piscina Borda Infinita")]
        Piscina_Borda_Infinita = 83,
        [Description("Terraço")]
        Terraco = 84,
        [Description("Coworking")]
        Coworking = 85,
        [Description("Terraço Coberto")]
        Terraco_coberto = 86,
        [Description("Sala de Leitura")]
        Sala_De_Leitura = 87,
        [Description("Sauna")]
        Sauna = 88,
        [Description("Pet Place")]
        Pet_Place = 89,
        [Description("Sala de Ginástica")]
        Sala_De_Ginastica = 90,
        [Description("Prainha")]
        Prainha = 91,
        [Description("Área Verde")]
        Area_verde = 92,
        [Description("Bar")]
        Bar = 93,
        [Description("Quadra")]
        Quadra = 94,
        [Description("Pet Wash")]
        Pet_Wash = 95,
        [Description("Salão de Festas Infantil")]
        Salao_De_Festas_Infantil = 96,
        [Description("Chuveirão")]
        Chuveirao = 97,
        [Description("Ofurô")]
        Ofuro = 98,
        [Description("SPA")]
        SPA = 99,
        [Description("Deck")]
        Deck = 100,
        [Description("Sala de TV")]
        Sala_de_TV = 101,
        [Description("Home Cinema")]
        Home_Cinema = 102,
        [Description("Espaço Relax")]
        Espaco_Relax = 103,

    }
}
