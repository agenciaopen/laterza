﻿using System.ComponentModel;

namespace Laterza.Admin.Data.Enum
{
    public enum EnumTipoGaleria
    {
        [Description("Imóvel")]
        Imovel = 1,

        [Description("Galeria de Plantas")]
        Planta = 2,

        [Description("Galeria de Status da Obra")]
        StatusDaObra = 3
    }
}
