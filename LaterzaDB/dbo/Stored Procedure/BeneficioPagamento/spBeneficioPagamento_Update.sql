﻿CREATE PROCEDURE [dbo].[spBeneficioPagamento_Update]
	@Id UNIQUEIDENTIFIER = NULL,
    @IdiomaId NVARCHAR(10) = NULL,
    @IdEmpreendimento UNIQUEIDENTIFIER = NULL, 
    @BeneficioPagamento NVARCHAR(200) = NULL
AS
BEGIN
    SET NOCOUNT ON;
	BEGIN TRANSACTION
        UPDATE dbo.BeneficioPagamento SET Id = @Id, IdEmpreendimento = @IdEmpreendimento, BeneficioPagamento = @BeneficioPagamento
        WHERE Id = @Id AND IdEmpreendimento = @IdEmpreendimento
    COMMIT TRANSACTION
END
