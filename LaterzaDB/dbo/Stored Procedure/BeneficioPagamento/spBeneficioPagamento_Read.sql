﻿CREATE PROCEDURE [dbo].[spBeneficioPagamento_Read]
	@Id UNIQUEIDENTIFIER = NULL,
	@IdEmpreendimento UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SELECT [Id], [IdiomaId], [IdEmpreendimento], [BeneficioPagamento]
	FROM BeneficioPagamento BP
	WHERE (BP.Id = @Id OR @Id = CONVERT(UNIQUEIDENTIFIER, 0x00) AND (IdEmpreendimento = @IdEmpreendimento OR @IdEmpreendimento = CONVERT(UNIQUEIDENTIFIER, 0x00)))
END
