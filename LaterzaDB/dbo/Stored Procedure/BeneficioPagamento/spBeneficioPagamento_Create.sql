﻿CREATE PROCEDURE [dbo].[spBeneficioPagamento_Create]
	@Id UNIQUEIDENTIFIER = NULL,
    @IdiomaId NVARCHAR(10) = NULL,
    @IdEmpreendimento UNIQUEIDENTIFIER = NULL, 
    @BeneficioPagamento NVARCHAR(200) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.BeneficioPagamento(Id, IdiomaId, IdEmpreendimento, BeneficioPagamento) 
			VALUES(@Id, @oneid, @IdEmpreendimento, @BeneficioPagamento);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END