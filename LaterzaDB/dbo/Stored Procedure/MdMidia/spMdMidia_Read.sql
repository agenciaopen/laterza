﻿CREATE PROCEDURE [dbo].[spMdMidia_Read]
	@Id uniqueidentifier = null
AS
BEGIN
    SELECT m.[Id], m.[IdiomaId], m.[Titulo], m.[Descricao], 
            m.[Conteudo], m.[DataPublicacao], m.[UrlImagem], m.[Link]
        FROM MdMidia m
        WHERE (m.Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
        ORDER BY m.[DataPublicacao];
END