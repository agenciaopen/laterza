﻿CREATE PROCEDURE [dbo].[spPoliticaDePrivacidade_Update]
	@Id UNIQUEIDENTIFIER = NULL,
	@Conteudo NVARCHAR(MAX) = NULL,
	@DataCriacao DATETIME2(7) = NULL, 
	@DataAlteracao DATETIME2(7) = NULL
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		UPDATE dbo.PoliticaDePrivacidade
		SET Conteudo = @Conteudo, DataCriacao = @DataCriacao, DataAlteracao = @DataAlteracao
		WHERE Id = @Id
	COMMIT TRANSACTION
END
