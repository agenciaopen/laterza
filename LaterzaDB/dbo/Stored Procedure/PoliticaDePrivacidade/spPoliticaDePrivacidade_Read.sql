﻿CREATE PROCEDURE [dbo].[spPoliticaDePrivacidade_Read]
	@Id UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SELECT [Id], [Conteudo], [DataCriacao], [DataAlteracao] 
	FROM dbo.PoliticaDePrivacidade
	WHERE (Id = @Id OR @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
END
