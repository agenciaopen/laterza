﻿CREATE PROCEDURE [dbo].[spPoliticaDePrivacidade_Create]
	@Id UNIQUEIDENTIFIER = NULL,
	@Conteudo NVARCHAR(MAX) = NULL,
	@DataCriacao DATETIME2(7) = NULL, 
	@DataAlteracao DATETIME2(7) = NULL
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		INSERT INTO dbo.PoliticaDePrivacidade(Id, Conteudo, DataCriacao, DataAlteracao)
		VALUES(@Id, @Conteudo, @DataCriacao, @DataAlteracao)
	COMMIT TRANSACTION
END
