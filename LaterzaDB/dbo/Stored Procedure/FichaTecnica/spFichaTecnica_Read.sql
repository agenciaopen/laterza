﻿CREATE PROCEDURE [dbo].[spFichaTecnica_Read]
	@Id UNIQUEIDENTIFIER = NULL,
	@IdEmpreendimento UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SELECT [Id], [IdiomaId], [IdEmpreendimento], [Titulo], [Destacar]
	FROM FichaTecnica FT
	WHERE (FT.Id = @Id OR @Id = CONVERT(UNIQUEIDENTIFIER, 0x00) AND (IdEmpreendimento = @IdEmpreendimento OR @IdEmpreendimento = CONVERT(UNIQUEIDENTIFIER, 0x00)))
END