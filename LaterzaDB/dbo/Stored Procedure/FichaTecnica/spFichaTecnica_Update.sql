﻿CREATE PROCEDURE [dbo].[spFichaTecnica_Update]
	@Id UNIQUEIDENTIFIER = NULL,
    @IdiomaId NVARCHAR(10) = NULL,
    @IdEmpreendimento UNIQUEIDENTIFIER = NULL, 
    @Titulo NVARCHAR(200) = NULL,
	@Destacar BIT = NULL
AS
BEGIN
    SET NOCOUNT ON;
	BEGIN TRANSACTION
        UPDATE dbo.FichaTecnica SET Id = @Id, IdEmpreendimento = @IdEmpreendimento, Titulo = @Titulo, Destacar = @Destacar
        WHERE Id = @Id AND IdEmpreendimento = @IdEmpreendimento
    COMMIT TRANSACTION
END
