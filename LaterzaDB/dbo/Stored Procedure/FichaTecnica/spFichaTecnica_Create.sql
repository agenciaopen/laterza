﻿CREATE PROCEDURE [dbo].[spFichaTecnica_Create]
	@Id UNIQUEIDENTIFIER = NULL,
    @IdiomaId NVARCHAR(10) = NULL,
    @IdEmpreendimento UNIQUEIDENTIFIER = NULL, 
    @Titulo NVARCHAR(200) = NULL,
	@Destacar BIT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.FichaTecnica(Id, IdiomaId, IdEmpreendimento, Titulo, Destacar) 
			VALUES(@Id, @oneid, @IdEmpreendimento, @Titulo, @Destacar);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END