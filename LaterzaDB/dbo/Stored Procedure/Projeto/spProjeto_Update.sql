﻿CREATE PROCEDURE [dbo].[spProjeto_Update]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Nome nvarchar(50) =  null,
	@Descricao nvarchar(max) =  null,
	@Endereco nvarchar(max) =  null,
	@Estado nvarchar(200) =  null,
	@Cidade nvarchar(200) =  null,
	@UrlImagem nvarchar(200) =  null,
	@ProjetoSocial bit =  null,
	@EstadoNome nvarchar(200),
	@CidadeNome nvarchar(200)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

		UPDATE dbo.Projeto SET IdiomaId = @IdiomaId, Nome = @Nome, Descricao = @Descricao, 
				Endereco = @Endereco, Estado = @Estado, Cidade = @Cidade, UrlImagem = @UrlImagem, ProjetoSocial = @ProjetoSocial
			WHERE Id = @Id AND IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END