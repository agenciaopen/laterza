﻿CREATE PROCEDURE [dbo].[spProjeto_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.ProjetoGaleria WHERE IdProjeto = @Id;

		DELETE FROM dbo.Projeto WHERE Id = @Id;
	COMMIT TRANSACTION
END