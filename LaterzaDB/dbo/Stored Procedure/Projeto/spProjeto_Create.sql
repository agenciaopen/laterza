﻿CREATE PROCEDURE [dbo].[spProjeto_Create]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Nome nvarchar(50) =  null,
	@Descricao nvarchar(max) =  null,
	@Endereco nvarchar(max) =  null,
	@Estado nvarchar(200) =  null,
	@Cidade nvarchar(200) =  null,
	@UrlImagem nvarchar(200) =  null,
	@ProjetoSocial bit =  null,
	@EstadoNome nvarchar(200),
	@CidadeNome nvarchar(200)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.Projeto(Id, IdiomaId, Nome, Descricao, Endereco, Estado, Cidade, UrlImagem, ProjetoSocial) 
			                       VALUES(@Id, @oneId, @Nome, @Descricao, @Endereco, @Estado, @Cidade, @UrlImagem, @ProjetoSocial);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END