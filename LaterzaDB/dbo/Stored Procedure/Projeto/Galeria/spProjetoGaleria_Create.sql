﻿CREATE PROCEDURE [dbo].[spProjetoGaleria_Create]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@IdProjeto uniqueidentifier = null,
	@UrlImagem nvarchar(MAX) = null,
	@UrlVideo nvarchar(MAX) = null,
	@Titulo nvarchar(MAX) = null,
	@EhVideo bit = null,
	@Ordem int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.ProjetoGaleria(Id, IdiomaId, UrlImagem, UrlVideo, Titulo, EhVideo, IdProjeto, Ordem) VALUES(@Id, @oneid, @UrlImagem, @UrlVideo, @Titulo, @EhVideo, @IdProjeto, @Ordem);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END