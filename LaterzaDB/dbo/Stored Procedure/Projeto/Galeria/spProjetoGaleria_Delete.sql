﻿CREATE PROCEDURE [dbo].[spProjetoGaleria_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.ProjetoGaleria WHERE Id = @Id;
	COMMIT TRANSACTION
END