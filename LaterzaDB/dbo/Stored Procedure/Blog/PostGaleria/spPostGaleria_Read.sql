﻿CREATE PROCEDURE [dbo].[spPostGaleria_Read]
	@Id UNIQUEIDENTIFIER = NULL,
	@IdPost UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SELECT [Id], [IdPost], [UrlImagem], [UrlVideo], [Descricao], [Titulo], [EhVideo], [Ordem]
	FROM PostGaleria PG
	WHERE (PG.Id = @Id OR @Id = CONVERT(UNIQUEIDENTIFIER, 0x00) AND IdPost = @IdPost)
	ORDER BY [Ordem];
END
