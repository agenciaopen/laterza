﻿CREATE PROCEDURE [dbo].[spPostGaleriaOrdem_Update]
	@Id uniqueidentifier = null,
	@Ordem int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION

		UPDATE dbo.PostGaleria set Ordem = @Ordem
		where Id = @Id;

	COMMIT TRANSACTION
END
