﻿CREATE PROCEDURE [dbo].[spPostGaleria_Create]
	@Id UNIQUEIDENTIFIER = NULL,
    @IdPost UNIQUEIDENTIFIER = NULL, 
	@UrlImagem NVARCHAR(MAX) = NULL, 
    @UrlVideo NVARCHAR(MAX) = NULL, 
    @Descricao NVARCHAR(200) = NULL,
    @Titulo NVARCHAR(50) = NULL, 
    @EhVideo BIT = NULL, 
    @Ordem INT = NULL
AS
BEGIN
    SET NOCOUNT ON
	BEGIN TRANSACTION
        INSERT INTO dbo.PostGaleria(Id, IdPost, UrlImagem, UrlVideo, Descricao, Titulo, EhVideo, Ordem)
        VALUES(@Id, @IdPost, @UrlImagem, @UrlVideo, @Descricao, @Titulo, @EhVideo, @Ordem)
    COMMIT TRANSACTION
END
