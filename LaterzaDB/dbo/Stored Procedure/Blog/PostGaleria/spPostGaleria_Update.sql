﻿CREATE PROCEDURE [dbo].[spPostGaleria_Update]
	@Id UNIQUEIDENTIFIER = NULL,
    @IdPost UNIQUEIDENTIFIER = NULL, 
	@UrlImagem NVARCHAR(MAX) = NULL, 
    @UrlVideo NVARCHAR(MAX) = NULL, 
    @Descricao NVARCHAR(200) = NULL,
    @Titulo NVARCHAR(50) = NULL, 
    @EhVideo BIT = NULL, 
    @Ordem INT = NULL
AS
BEGIN
    SET NOCOUNT ON
	BEGIN TRANSACTION
        UPDATE dbo.PostGaleria SET Id = @Id, IdPost = @IdPost, UrlImagem = @UrlImagem, 
        UrlVideo = @UrlVideo, Descricao = @Descricao, Titulo = @Titulo, EhVideo = @EhVideo, Ordem = @Ordem
        WHERE Id = @Id AND IdPost = @IdPost
    COMMIT TRANSACTION
END
