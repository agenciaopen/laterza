﻿CREATE PROCEDURE [dbo].[spPost_Create]
	@Id UNIQUEIDENTIFIER = NULL,
	@Titulo NVARCHAR(50) = NULL,
	@Descricao NVARCHAR(200) = NULL,
	@UrlImagemDestaque NVARCHAR(MAX) = NULL,
	@Destacar BIT = NULL,
	@Categoria NVARCHAR(200) = NULL,
	@Conteudo NVARCHAR(MAX) = NULL,
	@Slug NVARCHAR(200) = NULL,
	@DataCriacao DATETIME2(7) = NULL, 
	@DataAlteracao DATETIME2(7) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		INSERT INTO dbo.Post(Id, Titulo, Descricao, UrlImagemDestaque, Destacar, Categoria, Conteudo, Slug, DataCriacao, DataAlteracao) 
			VALUES(@Id, @Titulo, @Descricao, @UrlImagemDestaque, @Destacar, @Categoria, @Conteudo, @Slug, @DataCriacao, @DataAlteracao);
	COMMIT TRANSACTION
END
