﻿CREATE PROCEDURE [dbo].[spPost_Read]
	@Id UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SELECT P.[Id], P.[Titulo], P.[Descricao], P.[UrlImagemDestaque], P.[Destacar], P.[Categoria], P.[Conteudo], P.[Slug], P.[DataCriacao], P.[DataAlteracao]
	FROM Post P
	WHERE (P.Id = @Id OR @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
END

