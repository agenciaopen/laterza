﻿CREATE PROCEDURE [dbo].[spPost_Update]
	@Id UNIQUEIDENTIFIER = NULL,
	@Titulo NVARCHAR(50) = NULL,
	@Descricao NVARCHAR(200) = NULL,
	@UrlImagemDestaque NVARCHAR(MAX) = NULL,
	@Destacar BIT = NULL,
	@Categoria NVARCHAR(200) = NULL,
	@Conteudo NVARCHAR(MAX) = NULL,
	@Slug NVARCHAR(200) = NULL,
	@DataCriacao DATETIME2(7) = NULL,
	@DataAlteracao DATETIME2(7) = NULL
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION
		UPDATE dbo.Post SET
		Titulo = @Titulo, Descricao = @Descricao, UrlImagemDestaque = @UrlImagemDestaque, 
		Destacar = @Destacar, Categoria = @Categoria, Conteudo = @Conteudo, Slug = @Slug,
		DataCriacao = @DataCriacao, DataAlteracao = @DataAlteracao
		WHERE Id = @Id
	COMMIT TRANSACTION
END