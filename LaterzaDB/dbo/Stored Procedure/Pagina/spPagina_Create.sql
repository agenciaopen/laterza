﻿CREATE PROCEDURE [dbo].[spPagina_Create]
	@Slug NVARCHAR(200) = null,
    @IdiomaId NVARCHAR(10) = null,
    @Titulo NVARCHAR(200)  = null,
    @SubTitulo NVARCHAR(200)  = null,
    @Banner NVARCHAR(200)  = null,
    @Link NVARCHAR(MAX) = null,
    @Imagem NVARCHAR(MAX) = null,
    @Descricao NVARCHAR(MAX) = null,
    @MataTittle NVARCHAR(200) = null,
    @MetaDescription NVARCHAR(MAX)  = null,
    @MetaKeyword NVARCHAR(200) = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.Pagina(Slug, IdiomaId,Titulo, Descricao, MataTittle, MetaDescription, MetaKeyword, Banner, Imagem) 
			                VALUES(@Slug, @oneId,@Titulo, @Descricao, @MataTittle, @MetaDescription, @MetaKeyword, @Banner, @Imagem);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END