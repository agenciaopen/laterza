﻿CREATE PROCEDURE [dbo].[spPagina_Read]
	@Slug NVARCHAR(200) = null
AS
BEGIN
    SELECT [Slug], [IdiomaId], [Titulo], [SubTitulo], [Banner], [Link], [Imagem], [Descricao], [MataTittle], [MetaDescription], [MetaKeyword]
        FROM Pagina
        WHERE upper(Slug) like upper(@Slug)
        ORDER BY Slug;
END