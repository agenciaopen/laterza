﻿CREATE PROCEDURE [dbo].[spMensagem_Create]
	@Id UNIQUEIDENTIFIER = NULL, 
	@Nome  NVARCHAR(MAX) = NULL,  
	@Email  NVARCHAR(80) =NULL,  
	@Telefone  NVARCHAR(80)= NULL,  
	@Mensagem  NVARCHAR(MAX) =NULL,  
    @TipoMensagem INT= NULL, 
	@DataCriacao DATETIME2 =NULL
AS
BEGIN

	SET NOCOUNT ON;
	BEGIN TRANSACTION
		
		INSERT INTO dbo.Mensagem(Id, Nome, Email, Telefone, Mensagem, TipoMensagem, DataCriacao)
					VALUES(@Id, @Nome, @Email, @Telefone, @Mensagem, @TipoMensagem, @DataCriacao)

	COMMIT TRANSACTION
END