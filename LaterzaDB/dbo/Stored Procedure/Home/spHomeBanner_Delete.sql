﻿CREATE PROCEDURE [dbo].[spHomeBanner_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.HomeBanner WHERE Id = @Id;
	COMMIT TRANSACTION
END