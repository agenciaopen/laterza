﻿CREATE PROCEDURE [dbo].[spHomeBanner_Update]
	@Id UNIQUEIDENTIFIER = NULL,
	@Link NVARCHAR(50) = NULL,
	@UrlImagem NVARCHAR(MAX) = NULL,
	@Titulo NVARCHAR(50) = NULL,
	@Ativo BIT = NULL,
	@DataCriacao DATETIME2(7) = NULL,
	@DataAlteracao DATETIME2(7) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		UPDATE dbo.HomeBanner set Titulo = @Titulo, UrlImagem = @UrlImagem, Link = @Link, Ativo = @Ativo,
		DataCriacao = @DataAlteracao, DataAlteracao = @DataAlteracao
		where Id = @Id;
	COMMIT TRANSACTION
END