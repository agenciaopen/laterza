﻿CREATE PROCEDURE [dbo].[spHomeBanner_Create]
	@Id UNIQUEIDENTIFIER = NULL,
	@Link NVARCHAR(50) = NULL,
	@UrlImagem NVARCHAR(MAX) = NULL,
	@Titulo NVARCHAR(50) = NULL,
	@Ativo BIT = NULL,
	@DataCriacao DATETIME2(7) = NULL,
	@DataAlteracao DATETIME2(7) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		INSERT INTO dbo.HomeBanner(Id, Titulo, UrlImagem, Link, Ativo, DataCriacao, DataAlteracao) 
		VALUES(@Id, @Titulo, @UrlImagem, @Link, @Ativo, @DataCriacao, @DataAlteracao);
	COMMIT TRANSACTION
END