﻿CREATE PROCEDURE [dbo].[spHomeBanner_Read]
	@Id uniqueidentifier = null
AS
BEGIN
	SELECT h.Id, h.Titulo, h.UrlImagem, h.Link, h.Ativo, h.DataCriacao, h.DataAlteracao
	  FROM dbo.HomeBanner h
	  WHERE (h.Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00));
END