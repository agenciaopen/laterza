﻿CREATE PROCEDURE [dbo].[spHomeBanner_ReadOne]
	@Id int = null
AS
BEGIN
SELECT h.Id, h.LinkImagem, hd.IdiomaId, hd.Titulo, hd.Descricao, hd.Link
  FROM dbo.HomeBanner h
  inner join dbo.HomeBanner_Detalhes hd on hd.Id = h.Id
  where h.Id = @Id and hd.IdiomaId = 'pt';
END