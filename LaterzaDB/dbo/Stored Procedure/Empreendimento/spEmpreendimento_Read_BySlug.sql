﻿CREATE PROCEDURE [dbo].[spEmpreendimento_Read_BySlug]
	@Slug NVARCHAR(200) = null
AS
BEGIN
    SELECT  e.[Id], e.[IdiomaId], e.[Nome], e.[TipoEmpreendimento], 
            e.[StatusEmpreendimento], e.[Metragem], e.[VagasGaragem], 
            e.[Quartos], e.[UnidadeSujeitaDisponibilidade], e.[Endereco], 
            e.[UrlVideo], e.[UrlBanner], e.[UrlLogo], e.[Ativo], e.[Lote], 
            e.[DataCriacao], e.[DataAlteracao], e.[UserCriacao], e.[UserAlteracao], 
            e.[StatusFundacao], e.[StatusEstrutura], e.[StatusAlvenaria], 
            e.[StatusFachada], e.[StatusAcabamento], e.[StatusPintura], 
            e.[StatusEntrega], e.[StatusServicosPreliminares], e.[Slug], e.[AnoEntrega],
            e.[DestacarHome], e.[Latitude], e.[Longitude], e.[TourVirtualAtivo], e.[TourVirtual],
            e.[Bairro], e.[DestacarStatus], e.[DestacarVitrine], e.[Sazonal], e.[ArquivoPlanta], e.[ModeloNegocio], 
            e.[Estado], e.[Cidade], e.[CEP], c.Nome as 'CidadeNome', uf.UF as 'EstadoNome', e.[UrlChat], e.[MataTittle], e.[MetaDescription], e.[MetaKeyword],
            e.[IdRdstation], e.[UrlImagemCampanha], e.[SubTitulo], e.[QtdUnidades], e.[MetragemTerreno], e.[MetragemCasa], e.[Resumo], e.[RegiaoDoBairro],
            (
            select Valor from (
            select w.[Valor], ROW_NUMBER() OVER(ORDER BY w.IdEmpreendimento ASC) as rownum
            from EmpreendimentoValorDescricao w 
            where w.IdEmpreendimento = e.Id and w.IdiomaId = e.IdiomaId
            ) a where a.rownum = 1
            ) as Valor
            FROM Empreendimento e
      LEFT JOIN Estados uf on uf.Slug = e.Estado
      LEFT JOIN Cidades c on c.Slug = e.Cidade and c.UF = uf.UF
     WHERE e.Slug = @Slug
     ORDER BY Valor DESC;
END
