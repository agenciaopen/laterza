﻿CREATE PROCEDURE [dbo].[spEmpreendimento_Read_Distinct_Quartos]
    @Id uniqueidentifier = null
AS
BEGIN
    SELECT DISTINCT e.[Quartos]
      FROM Empreendimento e
      where trim(e.[Quartos]) not like '' and trim(e.[Quartos]) not like '-'
END