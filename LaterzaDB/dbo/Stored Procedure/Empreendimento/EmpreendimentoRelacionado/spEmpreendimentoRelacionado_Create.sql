﻿CREATE PROCEDURE [dbo].[spEmpreendimentoRelacionado_Create]
    @IdEmpreendimento UNIQUEIDENTIFIER = NULL,
    @IdEmpreendimentoRelacionado UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
			INSERT INTO dbo.EmpreendimentoRelacionado(IdEmpreendimento, IdEmpreendimentoRelacionado) 
			     VALUES(@IdEmpreendimento, @IdEmpreendimentoRelacionado);
	COMMIT TRANSACTION
END