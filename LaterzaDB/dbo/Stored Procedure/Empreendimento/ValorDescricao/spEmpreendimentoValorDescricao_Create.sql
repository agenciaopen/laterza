﻿CREATE PROCEDURE [dbo].[spEmpreendimentoValorDescricao_Create]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@Descricao nvarchar(MAX) = null,
	@Valor nvarchar(50) = NULL,
	@Titulo nvarchar(200) = null,
	@Slug nvarchar(200) = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.EmpreendimentoValorDescricao(Id, IdEmpreendimento, IdiomaId, Descricao, Valor, Titulo, Slug) VALUES(@Id, @IdEmpreendimento, @oneId, @Descricao, @Valor, @Titulo, @Slug);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END
