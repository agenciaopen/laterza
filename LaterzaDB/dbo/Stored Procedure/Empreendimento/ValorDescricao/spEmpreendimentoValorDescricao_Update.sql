﻿CREATE PROCEDURE [dbo].[spEmpreendimentoValorDescricao_Update]
	@Id uniqueidentifier = NULL,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@Descricao nvarchar(MAX) = NULL,
	@Valor nvarchar(50) = NULL,
	@Titulo nvarchar(200) = NULL,
	@Slug nvarchar(200) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

		UPDATE dbo.EmpreendimentoValorDescricao set Descricao = @Descricao, Valor = @Valor, Titulo = @Titulo, Slug = @Slug
		where Id = @Id and IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END