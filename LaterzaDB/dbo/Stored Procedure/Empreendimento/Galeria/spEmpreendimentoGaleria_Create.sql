﻿CREATE PROCEDURE [dbo].[spEmpreendimentoGaleria_Create]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@UrlImagem nvarchar(MAX) = null,
	@UrlVideo nvarchar(MAX) = null,
	@Descricao nvarchar(MAX) = null,
	@TipoGaleria int = null,
	@Titulo nvarchar(MAX) = null,
	@EhVideo bit = null,
	@Ordem int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.EmpreendimentoGaleria(Id, IdiomaId, UrlImagem, UrlVideo, Descricao, TipoGaleria, Titulo, EhVideo, IdEmpreendimento, Ordem) VALUES(@Id, @oneid, @UrlImagem, @UrlVideo, @Descricao, @TipoGaleria, @Titulo, @EhVideo, @IdEmpreendimento, @Ordem);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END