﻿CREATE PROCEDURE [dbo].[spEmpreendimentoGaleriaOrdem_Update]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@UrlImagem nvarchar(MAX) = null,
	@UrlVideo nvarchar(MAX) = null,
	@Descricao nvarchar(MAX) = null,
	@TipoGaleria int = null,
	@Titulo nvarchar(MAX) = null,
	@EhVideo bit= null,
	@Ordem int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION

		UPDATE dbo.EmpreendimentoGaleria set Ordem = @Ordem
		where Id = @Id;

	COMMIT TRANSACTION
END