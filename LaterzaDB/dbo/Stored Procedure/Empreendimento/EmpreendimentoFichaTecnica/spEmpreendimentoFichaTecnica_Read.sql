﻿CREATE PROCEDURE [dbo].[spEmpreendimentoFichaTecnica_Read]

AS
BEGIN
    SELECT * FROM Empreendimento E
    LEFT JOIN FichaTecnica F ON E.Id = F.IdEmpreendimento AND F.IdiomaId = 'pt' AND F.Destacar = 1
    WHERE E.IdiomaId = 'pt'
END
