﻿CREATE PROCEDURE [dbo].[spEmpreendimento_Read_Distinct_Bairro]
    @Id uniqueidentifier = null
AS
BEGIN
    SELECT DISTINCT e.[Bairro], e.[Estado], e.[Cidade]
      FROM Empreendimento e
      where e.[Bairro] not like ''
END