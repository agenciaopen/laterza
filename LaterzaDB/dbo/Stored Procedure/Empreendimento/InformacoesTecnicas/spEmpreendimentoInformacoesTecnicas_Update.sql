﻿CREATE PROCEDURE [dbo].[spEmpreendimentoInformacoesTecnicas_Update]
	@Id uniqueidentifier = NULL,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = NULL,
	@Nome nvarchar(MAX) = null,
	@Funcao nvarchar(MAX) = null
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

		UPDATE dbo.EmpreendimentoInformacoesTecnicas set Nome = @Nome, Funcao = @Funcao
		where Id = @Id and IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END