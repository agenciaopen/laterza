﻿CREATE PROCEDURE [dbo].[spCidades_Read]
	@Slug varchar(300) = '',
	@SlugEstado varchar(300) = ''
AS

BEGIN
	SELECT [c].[Nome], [c].[UF], [c].[Slug] from Cidades c
	INNER JOIN Estados e on e.UF = c.UF
	where (c.Slug = @Slug or @Slug = '') and (e.Slug = @SlugEstado or @SlugEstado = '')
	ORDER BY c.Slug ASC
END