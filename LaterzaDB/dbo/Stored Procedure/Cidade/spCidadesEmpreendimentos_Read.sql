﻿CREATE PROCEDURE [dbo].[spCidadesEmpreendimentos_Read]
	@SlugEstado varchar(300) = ''
AS

BEGIN
	SELECT distinct [c].[Slug], [c].[Nome], [c].[UF] from Cidades c
	INNER JOIN Estados e on e.UF = c.UF
	inner join Empreendimento es on es.Cidade = c.Slug
	where (e.Slug = @SlugEstado or @SlugEstado = '') AND es.Ativo = 1
	ORDER BY c.Slug ASC
END