﻿CREATE PROCEDURE [dbo].[spFaq_Update]
	@Id UNIQUEIDENTIFIER = NULL, 
    @IdiomaId NVARCHAR(10) = NULL,
	@TipoFaq INT = NULL, 
	@TipoFaqArea INT = NULL, 
    @Secao int = NULL,
    @Titulo NVARCHAR(200) = NULL, 
    @Texto NVARCHAR(MAX) = NULL, 
    @UrlImagem NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	Update dbo.Faq set Secao = @Secao, UrlImagem = @UrlImagem, TipoFaq = @TipoFaq, TipoFaqArea = @TipoFaqArea
	 WHERE Id = @Id;

	 	Update dbo.Faq set Titulo = @Titulo, Texto = @Texto
	 WHERE Id = @Id AND IdiomaId = @IdiomaId;
	
	COMMIT TRANSACTION
END