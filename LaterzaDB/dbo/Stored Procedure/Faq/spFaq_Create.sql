﻿CREATE PROCEDURE [dbo].[spFaq_Create]
    @Id UNIQUEIDENTIFIER = NULL, 
    @IdiomaId NVARCHAR(10) = NULL,
	@TipoFaq INT = NULL, 
	@TipoFaqArea INT = NULL,  
    @Secao int = NULL,
    @Titulo NVARCHAR(200) = NULL, 
    @Texto NVARCHAR(MAX) = NULL, 
    @UrlImagem NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.Faq(Id, IdiomaId, TipoFaq, Secao, Titulo, Texto, UrlImagem, TipoFaqArea) 
			                VALUES(@Id, @oneId, @TipoFaq, @Secao, @Titulo, @Texto, @UrlImagem, @TipoFaqArea);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END