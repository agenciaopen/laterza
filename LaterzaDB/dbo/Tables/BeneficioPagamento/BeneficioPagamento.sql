﻿CREATE TABLE [dbo].[BeneficioPagamento]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[IdiomaId] NVARCHAR(10) NOT NULL,
	[IdEmpreendimento] UNIQUEIDENTIFIER NOT NULL,
	[BeneficioPagamento] NVARCHAR(200) NOT NULL,
	CONSTRAINT [PK_EmpreendimentoBeneficioPagamento] PRIMARY KEY ([Id], [IdiomaId], [IdEmpreendimento]),
	CONSTRAINT [FK_BeneficioPagamento_To_Empreendimento] FOREIGN KEY ([IdEmpreendimento], [IdiomaId]) REFERENCES [Empreendimento]([Id], [IdiomaId])
)
