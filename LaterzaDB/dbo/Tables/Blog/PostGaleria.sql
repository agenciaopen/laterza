﻿CREATE TABLE [dbo].[PostGaleria]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
    [IdPost] UNIQUEIDENTIFIER NOT NULL, 
	[UrlImagem] NVARCHAR(MAX) NULL, 
    [UrlVideo] NVARCHAR(MAX) NULL, 
    [Descricao] NVARCHAR(200) NULL,
    [Titulo] NVARCHAR(50) NULL, 
    [EhVideo] BIT NULL, 
    [Ordem] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_PostGaleria_To_Post] FOREIGN KEY ([IdPost]) REFERENCES Post([Id])
)
