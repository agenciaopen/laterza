﻿CREATE TABLE [dbo].[Faq]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL,
    [TipoFaq] INT NULL, 
    [TipoFaqArea] INT NULL,  
    [Secao] int NOT NULL,
    [Titulo] NVARCHAR(200) NULL, 
    [Texto] NVARCHAR(MAX) NULL, 
    [UrlImagem] NVARCHAR(MAX) NULL,
    CONSTRAINT [PK_Faq] PRIMARY KEY ([Id], [IdiomaId], [Secao]),
    CONSTRAINT [FK_Faq_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id])


)
