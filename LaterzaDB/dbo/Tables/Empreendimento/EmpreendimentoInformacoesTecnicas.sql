﻿CREATE TABLE [dbo].[EmpreendimentoInformacoesTecnicas]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL, 
	[Nome] NVARCHAR(MAX) NULL, 
    [Funcao] NVARCHAR(MAX) NULL, 
    [IdEmpreendimento] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [PK_EmpreendimentoInformacoesTecnicas] PRIMARY KEY ([IdEmpreendimento], [IdiomaId], [Id]),
    CONSTRAINT [FK_EmpreendimentoInformacoesTecnicas_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id]),
    CONSTRAINT [FK_EmpreendimentoInformacoesTecnicas_To_Empreendimento] FOREIGN KEY ([IdEmpreendimento], [IdiomaId]) REFERENCES [Empreendimento]([Id], [IdiomaId])
)
