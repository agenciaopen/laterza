﻿CREATE TABLE [dbo].[EmpreendimentoRelacionado]
(
    [IdEmpreendimento] UNIQUEIDENTIFIER NOT NULL,
    [IdEmpreendimentoRelacionado] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_EmpreendimentoRelacionado] PRIMARY KEY ([IdEmpreendimento], [IdEmpreendimentoRelacionado])
)
