﻿CREATE TABLE [dbo].[Estados](
	[Id] [uniqueidentifier] NOT NULL,
	[UF] [varchar](2) NOT NULL,
	[Nome] [varchar](200) NOT NULL,
	[Slug] [varchar](300) NOT NULL,
    CONSTRAINT [PK_Etados] PRIMARY KEY ([Id])
)