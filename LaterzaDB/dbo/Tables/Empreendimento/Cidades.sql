﻿CREATE TABLE [dbo].[Cidades](
	[CidadeId] [int] NOT NULL,
	[Nome] [varchar](200) NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[UF] [varchar](2) NOT NULL,
	[Slug] [varchar](300) NOT NULL,    
	CONSTRAINT [PK_Cidades] PRIMARY KEY ([CidadeId])
) 