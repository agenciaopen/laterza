﻿CREATE TABLE [dbo].[Mensagem]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
	[Nome]  NVARCHAR(MAX) NULL,  
	[Email]  NVARCHAR(80) NULL,  
	[Telefone]  NVARCHAR(80) NULL,  
	[Mensagem]  NVARCHAR(MAX) NULL,  
    [TipoMensagem] INT NULL, 
	[DataCriacao] DATETIME2 NULL, 
    CONSTRAINT [PK_Mensagem] PRIMARY KEY ([Id])
)
