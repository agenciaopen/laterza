﻿CREATE TABLE [dbo].[Escritorio]
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [IdiomaId] NVARCHAR(10) NOT NULL,
    [Nome] NVARCHAR(50) NULL,
    [Telefone] NVARCHAR(50) NULL,
    [Endereco] NVARCHAR(MAX) NULL, 
    [Estado] NVARCHAR(200) NULL, 
    [Cidade] NVARCHAR(200) NULL, 
    [CEP] NVARCHAR(200) NULL,
    [HorarioFuncionamento] NVARCHAR(200) NULL,
    [UrlBanner] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_Escritorio] PRIMARY KEY ([Id], [IdiomaId]),
    CONSTRAINT [FK_Escritorio_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id])
)
