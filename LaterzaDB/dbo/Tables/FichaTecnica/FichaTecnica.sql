﻿CREATE TABLE [dbo].[FichaTecnica]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[IdiomaId] NVARCHAR(10) NOT NULL,
	[IdEmpreendimento] UNIQUEIDENTIFIER NOT NULL,
	[Titulo] NVARCHAR(200) NOT NULL,
	[Destacar] BIT NULL
	CONSTRAINT [PK_FichaTecnicaEmpreendimento] PRIMARY KEY ([Id], [IdiomaId], [IdEmpreendimento]),
	CONSTRAINT [FK_FichaTecnica_To_Empreendimento] FOREIGN KEY ([IdEmpreendimento], [IdiomaId]) REFERENCES [Empreendimento]([Id], [IdiomaId])
)
