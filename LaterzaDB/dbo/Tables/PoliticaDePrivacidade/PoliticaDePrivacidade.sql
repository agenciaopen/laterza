﻿CREATE TABLE [dbo].[PoliticaDePrivacidade]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Conteudo] NVARCHAR(MAX) NULL,
	[DataCriacao] DATETIME2 NULL,
	[DataAlteracao] DATETIME2 NULL,
)
