﻿using Laterza.Admin.Data.Data.Interfaces.EMail;
using Laterza.Admin.Data.Models.EMail;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Laterza.Front.Blazor.Background
{
    public class ServicoEnvioEMail : IHostedService, IDisposable
    {
        System.Timers.Timer _temporizadorServico { get; set; }
        IServiceScopeFactory _provedorServico;
        IControleMsgEMailDataService _appServico;

        void GravaLogInformacao(string msg)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info(msg);
        }

        void GravaLogErro(string msg)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Error(msg);
        }

        public ServicoEnvioEMail(IControleMsgEMailDataService appServico,
                                 IServiceScopeFactory serviceProvider,
                                 ConfigAplicacao config)
        {
            this._appServico = appServico;

            //this._temporizadorServico = new System.Timers.Timer(config.QteIntervalEnvioEMailMin * 60000);  // Tempo em minutos
            this._temporizadorServico = new System.Timers.Timer(200);  // Tempo em minutos
            this._provedorServico = serviceProvider;
        }


        public async Task StartAsync(CancellationToken cancellationToken)
        {
           _temporizadorServico.Start();

            _temporizadorServico.Elapsed += async (object sender, ElapsedEventArgs e) =>
            {
                try
                {
                    _temporizadorServico.Stop();

                    GravaLogInformacao("Inicializando processamento...");

                    using (var scope = _provedorServico.CreateScope())
                    {
                        foreach (var item in _appServico.GetMsg(DateTime.Now).Where(x => x.IdcEnviado == "N").ToList())
                        {
                            var _msg = _appServico.Enviar(item.Id);

                            if (string.IsNullOrEmpty(_msg))
                            {
                                GravaLogInformacao(string.Format("Mensagem {0} processada com Sucesso!", item.Id));
                            }
                            else
                            {
                                GravaLogErro(string.Format("Falha ao processar Mensagem {0}: {1}", item.Id, _msg));
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    GravaLogErro($"Ocorreu um erro no envio: {exception.Message}");
                }
                finally
                {
                    GravaLogInformacao("Processamento finalizado!");

                    _temporizadorServico.Start();
                }
            };

            _temporizadorServico.Start();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _temporizadorServico.Enabled = false;
            _temporizadorServico.Stop();

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _temporizadorServico?.Dispose();
        }
    }
}
