﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;

namespace Laterza.Front.Blazor.Data.MetaTag
{
    public class UrlHelper
    {
        public static IConfiguration Configuration;

        /// <summary>
        /// Regular expression to get all paths from short URL path without "/". Moreover, the first and last "/" may or may not be present. 
        /// Example: from the string "path1/path2/path3" - path1, path2 and path3 will be selected
        /// </summary>
        private static Regex ShortUrlPathRegex = new(@"^((?:/?)([\w\s\.-]+)*)*/*", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant);

        /// <summary>
        /// Retrieving the last path from short URL path based on a regular expression.
        /// </summary>
        /// <param name="path">Short URL path</param>
        /// <returns>Last path from short URL path</returns>
        public static string GetLastPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;
            try
            {
                var match = ShortUrlPathRegex.Match(path);
                if (!match.Success)
                    return string.Empty;
                return match.Groups[2].Value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get data for SEO (title, keywords, description, canonical) depending on the path URL
        /// </summary>
        /// <param name="path">URL path</param>
        /// <returns>
        /// Tuple: 
        /// item1 - title 
        /// item2 - keywords 
        /// item3 - description 
        /// item4 - canonical
        /// </returns>
        public static (string, string, string, string) GetSeoData(string path)
        {
            string title = "Seja bem-vindo a Laterza";
            string keywords = ".";
            string description = "";
            string canonical = "";

            if (string.IsNullOrWhiteSpace(path))
                return (title, keywords, description, canonical);

            var frontDataService = new FrontDataService(new SqlDataAccess(Configuration));

            if (path == "/")
            {
                path = "/home";
            }

            var pagina = frontDataService.GetPagina(UrlHelper.GetLastPath(path.Split('/')[1])).Where(x => x.IdiomaId == "pt");

            var empreendimento = frontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && x.Slug == UrlHelper.GetLastPath(path));

            if (pagina.Count() > 0)
            {
                title = pagina.FirstOrDefault().MataTittle;
                keywords = pagina.FirstOrDefault().MetaKeyword;
                description = pagina.FirstOrDefault().MetaDescription;
            }
            else if (empreendimento.Count() > 0)
            {
                title = empreendimento.FirstOrDefault().MataTittle;
                keywords = empreendimento.FirstOrDefault().MetaKeyword;
                description = empreendimento.FirstOrDefault().MetaDescription;
            }

            if (path == "/home")
            {
                path = "/";
            }
            canonical = canonical + path;

            return (title, keywords, description, canonical);
        }
    }
}
