﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Laterza.Front.Blazor.Data.MetaTag
{
    public class KeywordsMetaTagComponent : ComponentBase
    {
        [Parameter]
        public string Content { get; set; }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            base.BuildRenderTree(builder);
            builder.OpenElement(0, "meta");
            builder.AddAttribute(1, "name", "keywords");
            builder.AddAttribute(2, "content", HttpUtility.HtmlDecode(Content) ?? string.Empty);
            builder.CloseElement();
        }

    }
}
