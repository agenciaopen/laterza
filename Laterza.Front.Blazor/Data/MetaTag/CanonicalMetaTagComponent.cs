﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Data.MetaTag
{
    public class CanonicalMetaTagComponent : ComponentBase
    {
        [Parameter]
        public string Content { get; set; }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            base.BuildRenderTree(builder);
            builder.OpenElement(0, "link");
            builder.AddAttribute(1, "rel", "canonical");
            builder.AddAttribute(2, "href", Content ?? string.Empty);
            builder.CloseElement();
        }

    }
}
