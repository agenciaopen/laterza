﻿using System;

namespace MouraDubeux.Front.Blazor.Data
{
    public interface IExtensionMethods
    {
        string Description(Enum enumValue);
        string CreateSlug(string str);
    }
}