window.initprograma = function () {
    $('.menu__open ul li a').click(function (e) {
        $('.trigger').click();
    });
      $(".trigger").on('click', function(){
          if($(".trigger-top").hasClass("no-animation")){
              $(".menu").toggleClass("menu__open");
              $(".trigger-top").removeClass("no-animation");
              $(".trigger-middle").toggleClass("d-none");
              $(".trigger-bottom").removeClass("no-animation");
              $(this).toggleClass('is-active');
          }
          else{
              $(this).toggleClass('is-active');
              setTimeout(function() { 
                  $(".menu").toggleClass("menu__open");
                  $(".trigger-middle").toggleClass("d-none");
              }, 100); 
          }
        });
    // $(window).load(function(){
        $("#plan__slider").lightSlider({
            item: 2,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,
    
            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////
    
            speed: 400, //ms'
            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,
    
            keyPress: true,
            controls: true,
            prevHtml: '',
            nextHtml: '',
    
            rtl:false,
            adaptiveHeight:false,
    
            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,
    
            responsive : [],
        });
        $('#plan__slider').lightGallery({
            download: false,
            pullCaptionUp: true,
            subHtmlSelectorRelative: true
        });

        $(function () {
            $(".icon__list .media").slice(0, 1).show(); //slice quantidade que aparece
            $("body").on('click touchstart', '.load-more', function (e) {
                e.preventDefault();
                $(".icon__list .media:hidden").slice(0, 2).slideDown(); // quantidade oculta
                if ($(".icon__list .media:hidden").length == 0) {
                    $(".load-more").css('visibility', 'hidden');
                }
                $('html,body').animate({
                    scrollTop: $(this).offset().top
                }, 1000);
            });
        });
       //   LOAD MORE
       // $(function () {
       //     $(".icon__list .media2").slice(0, 3).show(); //slice quantidade que aparece
       //     $("body").on('click touchstart', '.load-more2', function (e) {
       //         e.preventDefault();
       //         $(".icon__list .media2:hidden").slice(0, 2).slideDown(); // quantidade oculta
       //         if ($(".icon__list .media2:hidden").length == 0) {
       //             $(".load-more2").css('visibility', 'hidden');
       //         }
       //         $('html,body').animate({
       //             scrollTop: $(this).offset().top
       //         }, 1000);
       //     });
       // }); END
    
}