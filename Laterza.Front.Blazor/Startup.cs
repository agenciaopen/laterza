using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Data.EMail;
using Laterza.Admin.Data.Data.Interfaces.EMail;
using Laterza.Admin.Data.DataAccess;
using Laterza.Admin.Data.Models.EMail;
using Laterza.Front.Blazor.Background;
using Laterza.Front.Blazor.Data;
using Laterza.Front.Blazor.Data.MetaTag;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MouraDubeux.Front.Blazor.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            UrlHelper.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();
            services.AddSingleton<IExtensionMethods, ExtensionMethods>();
            services.AddSingleton<ISqlDataAccess, SqlDataAccess>();
            services.AddScoped<IFrontDataService, FrontDataService>();


            services.AddSingleton<IControleMsgEMailDataService, ControleMsgEMailDataService>();
            services.AddSingleton<IControleMsgEMailLogDataService, ControleMsgEMailLogDataService>();
            services.AddHostedService<ServicoEnvioEMail>();

            CultureInfo.CurrentCulture = new CultureInfo("pt-BR");

            var _conf = new ConfigAplicacao();
            Configuration.Bind("ConfigAplicacao", _conf);
            services.AddSingleton(_conf);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
