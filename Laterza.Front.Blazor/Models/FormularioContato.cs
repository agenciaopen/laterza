﻿using Laterza.Front.Blazor.Validacao;
using System.ComponentModel.DataAnnotations;

namespace Laterza.Front.Blazor.Models
{
    public class FormularioContato
    {
        [Required(ErrorMessage = "A aplicação requer que o campo Nome seja preenchido!")]
        [StringLength(100, ErrorMessage = "O Nome deve possuir no máximo 100 caracteres")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo E-mail seja preenchido!")]
        [StringLength(100, ErrorMessage = "O E-mail deve possuir no máximo 100 caracteres")]
        [ValidaEMail]
        public string EMail { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Telefone seja preenchido!")]
        [StringLength(11, ErrorMessage = "O Telefone deve possuir no máximo 11 caracteres")]
        public string Telefone { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Mensagem seja preenchido!")]
        [StringLength(500, ErrorMessage = "O Mensagem deve possuir no máximo 500 caracteres")]
        public string Msg { get; set; }

        public bool IdcPoliticaPrivacidade { get; set; }

        public bool IdcRecebeInformacao { get; set; }
    }
}
