﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Models
{
    public class FiltroEstadoCidade
    {
        public string Estado { get; set; }
        public string Cidade { get; set; }
    }
}
