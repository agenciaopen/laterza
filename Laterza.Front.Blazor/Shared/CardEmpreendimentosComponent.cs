﻿using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Front.Blazor.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Laterza.Admin.Data.Enum;

namespace Laterza.Front.Blazor.Shared
{
    public partial class CardEmpreendimentosComponent
    {
        [Parameter]
        public List<EmpreendimentoFichaTecnicaModel> Empreendimentos { get; set; }
        [Parameter]
        public string Status { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }
    }
}
