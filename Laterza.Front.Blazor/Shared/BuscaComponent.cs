﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Models.Cidades;
using Laterza.Admin.Data.Models.Estados;
using Laterza.Front.Blazor.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Shared
{
    public partial class BuscaComponent
    {
        public List<EstadosModel> Estados { get; set; }
        public List<CidadesModel> Cidades { get; set; } = new List<CidadesModel>();

        [Inject]
        public IFrontDataService FrontDataService { get; set; }

        [Inject]
        public ILogger<BuscaComponent> _log { get; set; }

        private FiltroEstadoCidade filtroEstadoCidade = new FiltroEstadoCidade();

        [Parameter]
        public EventCallback<string> ProcessarEventCallback { get; set; }

        protected override void OnParametersSet()
        {
            filtroEstadoCidade = new FiltroEstadoCidade();
            Cidades.Clear();
            GetEstadosComEmpreendimento();
            base.OnParametersSet();
        }

        public void GetEstadosComEmpreendimento()
        {
            try
            {
                Estados = FrontDataService.GetEstadosComEmpreendimento();
            }
            catch (Exception e)
            {
                _log.LogWarning(e, "Falha ao carregar os estados");
            }
        }

        private async Task HandleValidSubmit()
        {
            string value = "";
            value += string.IsNullOrEmpty(filtroEstadoCidade.Estado) ? "" : filtroEstadoCidade.Estado + "/";
            value += string.IsNullOrEmpty(filtroEstadoCidade.Cidade) ? "" : filtroEstadoCidade.Cidade + "/";
            await ProcessarEventCallback.InvokeAsync(value);
        }

        private void HandleEstadoDropDownChange(ChangeEventArgs e)
        {
            filtroEstadoCidade.Cidade = string.Empty;
            Cidades = FrontDataService.GetCidadesComEmpreendimentoByEstado(e.Value.ToString());
            StateHasChanged();
        }
    }
}
