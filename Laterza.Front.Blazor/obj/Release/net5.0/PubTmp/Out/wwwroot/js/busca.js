window.initBusca = function () {

    let queryString = window.location.search;

    jQuery(document).ready(function ($) {
        if (queryString != null) {
            $('html, body').animate({
                scrollTop: $('#buscaResultado').offset().top
            }, 'slow');
        } else {
            console.log('empty string');
        }
    });
}