#pragma checksum "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dbc000fd0e9b135a875fcbe7bc1fed31c6b8b08c"
// <auto-generated/>
#pragma warning disable 1591
namespace Laterza.Front.Blazor.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Laterza.Front.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\_Imports.razor"
using Laterza.Front.Blazor.Shared;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    [Microsoft.AspNetCore.Components.RouteAttribute("/{estado}")]
    [Microsoft.AspNetCore.Components.RouteAttribute("/{estado}/{cidade}")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Laterza.Front.Blazor.Pages.home.HeroHomeComponent>(0);
            __builder.CloseComponent();
            __builder.AddMarkupContent(1, "\r\n");
            __builder.OpenComponent<Laterza.Front.Blazor.Shared.BuscaComponent>(2);
            __builder.AddAttribute(3, "ProcessarEventCallback", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, 
#nullable restore
#line 9 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\Pages\Index.razor"
                                        HandleStateChange

#line default
#line hidden
#nullable disable
            )));
            __builder.CloseComponent();
            __builder.AddMarkupContent(4, "\r\n");
            __builder.OpenComponent<Laterza.Front.Blazor.Pages.home.LancamentosComponent>(5);
            __builder.AddAttribute(6, "Empreendimentos", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.List<Laterza.Admin.Data.Models.Empreendimento.EmpreendimentoFichaTecnicaModel>>(
#nullable restore
#line 10 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\Pages\Index.razor"
                                                                       EmpreendimentosModel.Where(E => E.StatusEmpreendimento == 4).ToList()

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(7, "\r\n");
            __builder.OpenComponent<Laterza.Front.Blazor.Pages.home.ObrasAndamentoComponent>(8);
            __builder.AddAttribute(9, "Empreendimentos", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.List<Laterza.Admin.Data.Models.Empreendimento.EmpreendimentoFichaTecnicaModel>>(
#nullable restore
#line 11 "C:\workspace\ConstrutoraLaterza\Laterza.Front.Blazor\Pages\Index.razor"
                                                                          EmpreendimentosModel.Where(E => E.StatusEmpreendimento == 1).ToList()

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(10, "\r\n");
            __builder.OpenElement(11, "div");
            __builder.AddAttribute(12, "class", "contact_home");
            __builder.OpenComponent<Laterza.Front.Blazor.Shared.ContactFormComponent>(13);
            __builder.CloseComponent();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
