#pragma checksum "C:\workspace\laterza\Laterza.Front.Blazor\Pages\quem-somos\Visao.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a850a9e74bc71d0f017460d128067afe2aaf6dca"
// <auto-generated/>
#pragma warning disable 1591
namespace Laterza.Front.Blazor.Pages.quem_somos
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Laterza.Front.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Laterza.Front.Blazor.Shared;

#line default
#line hidden
#nullable disable
    public partial class Visao : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<section class=""flow section__visao""><div class=""h-100 container""><div class=""h-100 row justify-content-between""><div class=""col-md-12""><div class=""row h-100""><h2 class=""align-items-start"">Visão</h2>
                    <p>Ser uma referência no segmento médio da construção civil, com crescimento sustentável e de forma permanente com a máxima rentabilidade, comprometida com a melhoria contínua dos processos, elevando ainda mais os índices de confiança e satisfação dos nossos clientes.</p></div></div></div></div></section>");
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
