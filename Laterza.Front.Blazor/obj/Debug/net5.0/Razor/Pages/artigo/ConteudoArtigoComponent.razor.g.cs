#pragma checksum "C:\workspace\laterza\Laterza.Front.Blazor\Pages\artigo\ConteudoArtigoComponent.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0174bcadde5c3daeb67327e56475879579d5ba80"
// <auto-generated/>
#pragma warning disable 1591
namespace Laterza.Front.Blazor.Pages.artigo
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Laterza.Front.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\workspace\laterza\Laterza.Front.Blazor\_Imports.razor"
using Laterza.Front.Blazor.Shared;

#line default
#line hidden
#nullable disable
    public partial class ConteudoArtigoComponent : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "section");
            __builder.AddAttribute(1, "class", "flow content__artigo");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "h-100 container");
            __builder.AddMarkupContent(4, @"<a class=""row"" href=""/blog"" title=""voltar""><svg aria-hidden=""true"" focusable=""false"" data-prefix=""fas"" data-icon=""chevron-left"" class=""svg-inline--fa fa-chevron-left fa-w-10"" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 320 512""><path fill=""currentColor"" d=""M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z""></path></svg>
            <h5>ler outras matérias</h5></a>
        ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "h-100 row justify-content-center");
            __builder.OpenElement(7, "div");
            __builder.AddAttribute(8, "class", "col-md-12");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "class", "row h-100");
            __builder.OpenElement(11, "header");
            __builder.OpenElement(12, "h2");
            __builder.AddAttribute(13, "class", "align-items-start");
#nullable restore
#line 11 "C:\workspace\laterza\Laterza.Front.Blazor\Pages\artigo\ConteudoArtigoComponent.razor"
__builder.AddContent(14, Post.Descricao);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(15, "\r\n                    ");
#nullable restore
#line 13 "C:\workspace\laterza\Laterza.Front.Blazor\Pages\artigo\ConteudoArtigoComponent.razor"
__builder.AddContent(16, (MarkupString)Post.Conteudo);

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(17, "\r\n                    ");
            __builder.OpenElement(18, "div");
            __builder.AddAttribute(19, "class", "col-md-12");
            __builder.OpenElement(20, "img");
            __builder.AddAttribute(21, "class", "w-100");
            __builder.AddAttribute(22, "src", 
#nullable restore
#line 15 "C:\workspace\laterza\Laterza.Front.Blazor\Pages\artigo\ConteudoArtigoComponent.razor"
                                                 Post.UrlImagemDestaque

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(23, "alt", "Construtora Laterza");
            __builder.AddAttribute(24, "title", "Construtora Laterza");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
