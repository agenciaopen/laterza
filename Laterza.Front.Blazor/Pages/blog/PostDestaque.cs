﻿using Laterza.Admin.Data.Models.Blog;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.blog
{
    public partial class PostDestaque
    {
        [Parameter]
        public List<PostModel> Posts { get; set; }
    }
}
