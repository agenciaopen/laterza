﻿using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.empreendimento_detalhes
{
    public partial class GaleriaEmpreendimento
    {
        [Parameter]
        public List<EmpreendimentoGaleriaModel> Galeria { get; set; }
    }
}
