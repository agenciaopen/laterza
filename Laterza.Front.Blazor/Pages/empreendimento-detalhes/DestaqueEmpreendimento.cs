﻿using Laterza.Admin.Data.Models.BeneficioPagamento;
using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.empreendimento_detalhes
{
    public partial class DestaqueEmpreendimento
    {
        [Parameter]
        public EmpreendimentoModel Empreendimento { get; set; }
        
        [Parameter]
        public List<BeneficioPagamentoModel> BeneficiosPagamento { get; set; }
    }
}
