﻿using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.empreendimento_detalhes
{
    public partial class MapaEmpreendimento
    {
        [Parameter]
        public EmpreendimentoModel Empreendimento { get; set; }
        [Inject]
        public IJSRuntime JSRuntime { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender && (!string.IsNullOrEmpty(Empreendimento.Latitude) && !string.IsNullOrEmpty(Empreendimento.Longitude)))
            {
                InitMap(Empreendimento.Latitude, Empreendimento.Longitude);
            }

        }

        protected async void InitMap(string latitude, string longitude)
        {
            var parameters = new
            {
                latitude,
                longitude
            };

            await JSRuntime.InvokeAsync<object>("initMap", parameters);
            StateHasChanged();
        }
    }
}
