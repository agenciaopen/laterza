﻿using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.FichaTecnica;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.empreendimento_detalhes
{
    public partial class PlantaFicha
    {
        [Parameter]
        public List<EmpreendimentoGaleriaModel> Plantas { get; set; }
        [Parameter]
        public List<FichaTecnicaModel> FichasTecnicas { get; set; }
        [Parameter]
        public EmpreendimentoModel Empreendimento { get; set; }
    }
}
