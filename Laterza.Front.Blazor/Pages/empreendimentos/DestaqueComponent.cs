﻿using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.empreendimentos
{
    public partial class DestaqueComponent
    {
        [Parameter]
        public List<EmpreendimentoFichaTecnicaModel> EmpreeendimentosDestaques{ get; set; }
    }
}
