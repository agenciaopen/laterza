﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Models.BeneficioPagamento;
using Laterza.Admin.Data.Models.Empreendimento;
using Laterza.Admin.Data.Models.FichaTecnica;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages
{
    public partial class EmpreendimentoDetalhes
    {
        [Parameter]
        public string Slug { get; set; }
        public EmpreendimentoModel Empreendimento { get; set; }
        public List<EmpreendimentoGaleriaModel> GaleriaImovel { get; set; }
        public List<EmpreendimentoGaleriaModel> GaleriaPlantas { get; set; }
        public List<BeneficioPagamentoModel> BeneficiosPagamento { get; set; }
        public List<FichaTecnicaModel> FichasTecnicas { get; set; }
        [Inject]
        public IFrontDataService FrontDataService { get; set; }
        [Inject]
        public ILogger<Empreendimentos> _log { get; set; }

        protected override void OnParametersSet()
        {
            Empreendimento = FrontDataService.GetEmpreendimento(Slug).Where(E => E.IdiomaId == "pt").First();
            GaleriaImovel = FrontDataService.GetEmpreendimentoGaleria(Empreendimento.Id, 1).Where(E => E.IdiomaId == "pt").ToList();
            GaleriaPlantas = FrontDataService.GetEmpreendimentoGaleria(Empreendimento.Id, 2).Where(E => E.IdiomaId == "pt").ToList();
            BeneficiosPagamento = FrontDataService.GetBeneficioPagamentos(Empreendimento.Id).Where(E => E.IdiomaId == "pt").ToList();
            FichasTecnicas = FrontDataService.GetFichaTecnica(Empreendimento.Id).Where(E => E.IdiomaId == "pt").ToList();
            base.OnParametersSet();
        }
    }
}
