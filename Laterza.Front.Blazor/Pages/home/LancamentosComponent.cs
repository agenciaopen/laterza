﻿using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Front.Blazor.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.home
{
    public partial class LancamentosComponent
    {
        [Parameter]
        public List<EmpreendimentoFichaTecnicaModel> Empreendimentos { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }
    }
}
