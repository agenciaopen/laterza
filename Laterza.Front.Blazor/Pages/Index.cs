﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Laterza.Front.Blazor.Pages
{
    public partial class Index
    {
        [Parameter]
        public string Estado { get; set; }

        [Parameter]
        public string Cidade { get; set; }
        [Inject]
        public IFrontDataService FrontDataService { get; set; }
        [Inject]
        public ILogger<Empreendimentos> _log { get; set; }
        public List<EmpreendimentoFichaTecnicaModel> EmpreendimentosModel { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }


        protected override void OnParametersSet()
        {
            GetEmpreendimentos();
            base.OnParametersSet();
        }
        public void GetEmpreendimentos()
        {
            try
            {
                EmpreendimentosModel = FrontDataService.GetEmpreendimentoComFichaTecnica()
                    .Where(E => E.Ativo && E.IdiomaId == "pt" && E.DestacarHome
                        && (Estado != null ? E.Estado == Estado : true)
                        && (Cidade != null ? E.Cidade == Cidade : true))
                    .ToList();
            }
            catch (Exception e)
            {
                _log.LogWarning(e, "Falha ao carregar os empreendimentos");
            }
        }
        private void HandleStateChange(string value)
        {
            NavigationManager.NavigateTo(value);
        }
    }
}
