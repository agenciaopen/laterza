﻿using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages.politica
{
    public partial class ConteudoPoliticaComponent
    {
        [Parameter]
        public PoliticaDePrivacidadeModel PoliticaDePrivacidade { get; set; }
    }
}
