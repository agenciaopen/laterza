﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Models.Empreendimento;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Laterza.Front.Blazor.Pages.empreendimentos;
using MouraDubeux.Front.Blazor.Data;
using Laterza.Admin.Data.Enum;
using Microsoft.AspNetCore.WebUtilities;

namespace Laterza.Front.Blazor.Pages
{
    public partial class Empreendimentos
    {
        [Parameter]
        public string Estado { get; set; }
        
        [Parameter]
        public string Cidade { get; set; }

        public string Status { get; set; }
        [Inject]
        public IFrontDataService FrontDataService { get; set; }
        [Inject]
        public ILogger<Empreendimentos> _log { get; set; }
        public List<EmpreendimentoFichaTecnicaModel> EmpreendimentosModel { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }


        protected override void OnParametersSet()
        {
            GetQueryParams();
            GetEmpreendimentos();
            base.OnParametersSet();
        }

        public void GetQueryParams()
        {
            var uri = NavigationManager.ToAbsoluteUri(NavigationManager.Uri);
            var queryStrings = QueryHelpers.ParseQuery(uri.Query);
            if (queryStrings.TryGetValue("Status", out var value))
                Status = value;
            else
                Status = null;
        }

        public void GetEmpreendimentos()
        {
            try
            {
                EmpreendimentosModel = FrontDataService.GetEmpreendimentoComFichaTecnica()
                    .Where(E => E.Ativo && E.IdiomaId == "pt" 
                        && (Estado != null ? E.Estado == Estado : true)
                        && (Cidade != null ? E.Cidade == Cidade : true)
                        && (Status != null ? extensionMethods.Description((EnumStatusEmpreendimento)E.StatusEmpreendimento).Replace(" ", "") == Status : true))
                    .ToList();
            }
            catch(Exception e)
            {
                _log.LogWarning(e, "Falha ao carregar os empreendimentos");
            }
        }

        private void HandleStateChange(string value)
        {
            Dictionary<string, string> QueryParams = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Status))
                QueryParams.Add("Status", Status);
            NavigationManager.NavigateTo(QueryHelpers.AddQueryString("/empreendimentos/" + value, QueryParams));
        }
    }
}
