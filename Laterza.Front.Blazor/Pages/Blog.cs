﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Models.Blog;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages
{
    public partial class Blog
    {
        [Inject]
        public IFrontDataService FrontDataService { get; set; }
        [Inject]
        public ILogger<Blog> _log { get; set; }
        public List<PostModel> PostModel { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }
        [Parameter]
        public string Categoria { get; set; }


        protected override void OnParametersSet()
        {
            GetPosts();
            base.OnParametersSet();
        }
        public void GetPosts()
        {
            try
            {
                PostModel = FrontDataService.GetPosts();
            }
            catch (Exception e)
            {
                _log.LogWarning(e, "Falha ao carregar os empreendimentos");
            }
        }
    }
}
