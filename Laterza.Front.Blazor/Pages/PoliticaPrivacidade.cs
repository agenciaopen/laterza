﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Models.PoliticaDePrivacidade;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages
{
    public partial class PoliticaPrivacidade
    {
        [Inject]
        public IFrontDataService FrontDataService { get; set; }
        public PoliticaDePrivacidadeModel PoliticaDePrivacidade { get; set; }

        protected override void OnParametersSet()
        {
            GetPoliticaDePrivacidade();
            base.OnParametersSet();
        }

        private void GetPoliticaDePrivacidade()
        {
            PoliticaDePrivacidade = FrontDataService.GetPoliticaDePrivacidade();
        }
    }
}
