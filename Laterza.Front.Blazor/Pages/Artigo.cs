﻿using Laterza.Admin.Data.Data;
using Laterza.Admin.Data.Models.Blog;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laterza.Front.Blazor.Pages
{
    public partial class Artigo
    {
        [Inject]
        public IFrontDataService FrontDataService { get; set; }
        [Inject]
        public ILogger<Artigo> _log { get; set; }
        public PostModel PostModel { get; set; }
        public List<PostGaleriaModel> PostGaleriaModel { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }
        [Parameter]
        public string Slug { get; set; }

        protected override void OnParametersSet()
        {
            GetPost();
            GetPostGaleria();
            base.OnParametersSet();
        }
        public void GetPost()
        {
            try
            {
                PostModel = FrontDataService.GetPosts().Where(x => x.Slug == Slug || string.IsNullOrEmpty(Slug)).First();
            }
            catch (Exception e)
            {
                _log.LogWarning(e, "Falha ao carregar os posts");
            }
        }

        public void GetPostGaleria()
        {
            try
            {
                PostGaleriaModel = FrontDataService.GetPostGaleria(PostModel.Id);
            }
            catch (Exception e)
            {
                _log.LogWarning(e, "Falha ao carregar a galeria do post");
            }
        }
    }
}
