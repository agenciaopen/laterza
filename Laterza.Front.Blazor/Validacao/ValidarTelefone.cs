﻿

using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Laterza.Front.Blazor.Validacao
{
    public class ValidarTelefone: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext contex)
        {
            if (value == null)
            {
                return new ValidationResult(FormatErrorMessage("A aplicação requer que o campo Telefone seja preenchido!"));
            }

            return new Regex(@"^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$").Match(value.ToString()).Success ? ValidationResult.Success : new ValidationResult(FormatErrorMessage("Telefone inválido!"));
        }
    }
}
