﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Laterza.Front.Blazor.Validacao
{
    public sealed class ValidaEMail : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext contex)
        {
            if (value == null)
            {
                return new ValidationResult(FormatErrorMessage("A aplicação requer que o campo E-Mail seja preenchido!"));
            }

            return new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$").Match(value.ToString()).Success ? ValidationResult.Success : new ValidationResult(FormatErrorMessage("E-mail inválido!"));
        }
    }

}
