/*
drop table ControleMsgEMail_Log
drop table ControleMsgEMail
drop procedure spControleMsgEMailLog_Read
drop procedure spControleMsgEMailLog_Create
drop procedure spControleMsgEMail_Read
drop procedure spControleMsgEMail_ReadId
drop procedure spControleMsgEMail_Create
drop procedure spControleMsgEMail_ApontaEnvio
drop table ContatoMsg
drop procedure spContatoMsg_Create
*/

create table ControleMsgEMail
(Id int not null identity,
 DataMsg smalldatetime default getdate(),
 CodMensagem uniqueidentifier,
 Destino varchar(1000),
 Assunto varchar(100),
 CorpoMsg text,
 IdcEnviado char(1),
 NomeArquivo varchar(100),
 ConteudoArquivo varbinary(max),
 constraint pk_ControleMsgEMail primary key(Id),
 constraint ck1_ControleMsgEMail check(IdcEnviado = 'S' or IdcEnviado = 'N'));

 go

create table ControleMsgEMail_Log
(IdControleMsgEMail int not null,
 IdLog int not null,
 DescricaoLog varchar(1000),
 TipoLog char(1),
 DataLog smalldatetime default getdate(),
 constraint pk_ControleMsgEMail_Log primary key (IdControleMsgEMail, IdLog),
 constraint fk1_ControleMsgEMail_Log foreign key (IdControleMsgEMail) references ControleMsgEMail(Id),
 constraint ck1_ControleMsgEMail_Log check(TipoLog = 'I' or TipoLog = 'E'));

 go

 create PROCEDURE spControleMsgEMail_Read
   @DataMsg SMALLDATETIME = NULL
AS
BEGIN
   select	Id, DataMsg, CodMensagem, Destino, Assunto, CorpoMsg, IdcEnviado, NomeArquivo, ConteudoArquivo
	from ControleMsgEMail
	where ((day(DataMsg) = day(@DataMsg) and month(DataMsg) = month(@DataMsg) and year(DataMsg) = year(@DataMsg)) or @DataMsg is null)
END;
go
create PROCEDURE spControleMsgEMail_ReadId
   @Id int
AS
BEGIN
   select	Id, DataMsg, CodMensagem, Destino, Assunto, CorpoMsg, IdcEnviado, NomeArquivo, ConteudoArquivo
	from ControleMsgEMail
	where Id = @Id
END;

go

 CREATE PROCEDURE spControleMsgEMail_ApontaEnvio
   @Id int,
   @IdcEnviado char(1)
AS
BEGIN
   update ControleMsgEMail
   set IdcEnviado = @IdcEnviado
   where Id = @Id
END;

go

create PROCEDURE spControleMsgEMail_Create
   @CodMensagem uniqueidentifier,
   @Destino varchar(1000),
   @Assunto varchar(100),
   @CorpoMsg text,
   @NomeArquivo varchar(100),
   @ConteudoArquivo varbinary(max)
as
begin
   insert into ControleMsgEMail (DataMsg, CodMensagem, Destino, Assunto, CorpoMsg, IdcEnviado, NomeArquivo, ConteudoArquivo)
   values (dateadd(hour, -3, getdate()), @CodMensagem, @Destino, @Assunto, @CorpoMsg, 'N', @NomeArquivo, @ConteudoArquivo)
end;
go
create PROCEDURE spControleMsgEMailLog_Read
   @IdControleMsgEMail int
AS
BEGIN
   select	IdControleMsgEMail, IdLog, DescricaoLog, TipoLog, DataLog
	from ControleMsgEMail_Log
	where IdControleMsgEMail = @IdControleMsgEMail
END;

go
create procedure spControleMsgEMailLog_Create
  @IdControleMsgEMail int,
  @DescricaoLog varchar(1000),
  @TipoLog char(1)
as
begin
   declare @IdLog int

   select @IdLog = count(IdLog) + 1
   from ControleMsgEMail_Log
   where IdControleMsgEMail = @IdControleMsgEMail

   insert into ControleMsgEMail_Log (IdControleMsgEMail, IdLog, DescricaoLog, TipoLog, DataLog)
   values (@IdControleMsgEMail, @IdLog, @DescricaoLog, @TipoLog, dateadd(hour, -3, getdate()))
end
go
create table ContatoMsg
( CodMensagem uniqueidentifier,
  IdcConcordaReceberInformacao char(1),
  constraint ck1_ContatoMsg check (IdcConcordaReceberInformacao = 'S' or IdcConcordaReceberInformacao = 'N'));
go
create procedure spContatoMsg_Create
  @CodMensagem uniqueidentifier,
  @IdcConcordaReceberInformacao char(1)
as
set nocount on
  insert into ContatoMsg (CodMensagem, IdcConcordaReceberInformacao)
  values (@CodMensagem, @IdcConcordaReceberInformacao)
set nocount off